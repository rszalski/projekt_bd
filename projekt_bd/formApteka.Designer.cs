﻿namespace projekt_bd {
    partial class formApteka {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formApteka));
            this.MainMenu = new System.Windows.Forms.TabControl();
            this.tabSell = new System.Windows.Forms.TabPage();
            this.dtp_tabSell_filter_Date = new System.Windows.Forms.DateTimePicker();
            this.cb_tabSell_filter_Value = new System.Windows.Forms.ComboBox();
            this.cbx_tabSell_filter_Value = new System.Windows.Forms.CheckBox();
            this.tbx_tabSell_filter_Value = new System.Windows.Forms.TextBox();
            this.cb_tabSell_filter_Date = new System.Windows.Forms.ComboBox();
            this.cbx_tabSell_filter_Date = new System.Windows.Forms.CheckBox();
            this.btnSellAddFil = new System.Windows.Forms.Button();
            this.btnSellDelFil = new System.Windows.Forms.Button();
            this.btnSellNewSell = new System.Windows.Forms.Button();
            this.dgvClient = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvRecipeInfo = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Sell_Sell = new System.Windows.Forms.Label();
            this.lbl_Sell_SellMeds = new System.Windows.Forms.Label();
            this.dgvSellMeds = new System.Windows.Forms.DataGridView();
            this.dgvSell = new System.Windows.Forms.DataGridView();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.tabMeds = new System.Windows.Forms.TabPage();
            this.btnRemoveFilters = new System.Windows.Forms.Button();
            this.btnSaveMedsChanges = new System.Windows.Forms.Button();
            this.grpActiveFilters = new System.Windows.Forms.GroupBox();
            this.listView_ActiveFilters_Meds = new System.Windows.Forms.ListView();
            this.grpFilters = new System.Windows.Forms.GroupBox();
            this.tbx_tabMeds_filter_Price = new System.Windows.Forms.TextBox();
            this.tbx_tabMeds_filter_Count = new System.Windows.Forms.TextBox();
            this.cb_tabMeds_filter_Recipe = new System.Windows.Forms.ComboBox();
            this.cb_tabMeds_filter_Price = new System.Windows.Forms.ComboBox();
            this.cb_tabMeds_filter_Count = new System.Windows.Forms.ComboBox();
            this.cbx_tabMeds_filter_Recipe = new System.Windows.Forms.CheckBox();
            this.cbx_tabMeds_filter_Price = new System.Windows.Forms.CheckBox();
            this.cbx_tabMeds_filter_Count = new System.Windows.Forms.CheckBox();
            this.tbx_tabMeds_filter_Nr = new System.Windows.Forms.TextBox();
            this.tbx_tabMeds_filter_Name = new System.Windows.Forms.TextBox();
            this.tbx_tabMeds_filter_Id = new System.Windows.Forms.TextBox();
            this.cbx_tabMeds_filter_Nr = new System.Windows.Forms.CheckBox();
            this.cbx_tabMeds_filter_Name = new System.Windows.Forms.CheckBox();
            this.cbx_tabMeds_filter_Id = new System.Windows.Forms.CheckBox();
            this.cb_tabMeds_filter_Nr = new System.Windows.Forms.ComboBox();
            this.cb_tabMeds_filter_Name = new System.Windows.Forms.ComboBox();
            this.cb_tabMeds_filter_Id = new System.Windows.Forms.ComboBox();
            this.dgvMeds = new System.Windows.Forms.DataGridView();
            this.contextMenuLeki = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.additionalMedInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.bnMeds = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bsMeds = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAddFilters = new System.Windows.Forms.Button();
            this.tabDelivery = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_zmien_cene = new System.Windows.Forms.Button();
            this.TBox_nowa_cena = new System.Windows.Forms.TextBox();
            this.btn_wybierz_dostawce = new System.Windows.Forms.Button();
            this.btn_odswiez = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TBox_ilosc = new System.Windows.Forms.TextBox();
            this.MTBox_data_waznosci = new System.Windows.Forms.MaskedTextBox();
            this.btn_dodaj_lek = new System.Windows.Forms.Button();
            this.btn_anuluj_dostawe = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nazwaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iloscDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrpartiiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cenaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.narecepteDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lekiBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CBox_dostawca = new System.Windows.Forms.ComboBox();
            this.dostawcyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.btn_zapisz_dostawe = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ndostawcaAdres = new System.Windows.Forms.TextBox();
            this.ndostawcaKod = new System.Windows.Forms.TextBox();
            this.ndostawcaMiejscowosc = new System.Windows.Forms.TextBox();
            this.ndostawcaTelefon = new System.Windows.Forms.TextBox();
            this.ndostawcaNazwa = new System.Windows.Forms.TextBox();
            this.tabEmployees = new System.Windows.Forms.TabPage();
            this.farmaceuciDataGridView = new System.Windows.Forms.DataGridView();
            this.nazwiskoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imieDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataurDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kodpocztDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.miejscowoscDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefonDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDFarmaceutyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.farmaceuciBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.grpFarmaceuciFiltry = new System.Windows.Forms.GroupBox();
            this.tableFiltry = new System.Windows.Forms.TableLayoutPanel();
            this.comboBoxFarmaceuciKolumny = new System.Windows.Forms.ComboBox();
            this.btnFarmaceuciAddSelectedFIlter = new System.Windows.Forms.Button();
            this.btnFarmaceuciKasujFiltry = new System.Windows.Forms.Button();
            this.btnFarmaceuciFiltruj = new System.Windows.Forms.Button();
            this.btnSaveFarmaceuci = new System.Windows.Forms.Button();
            this.farmaceuciBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem1 = new System.Windows.Forms.ToolStripButton();
            this.farmaceuciBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tabHistory = new System.Windows.Forms.TabPage();
            this.dgvHistory = new System.Windows.Forms.DataGridView();
            this.grpHistory = new System.Windows.Forms.GroupBox();
            this.btnHistoryShowBestPharmacist = new System.Windows.Forms.Button();
            this.lblHistoryCompareId = new System.Windows.Forms.Label();
            this.txtHistoryCompareId = new System.Windows.Forms.TextBox();
            this.btnHistoryCompareSell = new System.Windows.Forms.Button();
            this.btnHistoryMySellProcedure = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox_OMNIE_value = new System.Windows.Forms.TextBox();
            this.textBox_OMNIE_key = new System.Windows.Forms.TextBox();
            this.panelLogin = new System.Windows.Forms.Panel();
            this.label_text_pass = new System.Windows.Forms.Label();
            this.label_text_login = new System.Windows.Forms.Label();
            this.button_LogIn = new System.Windows.Forms.Button();
            this.textBox_Password = new System.Windows.Forms.TextBox();
            this.textBox_Login = new System.Windows.Forms.TextBox();
            this.pictureBox_LoginBG = new System.Windows.Forms.PictureBox();
            this.dostawyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bsSell = new System.Windows.Forms.BindingSource(this.components);
            this.bsSellMeds = new System.Windows.Forms.BindingSource(this.components);
            this.bsRecipeInfo = new System.Windows.Forms.BindingSource(this.components);
            this.bsClient = new System.Windows.Forms.BindingSource(this.components);
            this.bsHistory = new System.Windows.Forms.BindingSource(this.components);
            this.MainMenu.SuspendLayout();
            this.tabSell.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipeInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSellMeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSell)).BeginInit();
            this.tabMeds.SuspendLayout();
            this.grpActiveFilters.SuspendLayout();
            this.grpFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMeds)).BeginInit();
            this.contextMenuLeki.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnMeds)).BeginInit();
            this.bnMeds.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsMeds)).BeginInit();
            this.tabDelivery.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lekiBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dostawcyBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabEmployees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.farmaceuciDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.farmaceuciBindingSource1)).BeginInit();
            this.grpFarmaceuciFiltry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.farmaceuciBindingNavigator)).BeginInit();
            this.farmaceuciBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.farmaceuciBindingSource)).BeginInit();
            this.tabHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).BeginInit();
            this.grpHistory.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panelLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_LoginBG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dostawyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSellMeds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsRecipeInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.Controls.Add(this.tabSell);
            this.MainMenu.Controls.Add(this.tabMeds);
            this.MainMenu.Controls.Add(this.tabDelivery);
            this.MainMenu.Controls.Add(this.tabEmployees);
            this.MainMenu.Controls.Add(this.tabHistory);
            this.MainMenu.Controls.Add(this.tabPage1);
            this.MainMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainMenu.ItemSize = new System.Drawing.Size(143, 35);
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Multiline = true;
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.SelectedIndex = 0;
            this.MainMenu.Size = new System.Drawing.Size(864, 522);
            this.MainMenu.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Visible = false;
            // 
            // tabSell
            // 
            this.tabSell.Controls.Add(this.dtp_tabSell_filter_Date);
            this.tabSell.Controls.Add(this.cb_tabSell_filter_Value);
            this.tabSell.Controls.Add(this.cbx_tabSell_filter_Value);
            this.tabSell.Controls.Add(this.tbx_tabSell_filter_Value);
            this.tabSell.Controls.Add(this.cb_tabSell_filter_Date);
            this.tabSell.Controls.Add(this.cbx_tabSell_filter_Date);
            this.tabSell.Controls.Add(this.btnSellAddFil);
            this.tabSell.Controls.Add(this.btnSellDelFil);
            this.tabSell.Controls.Add(this.btnSellNewSell);
            this.tabSell.Controls.Add(this.dgvClient);
            this.tabSell.Controls.Add(this.label2);
            this.tabSell.Controls.Add(this.dgvRecipeInfo);
            this.tabSell.Controls.Add(this.label1);
            this.tabSell.Controls.Add(this.lbl_Sell_Sell);
            this.tabSell.Controls.Add(this.lbl_Sell_SellMeds);
            this.tabSell.Controls.Add(this.dgvSellMeds);
            this.tabSell.Controls.Add(this.dgvSell);
            this.tabSell.Controls.Add(this.shapeContainer1);
            this.tabSell.Location = new System.Drawing.Point(4, 39);
            this.tabSell.Name = "tabSell";
            this.tabSell.Padding = new System.Windows.Forms.Padding(3);
            this.tabSell.Size = new System.Drawing.Size(856, 479);
            this.tabSell.TabIndex = 0;
            this.tabSell.Text = "SPRZEDAŻ";
            this.tabSell.UseVisualStyleBackColor = true;
            // 
            // dtp_tabSell_filter_Date
            // 
            this.dtp_tabSell_filter_Date.Enabled = false;
            this.dtp_tabSell_filter_Date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_tabSell_filter_Date.Location = new System.Drawing.Point(211, 427);
            this.dtp_tabSell_filter_Date.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dtp_tabSell_filter_Date.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtp_tabSell_filter_Date.Name = "dtp_tabSell_filter_Date";
            this.dtp_tabSell_filter_Date.Size = new System.Drawing.Size(78, 20);
            this.dtp_tabSell_filter_Date.TabIndex = 28;
            // 
            // cb_tabSell_filter_Value
            // 
            this.cb_tabSell_filter_Value.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tabSell_filter_Value.Enabled = false;
            this.cb_tabSell_filter_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_tabSell_filter_Value.FormattingEnabled = true;
            this.cb_tabSell_filter_Value.ItemHeight = 15;
            this.cb_tabSell_filter_Value.Items.AddRange(new object[] {
            "większe równe",
            "większe od",
            "równe",
            "mniejsze od",
            "mniejsze równe"});
            this.cb_tabSell_filter_Value.Location = new System.Drawing.Point(100, 453);
            this.cb_tabSell_filter_Value.Name = "cb_tabSell_filter_Value";
            this.cb_tabSell_filter_Value.Size = new System.Drawing.Size(105, 23);
            this.cb_tabSell_filter_Value.TabIndex = 24;
            // 
            // cbx_tabSell_filter_Value
            // 
            this.cbx_tabSell_filter_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbx_tabSell_filter_Value.Location = new System.Drawing.Point(8, 453);
            this.cbx_tabSell_filter_Value.Name = "cbx_tabSell_filter_Value";
            this.cbx_tabSell_filter_Value.Size = new System.Drawing.Size(86, 23);
            this.cbx_tabSell_filter_Value.TabIndex = 23;
            this.cbx_tabSell_filter_Value.Text = "Wartość";
            this.cbx_tabSell_filter_Value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_tabSell_filter_Value.UseVisualStyleBackColor = true;
            this.cbx_tabSell_filter_Value.CheckedChanged += new System.EventHandler(this.cbx_tabSell_filter_Value_CheckedChanged);
            // 
            // tbx_tabSell_filter_Value
            // 
            this.tbx_tabSell_filter_Value.Enabled = false;
            this.tbx_tabSell_filter_Value.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx_tabSell_filter_Value.Location = new System.Drawing.Point(211, 453);
            this.tbx_tabSell_filter_Value.MaxLength = 12;
            this.tbx_tabSell_filter_Value.Multiline = true;
            this.tbx_tabSell_filter_Value.Name = "tbx_tabSell_filter_Value";
            this.tbx_tabSell_filter_Value.Size = new System.Drawing.Size(78, 23);
            this.tbx_tabSell_filter_Value.TabIndex = 22;
            this.tbx_tabSell_filter_Value.Text = "0";
            this.tbx_tabSell_filter_Value.Leave += new System.EventHandler(this.tbx_tabMeds_filter_Leave);
            // 
            // cb_tabSell_filter_Date
            // 
            this.cb_tabSell_filter_Date.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tabSell_filter_Date.Enabled = false;
            this.cb_tabSell_filter_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_tabSell_filter_Date.FormattingEnabled = true;
            this.cb_tabSell_filter_Date.ItemHeight = 15;
            this.cb_tabSell_filter_Date.Items.AddRange(new object[] {
            "przed dniem",
            "w dniu",
            "po dniu"});
            this.cb_tabSell_filter_Date.Location = new System.Drawing.Point(100, 426);
            this.cb_tabSell_filter_Date.Name = "cb_tabSell_filter_Date";
            this.cb_tabSell_filter_Date.Size = new System.Drawing.Size(105, 23);
            this.cb_tabSell_filter_Date.TabIndex = 21;
            // 
            // cbx_tabSell_filter_Date
            // 
            this.cbx_tabSell_filter_Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbx_tabSell_filter_Date.Location = new System.Drawing.Point(8, 426);
            this.cbx_tabSell_filter_Date.Name = "cbx_tabSell_filter_Date";
            this.cbx_tabSell_filter_Date.Size = new System.Drawing.Size(86, 23);
            this.cbx_tabSell_filter_Date.TabIndex = 20;
            this.cbx_tabSell_filter_Date.Text = "Data";
            this.cbx_tabSell_filter_Date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_tabSell_filter_Date.UseVisualStyleBackColor = true;
            this.cbx_tabSell_filter_Date.CheckedChanged += new System.EventHandler(this.cbx_tabSell_filter_Date_CheckedChanged);
            // 
            // btnSellAddFil
            // 
            this.btnSellAddFil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSellAddFil.Location = new System.Drawing.Point(295, 426);
            this.btnSellAddFil.Name = "btnSellAddFil";
            this.btnSellAddFil.Size = new System.Drawing.Size(125, 50);
            this.btnSellAddFil.TabIndex = 11;
            this.btnSellAddFil.Text = "Zastosuj filtry";
            this.btnSellAddFil.UseVisualStyleBackColor = true;
            this.btnSellAddFil.Click += new System.EventHandler(this.btnSellAddFil_Click);
            // 
            // btnSellDelFil
            // 
            this.btnSellDelFil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSellDelFil.Location = new System.Drawing.Point(426, 426);
            this.btnSellDelFil.Name = "btnSellDelFil";
            this.btnSellDelFil.Size = new System.Drawing.Size(125, 50);
            this.btnSellDelFil.TabIndex = 10;
            this.btnSellDelFil.Text = "Usuń filtry";
            this.btnSellDelFil.UseVisualStyleBackColor = true;
            this.btnSellDelFil.Click += new System.EventHandler(this.btnSellDelFil_Click);
            // 
            // btnSellNewSell
            // 
            this.btnSellNewSell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSellNewSell.Location = new System.Drawing.Point(653, 426);
            this.btnSellNewSell.Name = "btnSellNewSell";
            this.btnSellNewSell.Size = new System.Drawing.Size(200, 50);
            this.btnSellNewSell.TabIndex = 8;
            this.btnSellNewSell.Text = "Nowa sprzedaż";
            this.btnSellNewSell.UseVisualStyleBackColor = true;
            this.btnSellNewSell.Click += new System.EventHandler(this.btnSellNewSell_Click);
            // 
            // dgvClient
            // 
            this.dgvClient.AllowUserToAddRows = false;
            this.dgvClient.AllowUserToResizeRows = false;
            this.dgvClient.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvClient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClient.Location = new System.Drawing.Point(332, 343);
            this.dgvClient.MultiSelect = false;
            this.dgvClient.Name = "dgvClient";
            this.dgvClient.ReadOnly = true;
            this.dgvClient.RowHeadersVisible = false;
            this.dgvClient.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgvClient.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvClient.Size = new System.Drawing.Size(519, 60);
            this.dgvClient.TabIndex = 7;
            this.dgvClient.SelectionChanged += new System.EventHandler(this.dgv_SelectionChanged);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(331, 318);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(522, 22);
            this.label2.TabIndex = 6;
            this.label2.Text = "Klient";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvRecipeInfo
            // 
            this.dgvRecipeInfo.AllowUserToAddRows = false;
            this.dgvRecipeInfo.AllowUserToResizeRows = false;
            this.dgvRecipeInfo.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvRecipeInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvRecipeInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecipeInfo.Location = new System.Drawing.Point(334, 267);
            this.dgvRecipeInfo.MultiSelect = false;
            this.dgvRecipeInfo.Name = "dgvRecipeInfo";
            this.dgvRecipeInfo.ReadOnly = true;
            this.dgvRecipeInfo.RowHeadersVisible = false;
            this.dgvRecipeInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecipeInfo.Size = new System.Drawing.Size(519, 43);
            this.dgvRecipeInfo.TabIndex = 5;
            this.dgvRecipeInfo.SelectionChanged += new System.EventHandler(this.dgv_SelectionChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(331, 242);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(522, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Recepta";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_Sell_Sell
            // 
            this.lbl_Sell_Sell.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Sell_Sell.Location = new System.Drawing.Point(6, 3);
            this.lbl_Sell_Sell.Name = "lbl_Sell_Sell";
            this.lbl_Sell_Sell.Size = new System.Drawing.Size(319, 36);
            this.lbl_Sell_Sell.TabIndex = 3;
            this.lbl_Sell_Sell.Text = "Sprzedaże";
            this.lbl_Sell_Sell.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbl_Sell_SellMeds
            // 
            this.lbl_Sell_SellMeds.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Sell_SellMeds.Location = new System.Drawing.Point(328, 3);
            this.lbl_Sell_SellMeds.Name = "lbl_Sell_SellMeds";
            this.lbl_Sell_SellMeds.Size = new System.Drawing.Size(522, 36);
            this.lbl_Sell_SellMeds.TabIndex = 2;
            this.lbl_Sell_SellMeds.Text = "Leki w ramach sprzedaży";
            this.lbl_Sell_SellMeds.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvSellMeds
            // 
            this.dgvSellMeds.AllowUserToAddRows = false;
            this.dgvSellMeds.AllowUserToResizeRows = false;
            this.dgvSellMeds.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvSellMeds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSellMeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSellMeds.Location = new System.Drawing.Point(331, 42);
            this.dgvSellMeds.MultiSelect = false;
            this.dgvSellMeds.Name = "dgvSellMeds";
            this.dgvSellMeds.ReadOnly = true;
            this.dgvSellMeds.RowHeadersVisible = false;
            this.dgvSellMeds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSellMeds.Size = new System.Drawing.Size(519, 197);
            this.dgvSellMeds.TabIndex = 1;
            this.dgvSellMeds.SelectionChanged += new System.EventHandler(this.dgvSellMeds_SelectionChanged);
            // 
            // dgvSell
            // 
            this.dgvSell.AllowUserToAddRows = false;
            this.dgvSell.AllowUserToResizeRows = false;
            this.dgvSell.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvSell.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSell.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSell.Location = new System.Drawing.Point(8, 42);
            this.dgvSell.MultiSelect = false;
            this.dgvSell.Name = "dgvSell";
            this.dgvSell.ReadOnly = true;
            this.dgvSell.RowHeadersVisible = false;
            this.dgvSell.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSell.Size = new System.Drawing.Size(317, 361);
            this.dgvSell.TabIndex = 0;
            this.dgvSell.SelectionChanged += new System.EventHandler(this.dgvSell_SelectionChanged);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 3);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(850, 473);
            this.shapeContainer1.TabIndex = 26;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = -8;
            this.lineShape1.X2 = 856;
            this.lineShape1.Y1 = 412;
            this.lineShape1.Y2 = 412;
            // 
            // tabMeds
            // 
            this.tabMeds.Controls.Add(this.btnRemoveFilters);
            this.tabMeds.Controls.Add(this.btnSaveMedsChanges);
            this.tabMeds.Controls.Add(this.grpActiveFilters);
            this.tabMeds.Controls.Add(this.grpFilters);
            this.tabMeds.Controls.Add(this.dgvMeds);
            this.tabMeds.Controls.Add(this.bnMeds);
            this.tabMeds.Controls.Add(this.btnAddFilters);
            this.tabMeds.Location = new System.Drawing.Point(4, 39);
            this.tabMeds.Name = "tabMeds";
            this.tabMeds.Size = new System.Drawing.Size(856, 479);
            this.tabMeds.TabIndex = 2;
            this.tabMeds.Text = "LEKI";
            this.tabMeds.UseVisualStyleBackColor = true;
            // 
            // btnRemoveFilters
            // 
            this.btnRemoveFilters.Enabled = false;
            this.btnRemoveFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnRemoveFilters.Location = new System.Drawing.Point(758, 364);
            this.btnRemoveFilters.Name = "btnRemoveFilters";
            this.btnRemoveFilters.Size = new System.Drawing.Size(95, 75);
            this.btnRemoveFilters.TabIndex = 0;
            this.btnRemoveFilters.Text = "Usuń filtry";
            this.btnRemoveFilters.UseVisualStyleBackColor = false;
            this.btnRemoveFilters.Click += new System.EventHandler(this.btnRemoveFilters_Click);
            // 
            // btnSaveMedsChanges
            // 
            this.btnSaveMedsChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSaveMedsChanges.Location = new System.Drawing.Point(659, 445);
            this.btnSaveMedsChanges.Name = "btnSaveMedsChanges";
            this.btnSaveMedsChanges.Size = new System.Drawing.Size(194, 30);
            this.btnSaveMedsChanges.TabIndex = 4;
            this.btnSaveMedsChanges.Text = "Zapisz Zmiany";
            this.btnSaveMedsChanges.UseVisualStyleBackColor = true;
            this.btnSaveMedsChanges.Click += new System.EventHandler(this.btnSaveMedsChanges_Click);
            // 
            // grpActiveFilters
            // 
            this.grpActiveFilters.Controls.Add(this.listView_ActiveFilters_Meds);
            this.grpActiveFilters.Location = new System.Drawing.Point(659, 28);
            this.grpActiveFilters.Name = "grpActiveFilters";
            this.grpActiveFilters.Size = new System.Drawing.Size(194, 330);
            this.grpActiveFilters.TabIndex = 3;
            this.grpActiveFilters.TabStop = false;
            this.grpActiveFilters.Text = "Aktywne filtry";
            // 
            // listView_ActiveFilters_Meds
            // 
            this.listView_ActiveFilters_Meds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listView_ActiveFilters_Meds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_ActiveFilters_Meds.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listView_ActiveFilters_Meds.Location = new System.Drawing.Point(3, 16);
            this.listView_ActiveFilters_Meds.Name = "listView_ActiveFilters_Meds";
            this.listView_ActiveFilters_Meds.Size = new System.Drawing.Size(188, 311);
            this.listView_ActiveFilters_Meds.TabIndex = 0;
            this.listView_ActiveFilters_Meds.UseCompatibleStateImageBehavior = false;
            // 
            // grpFilters
            // 
            this.grpFilters.Controls.Add(this.tbx_tabMeds_filter_Price);
            this.grpFilters.Controls.Add(this.tbx_tabMeds_filter_Count);
            this.grpFilters.Controls.Add(this.cb_tabMeds_filter_Recipe);
            this.grpFilters.Controls.Add(this.cb_tabMeds_filter_Price);
            this.grpFilters.Controls.Add(this.cb_tabMeds_filter_Count);
            this.grpFilters.Controls.Add(this.cbx_tabMeds_filter_Recipe);
            this.grpFilters.Controls.Add(this.cbx_tabMeds_filter_Price);
            this.grpFilters.Controls.Add(this.cbx_tabMeds_filter_Count);
            this.grpFilters.Controls.Add(this.tbx_tabMeds_filter_Nr);
            this.grpFilters.Controls.Add(this.tbx_tabMeds_filter_Name);
            this.grpFilters.Controls.Add(this.tbx_tabMeds_filter_Id);
            this.grpFilters.Controls.Add(this.cbx_tabMeds_filter_Nr);
            this.grpFilters.Controls.Add(this.cbx_tabMeds_filter_Name);
            this.grpFilters.Controls.Add(this.cbx_tabMeds_filter_Id);
            this.grpFilters.Controls.Add(this.cb_tabMeds_filter_Nr);
            this.grpFilters.Controls.Add(this.cb_tabMeds_filter_Name);
            this.grpFilters.Controls.Add(this.cb_tabMeds_filter_Id);
            this.grpFilters.Location = new System.Drawing.Point(3, 358);
            this.grpFilters.Name = "grpFilters";
            this.grpFilters.Size = new System.Drawing.Size(651, 118);
            this.grpFilters.TabIndex = 2;
            this.grpFilters.TabStop = false;
            this.grpFilters.Text = "Filtry";
            // 
            // tbx_tabMeds_filter_Price
            // 
            this.tbx_tabMeds_filter_Price.Enabled = false;
            this.tbx_tabMeds_filter_Price.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx_tabMeds_filter_Price.Location = new System.Drawing.Point(576, 52);
            this.tbx_tabMeds_filter_Price.MaxLength = 12;
            this.tbx_tabMeds_filter_Price.Multiline = true;
            this.tbx_tabMeds_filter_Price.Name = "tbx_tabMeds_filter_Price";
            this.tbx_tabMeds_filter_Price.Size = new System.Drawing.Size(69, 23);
            this.tbx_tabMeds_filter_Price.TabIndex = 19;
            this.tbx_tabMeds_filter_Price.Text = "0";
            this.tbx_tabMeds_filter_Price.Leave += new System.EventHandler(this.tbx_tabMeds_filter_Leave);
            // 
            // tbx_tabMeds_filter_Count
            // 
            this.tbx_tabMeds_filter_Count.Enabled = false;
            this.tbx_tabMeds_filter_Count.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx_tabMeds_filter_Count.Location = new System.Drawing.Point(576, 23);
            this.tbx_tabMeds_filter_Count.MaxLength = 10;
            this.tbx_tabMeds_filter_Count.Multiline = true;
            this.tbx_tabMeds_filter_Count.Name = "tbx_tabMeds_filter_Count";
            this.tbx_tabMeds_filter_Count.Size = new System.Drawing.Size(69, 23);
            this.tbx_tabMeds_filter_Count.TabIndex = 18;
            this.tbx_tabMeds_filter_Count.Text = "0";
            this.tbx_tabMeds_filter_Count.Leave += new System.EventHandler(this.tbx_tabMeds_filter_Leave);
            // 
            // cb_tabMeds_filter_Recipe
            // 
            this.cb_tabMeds_filter_Recipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tabMeds_filter_Recipe.Enabled = false;
            this.cb_tabMeds_filter_Recipe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_tabMeds_filter_Recipe.FormattingEnabled = true;
            this.cb_tabMeds_filter_Recipe.ItemHeight = 15;
            this.cb_tabMeds_filter_Recipe.Items.AddRange(new object[] {
            "tak",
            "nie"});
            this.cb_tabMeds_filter_Recipe.Location = new System.Drawing.Point(465, 81);
            this.cb_tabMeds_filter_Recipe.Name = "cb_tabMeds_filter_Recipe";
            this.cb_tabMeds_filter_Recipe.Size = new System.Drawing.Size(105, 23);
            this.cb_tabMeds_filter_Recipe.TabIndex = 17;
            // 
            // cb_tabMeds_filter_Price
            // 
            this.cb_tabMeds_filter_Price.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tabMeds_filter_Price.Enabled = false;
            this.cb_tabMeds_filter_Price.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_tabMeds_filter_Price.FormattingEnabled = true;
            this.cb_tabMeds_filter_Price.ItemHeight = 15;
            this.cb_tabMeds_filter_Price.Items.AddRange(new object[] {
            "większe równe",
            "większe od",
            "równe",
            "mniejsze od",
            "mniejsze równe"});
            this.cb_tabMeds_filter_Price.Location = new System.Drawing.Point(465, 52);
            this.cb_tabMeds_filter_Price.Name = "cb_tabMeds_filter_Price";
            this.cb_tabMeds_filter_Price.Size = new System.Drawing.Size(105, 23);
            this.cb_tabMeds_filter_Price.TabIndex = 16;
            // 
            // cb_tabMeds_filter_Count
            // 
            this.cb_tabMeds_filter_Count.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tabMeds_filter_Count.Enabled = false;
            this.cb_tabMeds_filter_Count.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_tabMeds_filter_Count.FormattingEnabled = true;
            this.cb_tabMeds_filter_Count.ItemHeight = 15;
            this.cb_tabMeds_filter_Count.Items.AddRange(new object[] {
            "większe równe",
            "większe od",
            "równe",
            "mniejsze od",
            "mniejsze równe"});
            this.cb_tabMeds_filter_Count.Location = new System.Drawing.Point(465, 23);
            this.cb_tabMeds_filter_Count.Name = "cb_tabMeds_filter_Count";
            this.cb_tabMeds_filter_Count.Size = new System.Drawing.Size(105, 23);
            this.cb_tabMeds_filter_Count.TabIndex = 15;
            // 
            // cbx_tabMeds_filter_Recipe
            // 
            this.cbx_tabMeds_filter_Recipe.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbx_tabMeds_filter_Recipe.Location = new System.Drawing.Point(373, 81);
            this.cbx_tabMeds_filter_Recipe.Name = "cbx_tabMeds_filter_Recipe";
            this.cbx_tabMeds_filter_Recipe.Size = new System.Drawing.Size(86, 23);
            this.cbx_tabMeds_filter_Recipe.TabIndex = 14;
            this.cbx_tabMeds_filter_Recipe.Text = "Recepta";
            this.cbx_tabMeds_filter_Recipe.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_tabMeds_filter_Recipe.UseVisualStyleBackColor = true;
            this.cbx_tabMeds_filter_Recipe.CheckedChanged += new System.EventHandler(this.cbx_tabMeds_filter_Recipe_CheckedChanged);
            // 
            // cbx_tabMeds_filter_Price
            // 
            this.cbx_tabMeds_filter_Price.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbx_tabMeds_filter_Price.Location = new System.Drawing.Point(373, 52);
            this.cbx_tabMeds_filter_Price.Name = "cbx_tabMeds_filter_Price";
            this.cbx_tabMeds_filter_Price.Size = new System.Drawing.Size(86, 23);
            this.cbx_tabMeds_filter_Price.TabIndex = 13;
            this.cbx_tabMeds_filter_Price.Text = "Cena";
            this.cbx_tabMeds_filter_Price.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_tabMeds_filter_Price.UseVisualStyleBackColor = true;
            this.cbx_tabMeds_filter_Price.CheckedChanged += new System.EventHandler(this.cbx_tabMeds_filter_Price_CheckedChanged);
            // 
            // cbx_tabMeds_filter_Count
            // 
            this.cbx_tabMeds_filter_Count.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbx_tabMeds_filter_Count.Location = new System.Drawing.Point(373, 23);
            this.cbx_tabMeds_filter_Count.Name = "cbx_tabMeds_filter_Count";
            this.cbx_tabMeds_filter_Count.Size = new System.Drawing.Size(86, 23);
            this.cbx_tabMeds_filter_Count.TabIndex = 12;
            this.cbx_tabMeds_filter_Count.Text = "Ilosc";
            this.cbx_tabMeds_filter_Count.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_tabMeds_filter_Count.UseVisualStyleBackColor = true;
            this.cbx_tabMeds_filter_Count.CheckedChanged += new System.EventHandler(this.cbx_tabMeds_filter_Count_CheckedChanged);
            // 
            // tbx_tabMeds_filter_Nr
            // 
            this.tbx_tabMeds_filter_Nr.Enabled = false;
            this.tbx_tabMeds_filter_Nr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx_tabMeds_filter_Nr.Location = new System.Drawing.Point(213, 81);
            this.tbx_tabMeds_filter_Nr.MaxLength = 8;
            this.tbx_tabMeds_filter_Nr.Multiline = true;
            this.tbx_tabMeds_filter_Nr.Name = "tbx_tabMeds_filter_Nr";
            this.tbx_tabMeds_filter_Nr.Size = new System.Drawing.Size(125, 23);
            this.tbx_tabMeds_filter_Nr.TabIndex = 11;
            // 
            // tbx_tabMeds_filter_Name
            // 
            this.tbx_tabMeds_filter_Name.Enabled = false;
            this.tbx_tabMeds_filter_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx_tabMeds_filter_Name.Location = new System.Drawing.Point(213, 52);
            this.tbx_tabMeds_filter_Name.MaxLength = 30;
            this.tbx_tabMeds_filter_Name.Multiline = true;
            this.tbx_tabMeds_filter_Name.Name = "tbx_tabMeds_filter_Name";
            this.tbx_tabMeds_filter_Name.Size = new System.Drawing.Size(125, 23);
            this.tbx_tabMeds_filter_Name.TabIndex = 10;
            // 
            // tbx_tabMeds_filter_Id
            // 
            this.tbx_tabMeds_filter_Id.Enabled = false;
            this.tbx_tabMeds_filter_Id.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbx_tabMeds_filter_Id.Location = new System.Drawing.Point(213, 23);
            this.tbx_tabMeds_filter_Id.MaxLength = 8;
            this.tbx_tabMeds_filter_Id.Multiline = true;
            this.tbx_tabMeds_filter_Id.Name = "tbx_tabMeds_filter_Id";
            this.tbx_tabMeds_filter_Id.Size = new System.Drawing.Size(125, 23);
            this.tbx_tabMeds_filter_Id.TabIndex = 9;
            // 
            // cbx_tabMeds_filter_Nr
            // 
            this.cbx_tabMeds_filter_Nr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbx_tabMeds_filter_Nr.Location = new System.Drawing.Point(10, 81);
            this.cbx_tabMeds_filter_Nr.Name = "cbx_tabMeds_filter_Nr";
            this.cbx_tabMeds_filter_Nr.Size = new System.Drawing.Size(86, 23);
            this.cbx_tabMeds_filter_Nr.TabIndex = 8;
            this.cbx_tabMeds_filter_Nr.Text = "Nr_partii";
            this.cbx_tabMeds_filter_Nr.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_tabMeds_filter_Nr.UseVisualStyleBackColor = true;
            this.cbx_tabMeds_filter_Nr.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cbx_tabMeds_filter_Name
            // 
            this.cbx_tabMeds_filter_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbx_tabMeds_filter_Name.Location = new System.Drawing.Point(10, 52);
            this.cbx_tabMeds_filter_Name.Name = "cbx_tabMeds_filter_Name";
            this.cbx_tabMeds_filter_Name.Size = new System.Drawing.Size(86, 23);
            this.cbx_tabMeds_filter_Name.TabIndex = 7;
            this.cbx_tabMeds_filter_Name.Text = "Nazwa";
            this.cbx_tabMeds_filter_Name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_tabMeds_filter_Name.UseVisualStyleBackColor = true;
            this.cbx_tabMeds_filter_Name.CheckedChanged += new System.EventHandler(this.cbx_tabMeds_filter_Name_CheckedChanged);
            // 
            // cbx_tabMeds_filter_Id
            // 
            this.cbx_tabMeds_filter_Id.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbx_tabMeds_filter_Id.Location = new System.Drawing.Point(10, 23);
            this.cbx_tabMeds_filter_Id.Name = "cbx_tabMeds_filter_Id";
            this.cbx_tabMeds_filter_Id.Size = new System.Drawing.Size(86, 23);
            this.cbx_tabMeds_filter_Id.TabIndex = 6;
            this.cbx_tabMeds_filter_Id.Text = "Id_Towaru";
            this.cbx_tabMeds_filter_Id.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbx_tabMeds_filter_Id.UseVisualStyleBackColor = true;
            this.cbx_tabMeds_filter_Id.CheckedChanged += new System.EventHandler(this.cbx_tabMeds_filter_Id_CheckedChanged);
            // 
            // cb_tabMeds_filter_Nr
            // 
            this.cb_tabMeds_filter_Nr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tabMeds_filter_Nr.Enabled = false;
            this.cb_tabMeds_filter_Nr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_tabMeds_filter_Nr.FormattingEnabled = true;
            this.cb_tabMeds_filter_Nr.ItemHeight = 15;
            this.cb_tabMeds_filter_Nr.Items.AddRange(new object[] {
            "zawiera",
            "zaczyna się od",
            "kończy się"});
            this.cb_tabMeds_filter_Nr.Location = new System.Drawing.Point(102, 81);
            this.cb_tabMeds_filter_Nr.Name = "cb_tabMeds_filter_Nr";
            this.cb_tabMeds_filter_Nr.Size = new System.Drawing.Size(105, 23);
            this.cb_tabMeds_filter_Nr.TabIndex = 2;
            // 
            // cb_tabMeds_filter_Name
            // 
            this.cb_tabMeds_filter_Name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tabMeds_filter_Name.Enabled = false;
            this.cb_tabMeds_filter_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_tabMeds_filter_Name.FormattingEnabled = true;
            this.cb_tabMeds_filter_Name.ItemHeight = 15;
            this.cb_tabMeds_filter_Name.Items.AddRange(new object[] {
            "zawiera",
            "zaczyna się od",
            "kończy się"});
            this.cb_tabMeds_filter_Name.Location = new System.Drawing.Point(102, 52);
            this.cb_tabMeds_filter_Name.Name = "cb_tabMeds_filter_Name";
            this.cb_tabMeds_filter_Name.Size = new System.Drawing.Size(105, 23);
            this.cb_tabMeds_filter_Name.TabIndex = 1;
            // 
            // cb_tabMeds_filter_Id
            // 
            this.cb_tabMeds_filter_Id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_tabMeds_filter_Id.Enabled = false;
            this.cb_tabMeds_filter_Id.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_tabMeds_filter_Id.FormattingEnabled = true;
            this.cb_tabMeds_filter_Id.ItemHeight = 15;
            this.cb_tabMeds_filter_Id.Items.AddRange(new object[] {
            "zawiera",
            "zaczyna się od",
            "kończy się"});
            this.cb_tabMeds_filter_Id.Location = new System.Drawing.Point(102, 23);
            this.cb_tabMeds_filter_Id.Name = "cb_tabMeds_filter_Id";
            this.cb_tabMeds_filter_Id.Size = new System.Drawing.Size(105, 23);
            this.cb_tabMeds_filter_Id.TabIndex = 0;
            // 
            // dgvMeds
            // 
            this.dgvMeds.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvMeds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMeds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMeds.ContextMenuStrip = this.contextMenuLeki;
            this.dgvMeds.Location = new System.Drawing.Point(3, 28);
            this.dgvMeds.Name = "dgvMeds";
            this.dgvMeds.RowHeadersVisible = false;
            this.dgvMeds.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMeds.Size = new System.Drawing.Size(650, 330);
            this.dgvMeds.TabIndex = 1;
            this.dgvMeds.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvMeds_CellValidating);
            // 
            // contextMenuLeki
            // 
            this.contextMenuLeki.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.additionalMedInfo});
            this.contextMenuLeki.Name = "contextMenuLeki";
            this.contextMenuLeki.Size = new System.Drawing.Size(295, 26);
            // 
            // additionalMedInfo
            // 
            this.additionalMedInfo.Name = "additionalMedInfo";
            this.additionalMedInfo.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D)));
            this.additionalMedInfo.Size = new System.Drawing.Size(294, 22);
            this.additionalMedInfo.Text = "&Dodatkowe informacje o leku";
            this.additionalMedInfo.Click += new System.EventHandler(this.additionalMedInfo_Click);
            // 
            // bnMeds
            // 
            this.bnMeds.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bnMeds.BindingSource = this.bsMeds;
            this.bnMeds.CountItem = this.bindingNavigatorCountItem;
            this.bnMeds.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bnMeds.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.bnMeds.Location = new System.Drawing.Point(0, 0);
            this.bnMeds.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bnMeds.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bnMeds.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bnMeds.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bnMeds.Name = "bnMeds";
            this.bnMeds.PositionItem = this.bindingNavigatorPositionItem;
            this.bnMeds.Size = new System.Drawing.Size(856, 25);
            this.bnMeds.TabIndex = 0;
            this.bnMeds.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAddFilters
            // 
            this.btnAddFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnAddFilters.Location = new System.Drawing.Point(659, 364);
            this.btnAddFilters.Name = "btnAddFilters";
            this.btnAddFilters.Size = new System.Drawing.Size(95, 75);
            this.btnAddFilters.TabIndex = 0;
            this.btnAddFilters.Text = "Zastosuj filtry";
            this.btnAddFilters.UseVisualStyleBackColor = true;
            this.btnAddFilters.Click += new System.EventHandler(this.btnAddFilter_Click);
            // 
            // tabDelivery
            // 
            this.tabDelivery.Controls.Add(this.groupBox2);
            this.tabDelivery.Controls.Add(this.groupBox1);
            this.tabDelivery.Location = new System.Drawing.Point(4, 39);
            this.tabDelivery.Name = "tabDelivery";
            this.tabDelivery.Padding = new System.Windows.Forms.Padding(3);
            this.tabDelivery.Size = new System.Drawing.Size(856, 479);
            this.tabDelivery.TabIndex = 1;
            this.tabDelivery.Text = "DOSTAWA";
            this.tabDelivery.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_zmien_cene);
            this.groupBox2.Controls.Add(this.TBox_nowa_cena);
            this.groupBox2.Controls.Add(this.btn_wybierz_dostawce);
            this.groupBox2.Controls.Add(this.btn_odswiez);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.TBox_ilosc);
            this.groupBox2.Controls.Add(this.MTBox_data_waznosci);
            this.groupBox2.Controls.Add(this.btn_dodaj_lek);
            this.groupBox2.Controls.Add(this.btn_anuluj_dostawe);
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Controls.Add(this.CBox_dostawca);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.btn_zapisz_dostawe);
            this.groupBox2.Location = new System.Drawing.Point(0, 65);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(859, 413);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Przyjęcie dostawy";
            // 
            // btn_zmien_cene
            // 
            this.btn_zmien_cene.Location = new System.Drawing.Point(692, 129);
            this.btn_zmien_cene.Name = "btn_zmien_cene";
            this.btn_zmien_cene.Size = new System.Drawing.Size(75, 23);
            this.btn_zmien_cene.TabIndex = 14;
            this.btn_zmien_cene.Text = "Zmień cenę";
            this.btn_zmien_cene.UseVisualStyleBackColor = true;
            this.btn_zmien_cene.Click += new System.EventHandler(this.btn_zmien_cene_Click);
            // 
            // TBox_nowa_cena
            // 
            this.TBox_nowa_cena.Location = new System.Drawing.Point(607, 129);
            this.TBox_nowa_cena.Name = "TBox_nowa_cena";
            this.TBox_nowa_cena.Size = new System.Drawing.Size(63, 20);
            this.TBox_nowa_cena.TabIndex = 13;
            // 
            // btn_wybierz_dostawce
            // 
            this.btn_wybierz_dostawce.Location = new System.Drawing.Point(159, 35);
            this.btn_wybierz_dostawce.Name = "btn_wybierz_dostawce";
            this.btn_wybierz_dostawce.Size = new System.Drawing.Size(65, 20);
            this.btn_wybierz_dostawce.TabIndex = 3;
            this.btn_wybierz_dostawce.Text = "Wybierz";
            this.btn_wybierz_dostawce.UseVisualStyleBackColor = true;
            this.btn_wybierz_dostawce.Click += new System.EventHandler(this.btn_wybierz_dostawce_Click);
            // 
            // btn_odswiez
            // 
            this.btn_odswiez.Location = new System.Drawing.Point(249, 35);
            this.btn_odswiez.Name = "btn_odswiez";
            this.btn_odswiez.Size = new System.Drawing.Size(74, 20);
            this.btn_odswiez.TabIndex = 11;
            this.btn_odswiez.Text = "Odśwież";
            this.btn_odswiez.UseVisualStyleBackColor = true;
            this.btn_odswiez.Click += new System.EventHandler(this.btn_odswiez_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(667, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Data ważności";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(558, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ilość";
            // 
            // TBox_ilosc
            // 
            this.TBox_ilosc.Location = new System.Drawing.Point(561, 35);
            this.TBox_ilosc.Name = "TBox_ilosc";
            this.TBox_ilosc.Size = new System.Drawing.Size(100, 20);
            this.TBox_ilosc.TabIndex = 4;
            // 
            // MTBox_data_waznosci
            // 
            this.MTBox_data_waznosci.Location = new System.Drawing.Point(667, 35);
            this.MTBox_data_waznosci.Mask = "0000/00/00";
            this.MTBox_data_waznosci.Name = "MTBox_data_waznosci";
            this.MTBox_data_waznosci.Size = new System.Drawing.Size(100, 20);
            this.MTBox_data_waznosci.TabIndex = 5;
            // 
            // btn_dodaj_lek
            // 
            this.btn_dodaj_lek.Enabled = false;
            this.btn_dodaj_lek.Location = new System.Drawing.Point(773, 35);
            this.btn_dodaj_lek.Name = "btn_dodaj_lek";
            this.btn_dodaj_lek.Size = new System.Drawing.Size(75, 21);
            this.btn_dodaj_lek.TabIndex = 6;
            this.btn_dodaj_lek.Text = "Dodaj";
            this.btn_dodaj_lek.UseVisualStyleBackColor = true;
            this.btn_dodaj_lek.Click += new System.EventHandler(this.btn_dodaj_lek_Click);
            // 
            // btn_anuluj_dostawe
            // 
            this.btn_anuluj_dostawe.Enabled = false;
            this.btn_anuluj_dostawe.Location = new System.Drawing.Point(607, 386);
            this.btn_anuluj_dostawe.Name = "btn_anuluj_dostawe";
            this.btn_anuluj_dostawe.Size = new System.Drawing.Size(75, 23);
            this.btn_anuluj_dostawe.TabIndex = 7;
            this.btn_anuluj_dostawe.Text = "Anuluj";
            this.btn_anuluj_dostawe.UseVisualStyleBackColor = true;
            this.btn_anuluj_dostawe.Click += new System.EventHandler(this.btn_anuluj_dostawe_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nazwaDataGridViewTextBoxColumn,
            this.iloscDataGridViewTextBoxColumn,
            this.nrpartiiDataGridViewTextBoxColumn,
            this.cenaDataGridViewTextBoxColumn,
            this.narecepteDataGridViewCheckBoxColumn});
            this.dataGridView1.DataSource = this.lekiBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(9, 94);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(549, 291);
            this.dataGridView1.TabIndex = 11;
            // 
            // nazwaDataGridViewTextBoxColumn
            // 
            this.nazwaDataGridViewTextBoxColumn.DataPropertyName = "Nazwa";
            this.nazwaDataGridViewTextBoxColumn.HeaderText = "Nazwa";
            this.nazwaDataGridViewTextBoxColumn.Name = "nazwaDataGridViewTextBoxColumn";
            this.nazwaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iloscDataGridViewTextBoxColumn
            // 
            this.iloscDataGridViewTextBoxColumn.DataPropertyName = "Ilosc";
            this.iloscDataGridViewTextBoxColumn.HeaderText = "Ilosc";
            this.iloscDataGridViewTextBoxColumn.Name = "iloscDataGridViewTextBoxColumn";
            this.iloscDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nrpartiiDataGridViewTextBoxColumn
            // 
            this.nrpartiiDataGridViewTextBoxColumn.DataPropertyName = "Nr_partii";
            this.nrpartiiDataGridViewTextBoxColumn.HeaderText = "Nr_partii";
            this.nrpartiiDataGridViewTextBoxColumn.Name = "nrpartiiDataGridViewTextBoxColumn";
            this.nrpartiiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cenaDataGridViewTextBoxColumn
            // 
            this.cenaDataGridViewTextBoxColumn.DataPropertyName = "Cena";
            this.cenaDataGridViewTextBoxColumn.HeaderText = "Cena";
            this.cenaDataGridViewTextBoxColumn.Name = "cenaDataGridViewTextBoxColumn";
            this.cenaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // narecepteDataGridViewCheckBoxColumn
            // 
            this.narecepteDataGridViewCheckBoxColumn.DataPropertyName = "Na_recepte";
            this.narecepteDataGridViewCheckBoxColumn.HeaderText = "Na_recepte";
            this.narecepteDataGridViewCheckBoxColumn.Name = "narecepteDataGridViewCheckBoxColumn";
            this.narecepteDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // lekiBindingSource
            // 
            this.lekiBindingSource.DataSource = typeof(leki);
            // 
            // CBox_dostawca
            // 
            this.CBox_dostawca.DataSource = this.dostawcyBindingSource;
            this.CBox_dostawca.DisplayMember = "Nazwa";
            this.CBox_dostawca.FormattingEnabled = true;
            this.CBox_dostawca.Location = new System.Drawing.Point(9, 35);
            this.CBox_dostawca.Name = "CBox_dostawca";
            this.CBox_dostawca.Size = new System.Drawing.Size(121, 21);
            this.CBox_dostawca.TabIndex = 2;
            this.CBox_dostawca.ValueMember = "ID_Dostawcy";
            // 
            // dostawcyBindingSource
            // 
            this.dostawcyBindingSource.DataSource = typeof(dostawcy);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Dostawca";
            // 
            // btn_zapisz_dostawe
            // 
            this.btn_zapisz_dostawe.Enabled = false;
            this.btn_zapisz_dostawe.Location = new System.Drawing.Point(736, 383);
            this.btn_zapisz_dostawe.Name = "btn_zapisz_dostawe";
            this.btn_zapisz_dostawe.Size = new System.Drawing.Size(118, 29);
            this.btn_zapisz_dostawe.TabIndex = 6;
            this.btn_zapisz_dostawe.Text = "Zapisz";
            this.btn_zapisz_dostawe.UseVisualStyleBackColor = true;
            this.btn_zapisz_dostawe.Click += new System.EventHandler(this.btn_zapisz_dostawe_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.ndostawcaAdres);
            this.groupBox1.Controls.Add(this.ndostawcaKod);
            this.groupBox1.Controls.Add(this.ndostawcaMiejscowosc);
            this.groupBox1.Controls.Add(this.ndostawcaTelefon);
            this.groupBox1.Controls.Add(this.ndostawcaNazwa);
            this.groupBox1.Location = new System.Drawing.Point(0, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(855, 61);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dodaj nowego dostawcę";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(560, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Telefon";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(133, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Adres";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(272, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Kod pocztowy";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(420, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Miejscowość";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Nazwa";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(777, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 24);
            this.button1.TabIndex = 5;
            this.button1.Text = "Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ndostawcaAdres
            // 
            this.ndostawcaAdres.Location = new System.Drawing.Point(136, 36);
            this.ndostawcaAdres.Name = "ndostawcaAdres";
            this.ndostawcaAdres.Size = new System.Drawing.Size(100, 20);
            this.ndostawcaAdres.TabIndex = 1;
            // 
            // ndostawcaKod
            // 
            this.ndostawcaKod.Location = new System.Drawing.Point(275, 36);
            this.ndostawcaKod.Name = "ndostawcaKod";
            this.ndostawcaKod.Size = new System.Drawing.Size(100, 20);
            this.ndostawcaKod.TabIndex = 2;
            // 
            // ndostawcaMiejscowosc
            // 
            this.ndostawcaMiejscowosc.Location = new System.Drawing.Point(423, 36);
            this.ndostawcaMiejscowosc.Name = "ndostawcaMiejscowosc";
            this.ndostawcaMiejscowosc.Size = new System.Drawing.Size(100, 20);
            this.ndostawcaMiejscowosc.TabIndex = 3;
            // 
            // ndostawcaTelefon
            // 
            this.ndostawcaTelefon.Location = new System.Drawing.Point(561, 36);
            this.ndostawcaTelefon.Name = "ndostawcaTelefon";
            this.ndostawcaTelefon.Size = new System.Drawing.Size(100, 20);
            this.ndostawcaTelefon.TabIndex = 4;
            // 
            // ndostawcaNazwa
            // 
            this.ndostawcaNazwa.Location = new System.Drawing.Point(8, 36);
            this.ndostawcaNazwa.Name = "ndostawcaNazwa";
            this.ndostawcaNazwa.Size = new System.Drawing.Size(100, 20);
            this.ndostawcaNazwa.TabIndex = 0;
            // 
            // tabEmployees
            // 
            this.tabEmployees.AutoScroll = true;
            this.tabEmployees.Controls.Add(this.farmaceuciDataGridView);
            this.tabEmployees.Controls.Add(this.grpFarmaceuciFiltry);
            this.tabEmployees.Controls.Add(this.btnSaveFarmaceuci);
            this.tabEmployees.Controls.Add(this.farmaceuciBindingNavigator);
            this.tabEmployees.Location = new System.Drawing.Point(4, 39);
            this.tabEmployees.Name = "tabEmployees";
            this.tabEmployees.Size = new System.Drawing.Size(856, 479);
            this.tabEmployees.TabIndex = 3;
            this.tabEmployees.Text = "PRACOWNICY";
            this.tabEmployees.UseVisualStyleBackColor = true;
            // 
            // farmaceuciDataGridView
            // 
            this.farmaceuciDataGridView.AutoGenerateColumns = false;
            this.farmaceuciDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.farmaceuciDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nazwiskoDataGridViewTextBoxColumn,
            this.imieDataGridViewTextBoxColumn,
            this.dataurDataGridViewTextBoxColumn,
            this.adresDataGridViewTextBoxColumn,
            this.kodpocztDataGridViewTextBoxColumn,
            this.miejscowoscDataGridViewTextBoxColumn,
            this.telefonDataGridViewTextBoxColumn,
            this.iDFarmaceutyDataGridViewTextBoxColumn});
            this.farmaceuciDataGridView.DataSource = this.farmaceuciBindingSource1;
            this.farmaceuciDataGridView.Location = new System.Drawing.Point(0, 23);
            this.farmaceuciDataGridView.Name = "farmaceuciDataGridView";
            this.farmaceuciDataGridView.Size = new System.Drawing.Size(856, 225);
            this.farmaceuciDataGridView.TabIndex = 5;
            this.farmaceuciDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.farmaceuciDataGridView_CellValidating);
            // 
            // nazwiskoDataGridViewTextBoxColumn
            // 
            this.nazwiskoDataGridViewTextBoxColumn.DataPropertyName = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn.HeaderText = "Nazwisko";
            this.nazwiskoDataGridViewTextBoxColumn.Name = "nazwiskoDataGridViewTextBoxColumn";
            // 
            // imieDataGridViewTextBoxColumn
            // 
            this.imieDataGridViewTextBoxColumn.DataPropertyName = "Imie";
            this.imieDataGridViewTextBoxColumn.HeaderText = "Imie";
            this.imieDataGridViewTextBoxColumn.Name = "imieDataGridViewTextBoxColumn";
            // 
            // dataurDataGridViewTextBoxColumn
            // 
            this.dataurDataGridViewTextBoxColumn.DataPropertyName = "Data_ur";
            this.dataurDataGridViewTextBoxColumn.HeaderText = "Data_ur";
            this.dataurDataGridViewTextBoxColumn.Name = "dataurDataGridViewTextBoxColumn";
            // 
            // adresDataGridViewTextBoxColumn
            // 
            this.adresDataGridViewTextBoxColumn.DataPropertyName = "Adres";
            this.adresDataGridViewTextBoxColumn.HeaderText = "Adres";
            this.adresDataGridViewTextBoxColumn.Name = "adresDataGridViewTextBoxColumn";
            // 
            // kodpocztDataGridViewTextBoxColumn
            // 
            this.kodpocztDataGridViewTextBoxColumn.DataPropertyName = "Kod_poczt";
            this.kodpocztDataGridViewTextBoxColumn.HeaderText = "Kod_poczt";
            this.kodpocztDataGridViewTextBoxColumn.Name = "kodpocztDataGridViewTextBoxColumn";
            // 
            // miejscowoscDataGridViewTextBoxColumn
            // 
            this.miejscowoscDataGridViewTextBoxColumn.DataPropertyName = "Miejscowosc";
            this.miejscowoscDataGridViewTextBoxColumn.HeaderText = "Miejscowosc";
            this.miejscowoscDataGridViewTextBoxColumn.Name = "miejscowoscDataGridViewTextBoxColumn";
            // 
            // telefonDataGridViewTextBoxColumn
            // 
            this.telefonDataGridViewTextBoxColumn.DataPropertyName = "Telefon";
            this.telefonDataGridViewTextBoxColumn.HeaderText = "Telefon";
            this.telefonDataGridViewTextBoxColumn.Name = "telefonDataGridViewTextBoxColumn";
            // 
            // iDFarmaceutyDataGridViewTextBoxColumn
            // 
            this.iDFarmaceutyDataGridViewTextBoxColumn.DataPropertyName = "ID_Farmaceuty";
            this.iDFarmaceutyDataGridViewTextBoxColumn.HeaderText = "ID_Farmaceuty";
            this.iDFarmaceutyDataGridViewTextBoxColumn.Name = "iDFarmaceutyDataGridViewTextBoxColumn";
            // 
            // farmaceuciBindingSource1
            // 
            this.farmaceuciBindingSource1.DataSource = typeof(farmaceuci);
            // 
            // grpFarmaceuciFiltry
            // 
            this.grpFarmaceuciFiltry.Controls.Add(this.tableFiltry);
            this.grpFarmaceuciFiltry.Controls.Add(this.comboBoxFarmaceuciKolumny);
            this.grpFarmaceuciFiltry.Controls.Add(this.btnFarmaceuciAddSelectedFIlter);
            this.grpFarmaceuciFiltry.Controls.Add(this.btnFarmaceuciKasujFiltry);
            this.grpFarmaceuciFiltry.Controls.Add(this.btnFarmaceuciFiltruj);
            this.grpFarmaceuciFiltry.Location = new System.Drawing.Point(224, 254);
            this.grpFarmaceuciFiltry.Name = "grpFarmaceuciFiltry";
            this.grpFarmaceuciFiltry.Size = new System.Drawing.Size(629, 217);
            this.grpFarmaceuciFiltry.TabIndex = 4;
            this.grpFarmaceuciFiltry.TabStop = false;
            this.grpFarmaceuciFiltry.Text = "Filtrowanie";
            // 
            // tableFiltry
            // 
            this.tableFiltry.ColumnCount = 2;
            this.tableFiltry.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableFiltry.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.tableFiltry.Location = new System.Drawing.Point(213, 19);
            this.tableFiltry.Name = "tableFiltry";
            this.tableFiltry.RowCount = 1;
            this.tableFiltry.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableFiltry.Size = new System.Drawing.Size(275, 192);
            this.tableFiltry.TabIndex = 6;
            // 
            // comboBoxFarmaceuciKolumny
            // 
            this.comboBoxFarmaceuciKolumny.FormattingEnabled = true;
            this.comboBoxFarmaceuciKolumny.Location = new System.Drawing.Point(86, 19);
            this.comboBoxFarmaceuciKolumny.Name = "comboBoxFarmaceuciKolumny";
            this.comboBoxFarmaceuciKolumny.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFarmaceuciKolumny.TabIndex = 5;
            this.comboBoxFarmaceuciKolumny.SelectionChangeCommitted += new System.EventHandler(this.comboBoxFarmaceuciKolumny_SelectionChangeCommitted);
            // 
            // btnFarmaceuciAddSelectedFIlter
            // 
            this.btnFarmaceuciAddSelectedFIlter.Location = new System.Drawing.Point(132, 46);
            this.btnFarmaceuciAddSelectedFIlter.Name = "btnFarmaceuciAddSelectedFIlter";
            this.btnFarmaceuciAddSelectedFIlter.Size = new System.Drawing.Size(75, 23);
            this.btnFarmaceuciAddSelectedFIlter.TabIndex = 4;
            this.btnFarmaceuciAddSelectedFIlter.Text = "Dodaj Filtr";
            this.btnFarmaceuciAddSelectedFIlter.UseVisualStyleBackColor = true;
            this.btnFarmaceuciAddSelectedFIlter.Click += new System.EventHandler(this.btnFarmaceuciAddSelectedFIlter_Click);
            // 
            // btnFarmaceuciKasujFiltry
            // 
            this.btnFarmaceuciKasujFiltry.Enabled = false;
            this.btnFarmaceuciKasujFiltry.Location = new System.Drawing.Point(6, 75);
            this.btnFarmaceuciKasujFiltry.Name = "btnFarmaceuciKasujFiltry";
            this.btnFarmaceuciKasujFiltry.Size = new System.Drawing.Size(75, 23);
            this.btnFarmaceuciKasujFiltry.TabIndex = 3;
            this.btnFarmaceuciKasujFiltry.Text = "Kasuj filtry";
            this.btnFarmaceuciKasujFiltry.UseVisualStyleBackColor = true;
            this.btnFarmaceuciKasujFiltry.Click += new System.EventHandler(this.btnFarmaceuciKasujFiltry_Click);
            // 
            // btnFarmaceuciFiltruj
            // 
            this.btnFarmaceuciFiltruj.Location = new System.Drawing.Point(6, 19);
            this.btnFarmaceuciFiltruj.Name = "btnFarmaceuciFiltruj";
            this.btnFarmaceuciFiltruj.Size = new System.Drawing.Size(74, 50);
            this.btnFarmaceuciFiltruj.TabIndex = 0;
            this.btnFarmaceuciFiltruj.Text = "Filtruj";
            this.btnFarmaceuciFiltruj.UseVisualStyleBackColor = true;
            this.btnFarmaceuciFiltruj.Click += new System.EventHandler(this.btnFarmaceuciFiltruj_Click);
            // 
            // btnSaveFarmaceuci
            // 
            this.btnSaveFarmaceuci.Location = new System.Drawing.Point(8, 254);
            this.btnSaveFarmaceuci.Name = "btnSaveFarmaceuci";
            this.btnSaveFarmaceuci.Size = new System.Drawing.Size(83, 69);
            this.btnSaveFarmaceuci.TabIndex = 3;
            this.btnSaveFarmaceuci.Text = "Zapisz";
            this.btnSaveFarmaceuci.UseVisualStyleBackColor = true;
            this.btnSaveFarmaceuci.Click += new System.EventHandler(this.btnSaveFarmaceuci_Click);
            // 
            // farmaceuciBindingNavigator
            // 
            this.farmaceuciBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem1;
            this.farmaceuciBindingNavigator.BindingSource = this.farmaceuciBindingSource;
            this.farmaceuciBindingNavigator.CountItem = this.bindingNavigatorCountItem1;
            this.farmaceuciBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem1;
            this.farmaceuciBindingNavigator.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.farmaceuciBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.bindingNavigatorAddNewItem1,
            this.bindingNavigatorDeleteItem1});
            this.farmaceuciBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.farmaceuciBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.farmaceuciBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.farmaceuciBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.farmaceuciBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.farmaceuciBindingNavigator.Name = "farmaceuciBindingNavigator";
            this.farmaceuciBindingNavigator.PositionItem = this.bindingNavigatorPositionItem1;
            this.farmaceuciBindingNavigator.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.farmaceuciBindingNavigator.Size = new System.Drawing.Size(856, 25);
            this.farmaceuciBindingNavigator.TabIndex = 2;
            this.farmaceuciBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem1
            // 
            this.bindingNavigatorAddNewItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem1.Image")));
            this.bindingNavigatorAddNewItem1.Name = "bindingNavigatorAddNewItem1";
            this.bindingNavigatorAddNewItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem1.Text = "Add new";
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem1.Text = "of {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem1
            // 
            this.bindingNavigatorDeleteItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem1.Image")));
            this.bindingNavigatorDeleteItem1.Name = "bindingNavigatorDeleteItem1";
            this.bindingNavigatorDeleteItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem1.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem1.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem1.Text = "Move previous";
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Position";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem1.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem1.Text = "Move last";
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // tabHistory
            // 
            this.tabHistory.AutoScroll = true;
            this.tabHistory.Controls.Add(this.dgvHistory);
            this.tabHistory.Controls.Add(this.grpHistory);
            this.tabHistory.Location = new System.Drawing.Point(4, 39);
            this.tabHistory.Name = "tabHistory";
            this.tabHistory.Size = new System.Drawing.Size(856, 479);
            this.tabHistory.TabIndex = 4;
            this.tabHistory.Text = "HISTORIA";
            this.tabHistory.UseVisualStyleBackColor = true;
            // 
            // dgvHistory
            // 
            this.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistory.Location = new System.Drawing.Point(3, 3);
            this.dgvHistory.Name = "dgvHistory";
            this.dgvHistory.Size = new System.Drawing.Size(548, 323);
            this.dgvHistory.TabIndex = 2;
            // 
            // grpHistory
            // 
            this.grpHistory.Controls.Add(this.btnHistoryShowBestPharmacist);
            this.grpHistory.Controls.Add(this.lblHistoryCompareId);
            this.grpHistory.Controls.Add(this.txtHistoryCompareId);
            this.grpHistory.Controls.Add(this.btnHistoryCompareSell);
            this.grpHistory.Controls.Add(this.btnHistoryMySellProcedure);
            this.grpHistory.Location = new System.Drawing.Point(557, 3);
            this.grpHistory.Name = "grpHistory";
            this.grpHistory.Size = new System.Drawing.Size(296, 473);
            this.grpHistory.TabIndex = 1;
            this.grpHistory.TabStop = false;
            this.grpHistory.Text = "Historia";
            // 
            // btnHistoryShowBestPharmacist
            // 
            this.btnHistoryShowBestPharmacist.Location = new System.Drawing.Point(6, 48);
            this.btnHistoryShowBestPharmacist.Name = "btnHistoryShowBestPharmacist";
            this.btnHistoryShowBestPharmacist.Size = new System.Drawing.Size(284, 23);
            this.btnHistoryShowBestPharmacist.TabIndex = 5;
            this.btnHistoryShowBestPharmacist.Text = "Pokaż najlepszego faramaceutę";
            this.btnHistoryShowBestPharmacist.UseVisualStyleBackColor = true;
            this.btnHistoryShowBestPharmacist.Click += new System.EventHandler(this.btnHistoryShowBestPharmacist_Click);
            // 
            // lblHistoryCompareId
            // 
            this.lblHistoryCompareId.AutoSize = true;
            this.lblHistoryCompareId.Location = new System.Drawing.Point(7, 99);
            this.lblHistoryCompareId.Name = "lblHistoryCompareId";
            this.lblHistoryCompareId.Size = new System.Drawing.Size(133, 13);
            this.lblHistoryCompareId.TabIndex = 4;
            this.lblHistoryCompareId.Text = "Porównaj farmaceutę o ID:";
            // 
            // txtHistoryCompareId
            // 
            this.txtHistoryCompareId.Location = new System.Drawing.Point(154, 96);
            this.txtHistoryCompareId.Name = "txtHistoryCompareId";
            this.txtHistoryCompareId.Size = new System.Drawing.Size(136, 20);
            this.txtHistoryCompareId.TabIndex = 3;
            // 
            // btnHistoryCompareSell
            // 
            this.btnHistoryCompareSell.Location = new System.Drawing.Point(6, 118);
            this.btnHistoryCompareSell.Name = "btnHistoryCompareSell";
            this.btnHistoryCompareSell.Size = new System.Drawing.Size(284, 23);
            this.btnHistoryCompareSell.TabIndex = 2;
            this.btnHistoryCompareSell.Text = "Porównaj sprzedaż";
            this.btnHistoryCompareSell.UseVisualStyleBackColor = true;
            this.btnHistoryCompareSell.Click += new System.EventHandler(this.btnHistoryCompareSell_Click);
            // 
            // btnHistoryMySellProcedure
            // 
            this.btnHistoryMySellProcedure.Location = new System.Drawing.Point(6, 19);
            this.btnHistoryMySellProcedure.Name = "btnHistoryMySellProcedure";
            this.btnHistoryMySellProcedure.Size = new System.Drawing.Size(284, 23);
            this.btnHistoryMySellProcedure.TabIndex = 1;
            this.btnHistoryMySellProcedure.Text = "Moja sprzedaż ";
            this.btnHistoryMySellProcedure.UseVisualStyleBackColor = true;
            this.btnHistoryMySellProcedure.Click += new System.EventHandler(this.btnHistoryMySellProcedure_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox_OMNIE_value);
            this.tabPage1.Controls.Add(this.textBox_OMNIE_key);
            this.tabPage1.Location = new System.Drawing.Point(4, 39);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(856, 479);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "O MNIE";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox_OMNIE_value
            // 
            this.textBox_OMNIE_value.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_OMNIE_value.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_OMNIE_value.Location = new System.Drawing.Point(228, 12);
            this.textBox_OMNIE_value.Multiline = true;
            this.textBox_OMNIE_value.Name = "textBox_OMNIE_value";
            this.textBox_OMNIE_value.Size = new System.Drawing.Size(620, 459);
            this.textBox_OMNIE_value.TabIndex = 1;
            // 
            // textBox_OMNIE_key
            // 
            this.textBox_OMNIE_key.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_OMNIE_key.Font = new System.Drawing.Font("Consolas", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_OMNIE_key.Location = new System.Drawing.Point(8, 12);
            this.textBox_OMNIE_key.Multiline = true;
            this.textBox_OMNIE_key.Name = "textBox_OMNIE_key";
            this.textBox_OMNIE_key.Size = new System.Drawing.Size(214, 459);
            this.textBox_OMNIE_key.TabIndex = 0;
            this.textBox_OMNIE_key.Text = "ID:\r\n\r\nNazwisko:\r\n\r\nImię:\r\n\r\nData urodzenia:\r\n\r\nAdres:\r\n\r\nKod pocztowy:\r\n\r\nMiejsc" +
    "owość:\r\n\r\nNr telefonu:";
            this.textBox_OMNIE_key.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panelLogin
            // 
            this.panelLogin.Controls.Add(this.label_text_pass);
            this.panelLogin.Controls.Add(this.label_text_login);
            this.panelLogin.Controls.Add(this.button_LogIn);
            this.panelLogin.Controls.Add(this.textBox_Password);
            this.panelLogin.Controls.Add(this.textBox_Login);
            this.panelLogin.Controls.Add(this.pictureBox_LoginBG);
            this.panelLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLogin.Location = new System.Drawing.Point(0, 0);
            this.panelLogin.Name = "panelLogin";
            this.panelLogin.Size = new System.Drawing.Size(864, 522);
            this.panelLogin.TabIndex = 1;
            // 
            // label_text_pass
            // 
            this.label_text_pass.BackColor = System.Drawing.Color.White;
            this.label_text_pass.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_text_pass.ForeColor = System.Drawing.Color.Green;
            this.label_text_pass.Location = new System.Drawing.Point(445, 246);
            this.label_text_pass.Name = "label_text_pass";
            this.label_text_pass.Size = new System.Drawing.Size(160, 26);
            this.label_text_pass.TabIndex = 5;
            this.label_text_pass.Text = "HASŁO:";
            this.label_text_pass.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_text_login
            // 
            this.label_text_login.BackColor = System.Drawing.Color.White;
            this.label_text_login.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_text_login.ForeColor = System.Drawing.Color.Green;
            this.label_text_login.Location = new System.Drawing.Point(445, 186);
            this.label_text_login.Name = "label_text_login";
            this.label_text_login.Size = new System.Drawing.Size(160, 26);
            this.label_text_login.TabIndex = 4;
            this.label_text_login.Text = "LOGIN:";
            this.label_text_login.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button_LogIn
            // 
            this.button_LogIn.Location = new System.Drawing.Point(611, 306);
            this.button_LogIn.Name = "button_LogIn";
            this.button_LogIn.Size = new System.Drawing.Size(160, 26);
            this.button_LogIn.TabIndex = 3;
            this.button_LogIn.Text = "ZALOGUJ";
            this.button_LogIn.UseVisualStyleBackColor = true;
            this.button_LogIn.Click += new System.EventHandler(this.button_LogIn_Click);
            // 
            // textBox_Password
            // 
            this.textBox_Password.BackColor = System.Drawing.Color.Green;
            this.textBox_Password.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_Password.ForeColor = System.Drawing.Color.White;
            this.textBox_Password.Location = new System.Drawing.Point(611, 246);
            this.textBox_Password.MaxLength = 16;
            this.textBox_Password.Name = "textBox_Password";
            this.textBox_Password.PasswordChar = '*';
            this.textBox_Password.Size = new System.Drawing.Size(160, 26);
            this.textBox_Password.TabIndex = 2;
            this.textBox_Password.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_Login
            // 
            this.textBox_Login.BackColor = System.Drawing.Color.Green;
            this.textBox_Login.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_Login.ForeColor = System.Drawing.Color.White;
            this.textBox_Login.Location = new System.Drawing.Point(611, 186);
            this.textBox_Login.MaxLength = 16;
            this.textBox_Login.Name = "textBox_Login";
            this.textBox_Login.Size = new System.Drawing.Size(160, 26);
            this.textBox_Login.TabIndex = 1;
            this.textBox_Login.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox_LoginBG
            // 
            this.pictureBox_LoginBG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_LoginBG.ErrorImage = null;
            this.pictureBox_LoginBG.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_LoginBG.Image")));
            this.pictureBox_LoginBG.InitialImage = null;
            this.pictureBox_LoginBG.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_LoginBG.Name = "pictureBox_LoginBG";
            this.pictureBox_LoginBG.Size = new System.Drawing.Size(864, 522);
            this.pictureBox_LoginBG.TabIndex = 0;
            this.pictureBox_LoginBG.TabStop = false;
            // 
            // formApteka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 522);
            this.Controls.Add(this.MainMenu);
            this.Controls.Add(this.panelLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "formApteka";
            this.Text = "Apteka - Projekt BD";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MainMenu.ResumeLayout(false);
            this.tabSell.ResumeLayout(false);
            this.tabSell.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipeInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSellMeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSell)).EndInit();
            this.tabMeds.ResumeLayout(false);
            this.tabMeds.PerformLayout();
            this.grpActiveFilters.ResumeLayout(false);
            this.grpFilters.ResumeLayout(false);
            this.grpFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMeds)).EndInit();
            this.contextMenuLeki.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bnMeds)).EndInit();
            this.bnMeds.ResumeLayout(false);
            this.bnMeds.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsMeds)).EndInit();
            this.tabDelivery.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lekiBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dostawcyBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabEmployees.ResumeLayout(false);
            this.tabEmployees.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.farmaceuciDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.farmaceuciBindingSource1)).EndInit();
            this.grpFarmaceuciFiltry.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.farmaceuciBindingNavigator)).EndInit();
            this.farmaceuciBindingNavigator.ResumeLayout(false);
            this.farmaceuciBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.farmaceuciBindingSource)).EndInit();
            this.tabHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).EndInit();
            this.grpHistory.ResumeLayout(false);
            this.grpHistory.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panelLogin.ResumeLayout(false);
            this.panelLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_LoginBG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dostawyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsSellMeds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsRecipeInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsHistory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MainMenu;
        private System.Windows.Forms.TabPage tabSell;
        private System.Windows.Forms.TabPage tabDelivery;
        private System.Windows.Forms.TabPage tabMeds;
        private System.Windows.Forms.TabPage tabEmployees;
        private System.Windows.Forms.TabPage tabHistory;
        private System.Windows.Forms.BindingNavigator bnMeds;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.BindingSource bsMeds;
        private System.Windows.Forms.DataGridView dgvMeds;
        private System.Windows.Forms.Button btnAddFilters;
        private System.Windows.Forms.GroupBox grpFilters;
        private System.Windows.Forms.Button btnRemoveFilters;
        private System.Windows.Forms.Panel panelLogin;
        private System.Windows.Forms.PictureBox pictureBox_LoginBG;
        private System.Windows.Forms.TextBox textBox_Login;
        private System.Windows.Forms.TextBox textBox_Password;
        private System.Windows.Forms.Button button_LogIn;
        private System.Windows.Forms.Label label_text_pass;
        private System.Windows.Forms.Label label_text_login;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBox_OMNIE_key;
        private System.Windows.Forms.TextBox textBox_OMNIE_value;
        private System.Windows.Forms.BindingSource farmaceuciBindingSource;
        private System.Windows.Forms.BindingNavigator farmaceuciBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem1;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator5;
        private System.Windows.Forms.Button btnSaveFarmaceuci;
        private System.Windows.Forms.GroupBox grpFarmaceuciFiltry;
        private System.Windows.Forms.Button btnFarmaceuciKasujFiltry;
        private System.Windows.Forms.Button btnFarmaceuciFiltruj;
        private System.Windows.Forms.ComboBox comboBoxFarmaceuciKolumny;
        private System.Windows.Forms.Button btnFarmaceuciAddSelectedFIlter;
        private System.Windows.Forms.TableLayoutPanel tableFiltry;
        private System.Windows.Forms.ContextMenuStrip contextMenuLeki;
        private System.Windows.Forms.ToolStripMenuItem additionalMedInfo;
        public System.Windows.Forms.GroupBox grpActiveFilters;
        private System.Windows.Forms.Button btnSaveMedsChanges;
        private System.Windows.Forms.ComboBox cb_tabMeds_filter_Id;
        private System.Windows.Forms.ComboBox cb_tabMeds_filter_Name;
        private System.Windows.Forms.ComboBox cb_tabMeds_filter_Nr;
        private System.Windows.Forms.TextBox tbx_tabMeds_filter_Nr;
        private System.Windows.Forms.TextBox tbx_tabMeds_filter_Name;
        private System.Windows.Forms.TextBox tbx_tabMeds_filter_Id;
        private System.Windows.Forms.CheckBox cbx_tabMeds_filter_Nr;
        private System.Windows.Forms.CheckBox cbx_tabMeds_filter_Name;
        private System.Windows.Forms.CheckBox cbx_tabMeds_filter_Id;
        private System.Windows.Forms.ComboBox cb_tabMeds_filter_Recipe;
        private System.Windows.Forms.ComboBox cb_tabMeds_filter_Price;
        private System.Windows.Forms.ComboBox cb_tabMeds_filter_Count;
        private System.Windows.Forms.CheckBox cbx_tabMeds_filter_Recipe;
        private System.Windows.Forms.CheckBox cbx_tabMeds_filter_Price;
        private System.Windows.Forms.CheckBox cbx_tabMeds_filter_Count;
        private System.Windows.Forms.TextBox tbx_tabMeds_filter_Price;
        private System.Windows.Forms.TextBox tbx_tabMeds_filter_Count;
        private System.Windows.Forms.DataGridView farmaceuciDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwiskoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn imieDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataurDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn adresDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kodpocztDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn miejscowoscDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefonDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDFarmaceutyDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource farmaceuciBindingSource1;
        private System.Windows.Forms.ListView listView_ActiveFilters_Meds;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox ndostawcaAdres;
        private System.Windows.Forms.TextBox ndostawcaKod;
        private System.Windows.Forms.TextBox ndostawcaMiejscowosc;
        private System.Windows.Forms.TextBox ndostawcaTelefon;
        private System.Windows.Forms.TextBox ndostawcaNazwa;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.BindingSource dostawcyBindingSource;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_zapisz_dostawe;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.BindingSource dostawyBindingSource;
        private System.Windows.Forms.ComboBox CBox_dostawca;
        private System.Windows.Forms.DataGridView dgvSell;
        private System.Windows.Forms.BindingSource bsSell;
        private System.Windows.Forms.DataGridView dgvSellMeds;
        private System.Windows.Forms.BindingSource bsSellMeds;
        private System.Windows.Forms.Label lbl_Sell_Sell;
        private System.Windows.Forms.Label lbl_Sell_SellMeds;
        private System.Windows.Forms.DataGridView dgvRecipeInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource bsRecipeInfo;
        private System.Windows.Forms.DataGridView dgvClient;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource bsClient;
        private System.Windows.Forms.ComboBox cb_tabSell_filter_Value;
        private System.Windows.Forms.CheckBox cbx_tabSell_filter_Value;
        private System.Windows.Forms.TextBox tbx_tabSell_filter_Value;
        private System.Windows.Forms.ComboBox cb_tabSell_filter_Date;
        private System.Windows.Forms.CheckBox cbx_tabSell_filter_Date;
        private System.Windows.Forms.Button btnSellAddFil;
        private System.Windows.Forms.Button btnSellDelFil;
        private System.Windows.Forms.Button btnSellNewSell;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.DateTimePicker dtp_tabSell_filter_Date;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazwaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iloscDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nrpartiiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cenaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn narecepteDataGridViewCheckBoxColumn;
        private System.Windows.Forms.BindingSource lekiBindingSource;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TBox_ilosc;
        private System.Windows.Forms.MaskedTextBox MTBox_data_waznosci;
        private System.Windows.Forms.Button btn_dodaj_lek;
        private System.Windows.Forms.Button btn_anuluj_dostawe;
        private System.Windows.Forms.Button btn_odswiez;
        private System.Windows.Forms.Button btn_wybierz_dostawce;
        private System.Windows.Forms.GroupBox grpHistory;
        private System.Windows.Forms.Button btnHistoryMySellProcedure;
        private System.Windows.Forms.DataGridView dgvHistory;
        private System.Windows.Forms.BindingSource bsHistory;
        private System.Windows.Forms.Button btnHistoryCompareSell;
        private System.Windows.Forms.Label lblHistoryCompareId;
        private System.Windows.Forms.TextBox txtHistoryCompareId;
        private System.Windows.Forms.Button btn_zmien_cene;
        private System.Windows.Forms.TextBox TBox_nowa_cena;
        private System.Windows.Forms.Button btnHistoryShowBestPharmacist;
    }
}
