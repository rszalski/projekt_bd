﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekt_bd {
    public class Database {
        private string connectionString;
        private string databaseName;
        private SqlDataAdapter dataGridAdapter;
        private SqlConnection connection;

        public Database() {
            this.databaseName = "apteka";
        }

        public void connect(String login, String pass) {
            if (!this.isConnected()) {
                this.connectionString = string.Format("Server=localhost; Initial Catalog={0}; User ID={1}; Password={2}",
                    this.databaseName, login, pass);

                this.connection = new SqlConnection(this.connectionString);
                this.connection.Open();
            }
        }

        public void disconnect() {
            if (this.connection.State == ConnectionState.Open) {
                this.connection.Close();
            }
        }

        public bool isConnected() {
            return (this.connection != null && this.connection.State == ConnectionState.Open);
        }

        public void update(DataTable table) {
            dataGridAdapter.Update(table);
        }

        public DataTable getDataSet(string selectQuery, Dictionary<string, string> parameters = null) {
            DataTable table = new DataTable();
            this.dataGridAdapter = new SqlDataAdapter(selectQuery, this.connection);

            if (parameters != null) {
                foreach (String key in parameters.Keys) {
                    this.dataGridAdapter.SelectCommand.Parameters.Add(new SqlParameter(key, parameters[key]));
                }
            }
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataGridAdapter);
            this.dataGridAdapter.Fill(table);

            return table;
        }

        public SqlTransaction getTransacion()
        {
            return this.connection.BeginTransaction();
        }

        public SqlTransaction getTransacion(IsolationLevel lvl)
        {
            return this.connection.BeginTransaction(lvl);
        }

        public SqlCommand getProcedure(string name)
        {
            SqlCommand procedure = new SqlCommand(name, this.connection);
            procedure.CommandType = CommandType.StoredProcedure;
            return procedure;
        }

        public SqlCommand getCommand(string cmd, SqlTransaction trans)
        {
            return new SqlCommand(cmd, this.connection, trans);
        }
    }
}
