﻿namespace projekt_bd
{
    partial class formNewSell
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formNewSell));
            this.dgvNewSell = new System.Windows.Forms.DataGridView();
            this.Nazwa = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Nr_partii = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Ilosc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_Recepty = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bnNewSell = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bsNewSell = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.dgvNewSellClientInfo = new System.Windows.Forms.DataGridView();
            this.lblNewSellClient = new System.Windows.Forms.Label();
            this.dgvNewSellRecipeInfo = new System.Windows.Forms.DataGridView();
            this.lblNewSellRecipe = new System.Windows.Forms.Label();
            this.gbxNewSellNewRecipe = new System.Windows.Forms.GroupBox();
            this.cbxNewSellRecipePESEL = new System.Windows.Forms.ComboBox();
            this.dtpNewSellRecipeDate = new System.Windows.Forms.DateTimePicker();
            this.tbxNewSellRecipeDoc = new System.Windows.Forms.TextBox();
            this.tbxNewSellRecipeId = new System.Windows.Forms.TextBox();
            this.lblNewSellRecipeDate = new System.Windows.Forms.Label();
            this.lblNewSellRecipeDoc = new System.Windows.Forms.Label();
            this.lblNewSellRecipePESEL = new System.Windows.Forms.Label();
            this.lblNewSellRecipeId = new System.Windows.Forms.Label();
            this.gbxNewSellNewClient = new System.Windows.Forms.GroupBox();
            this.lblSexM = new System.Windows.Forms.Label();
            this.lblSexK = new System.Windows.Forms.Label();
            this.tbxNewSellClientPESEL = new System.Windows.Forms.TextBox();
            this.tbxNewSellClientCity = new System.Windows.Forms.TextBox();
            this.tbxNewSellClientPostal = new System.Windows.Forms.TextBox();
            this.tbxNewSellClientAddress = new System.Windows.Forms.TextBox();
            this.dtpNewSellClientBornOn = new System.Windows.Forms.DateTimePicker();
            this.tbNewSellClientSex = new System.Windows.Forms.TrackBar();
            this.tbxNewSellClientFirstname = new System.Windows.Forms.TextBox();
            this.tbxNewSellClientSurname = new System.Windows.Forms.TextBox();
            this.lblNewSellClientPESEL = new System.Windows.Forms.Label();
            this.lblNewSellClientCity = new System.Windows.Forms.Label();
            this.lblNewSellClientPostal = new System.Windows.Forms.Label();
            this.lblNewSellClientAddress = new System.Windows.Forms.Label();
            this.lblNewSellClientBornOn = new System.Windows.Forms.Label();
            this.lblNewSellClientSex = new System.Windows.Forms.Label();
            this.lblNewSellClientFirstname = new System.Windows.Forms.Label();
            this.lblNewSellClientSurname = new System.Windows.Forms.Label();
            this.bsNewSellRecipeInfo = new System.Windows.Forms.BindingSource(this.components);
            this.bsNewSellClientInfo = new System.Windows.Forms.BindingSource(this.components);
            this.btnNewSellSaveRecipe = new System.Windows.Forms.Button();
            this.btnNewSellSaveClient = new System.Windows.Forms.Button();
            this.btnNewSellSaveSell = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnNewSell)).BeginInit();
            this.bnNewSell.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsNewSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewSellClientInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewSellRecipeInfo)).BeginInit();
            this.gbxNewSellNewRecipe.SuspendLayout();
            this.gbxNewSellNewClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNewSellClientSex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsNewSellRecipeInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsNewSellClientInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvNewSell
            // 
            this.dgvNewSell.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvNewSell.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvNewSell.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNewSell.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nazwa,
            this.Nr_partii,
            this.Ilosc,
            this.ID_Recepty});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvNewSell.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvNewSell.Location = new System.Drawing.Point(0, 28);
            this.dgvNewSell.Name = "dgvNewSell";
            this.dgvNewSell.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvNewSell.Size = new System.Drawing.Size(613, 133);
            this.dgvNewSell.TabIndex = 0;
            this.dgvNewSell.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNewSell_CellValueChanged);
            this.dgvNewSell.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvNewSell_RowsAdded);
            this.dgvNewSell.SelectionChanged += new System.EventHandler(this.dgvNewSell_SelectionChanged);
            // 
            // Nazwa
            // 
            this.Nazwa.HeaderText = "Nazwa";
            this.Nazwa.Name = "Nazwa";
            this.Nazwa.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Nazwa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Nazwa.Width = 195;
            // 
            // Nr_partii
            // 
            this.Nr_partii.HeaderText = "Nr_partii";
            this.Nr_partii.Name = "Nr_partii";
            this.Nr_partii.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Nr_partii.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Ilosc
            // 
            this.Ilosc.HeaderText = "Ilosc";
            this.Ilosc.MaxInputLength = 8;
            this.Ilosc.Name = "Ilosc";
            // 
            // ID_Recepty
            // 
            this.ID_Recepty.HeaderText = "ID_Recepty";
            this.ID_Recepty.Name = "ID_Recepty";
            this.ID_Recepty.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ID_Recepty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ID_Recepty.Width = 175;
            // 
            // bnNewSell
            // 
            this.bnNewSell.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bnNewSell.BindingSource = this.bsNewSell;
            this.bnNewSell.CountItem = this.bindingNavigatorCountItem;
            this.bnNewSell.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bnNewSell.Dock = System.Windows.Forms.DockStyle.None;
            this.bnNewSell.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.bnNewSell.Location = new System.Drawing.Point(0, 0);
            this.bnNewSell.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bnNewSell.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bnNewSell.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bnNewSell.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bnNewSell.Name = "bnNewSell";
            this.bnNewSell.PositionItem = this.bindingNavigatorPositionItem;
            this.bnNewSell.Size = new System.Drawing.Size(255, 25);
            this.bnNewSell.TabIndex = 1;
            this.bnNewSell.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // dgvNewSellClientInfo
            // 
            this.dgvNewSellClientInfo.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvNewSellClientInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvNewSellClientInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNewSellClientInfo.Location = new System.Drawing.Point(-3, 265);
            this.dgvNewSellClientInfo.MultiSelect = false;
            this.dgvNewSellClientInfo.Name = "dgvNewSellClientInfo";
            this.dgvNewSellClientInfo.ReadOnly = true;
            this.dgvNewSellClientInfo.RowHeadersVisible = false;
            this.dgvNewSellClientInfo.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgvNewSellClientInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNewSellClientInfo.Size = new System.Drawing.Size(614, 60);
            this.dgvNewSellClientInfo.TabIndex = 11;
            this.dgvNewSellClientInfo.SelectionChanged += new System.EventHandler(this.dgv_SelectionChanged);
            // 
            // lblNewSellClient
            // 
            this.lblNewSellClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellClient.Location = new System.Drawing.Point(-4, 240);
            this.lblNewSellClient.Name = "lblNewSellClient";
            this.lblNewSellClient.Size = new System.Drawing.Size(617, 22);
            this.lblNewSellClient.TabIndex = 10;
            this.lblNewSellClient.Text = "Klient";
            this.lblNewSellClient.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvNewSellRecipeInfo
            // 
            this.dgvNewSellRecipeInfo.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvNewSellRecipeInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvNewSellRecipeInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNewSellRecipeInfo.Location = new System.Drawing.Point(-1, 189);
            this.dgvNewSellRecipeInfo.MultiSelect = false;
            this.dgvNewSellRecipeInfo.Name = "dgvNewSellRecipeInfo";
            this.dgvNewSellRecipeInfo.ReadOnly = true;
            this.dgvNewSellRecipeInfo.RowHeadersVisible = false;
            this.dgvNewSellRecipeInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNewSellRecipeInfo.Size = new System.Drawing.Size(614, 43);
            this.dgvNewSellRecipeInfo.TabIndex = 9;
            this.dgvNewSellRecipeInfo.SelectionChanged += new System.EventHandler(this.dgv_SelectionChanged);
            // 
            // lblNewSellRecipe
            // 
            this.lblNewSellRecipe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellRecipe.Location = new System.Drawing.Point(-4, 164);
            this.lblNewSellRecipe.Name = "lblNewSellRecipe";
            this.lblNewSellRecipe.Size = new System.Drawing.Size(617, 22);
            this.lblNewSellRecipe.TabIndex = 8;
            this.lblNewSellRecipe.Text = "Recepta";
            this.lblNewSellRecipe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxNewSellNewRecipe
            // 
            this.gbxNewSellNewRecipe.Controls.Add(this.cbxNewSellRecipePESEL);
            this.gbxNewSellNewRecipe.Controls.Add(this.dtpNewSellRecipeDate);
            this.gbxNewSellNewRecipe.Controls.Add(this.tbxNewSellRecipeDoc);
            this.gbxNewSellNewRecipe.Controls.Add(this.tbxNewSellRecipeId);
            this.gbxNewSellNewRecipe.Controls.Add(this.lblNewSellRecipeDate);
            this.gbxNewSellNewRecipe.Controls.Add(this.lblNewSellRecipeDoc);
            this.gbxNewSellNewRecipe.Controls.Add(this.lblNewSellRecipePESEL);
            this.gbxNewSellNewRecipe.Controls.Add(this.lblNewSellRecipeId);
            this.gbxNewSellNewRecipe.Location = new System.Drawing.Point(0, 331);
            this.gbxNewSellNewRecipe.Name = "gbxNewSellNewRecipe";
            this.gbxNewSellNewRecipe.Size = new System.Drawing.Size(304, 161);
            this.gbxNewSellNewRecipe.TabIndex = 12;
            this.gbxNewSellNewRecipe.TabStop = false;
            this.gbxNewSellNewRecipe.Text = "Nowa recepta";
            // 
            // cbxNewSellRecipePESEL
            // 
            this.cbxNewSellRecipePESEL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxNewSellRecipePESEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbxNewSellRecipePESEL.FormattingEnabled = true;
            this.cbxNewSellRecipePESEL.Location = new System.Drawing.Point(136, 59);
            this.cbxNewSellRecipePESEL.Name = "cbxNewSellRecipePESEL";
            this.cbxNewSellRecipePESEL.Size = new System.Drawing.Size(150, 28);
            this.cbxNewSellRecipePESEL.TabIndex = 23;
            // 
            // dtpNewSellRecipeDate
            // 
            this.dtpNewSellRecipeDate.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpNewSellRecipeDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtpNewSellRecipeDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNewSellRecipeDate.Location = new System.Drawing.Point(136, 120);
            this.dtpNewSellRecipeDate.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dtpNewSellRecipeDate.MinDate = new System.DateTime(1950, 1, 1, 0, 0, 0, 0);
            this.dtpNewSellRecipeDate.Name = "dtpNewSellRecipeDate";
            this.dtpNewSellRecipeDate.Size = new System.Drawing.Size(150, 29);
            this.dtpNewSellRecipeDate.TabIndex = 16;
            this.dtpNewSellRecipeDate.Value = new System.DateTime(2013, 1, 1, 0, 0, 0, 0);
            // 
            // tbxNewSellRecipeDoc
            // 
            this.tbxNewSellRecipeDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbxNewSellRecipeDoc.Location = new System.Drawing.Point(136, 88);
            this.tbxNewSellRecipeDoc.MaxLength = 7;
            this.tbxNewSellRecipeDoc.Multiline = true;
            this.tbxNewSellRecipeDoc.Name = "tbxNewSellRecipeDoc";
            this.tbxNewSellRecipeDoc.Size = new System.Drawing.Size(150, 30);
            this.tbxNewSellRecipeDoc.TabIndex = 22;
            this.tbxNewSellRecipeDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxNewSellRecipeId
            // 
            this.tbxNewSellRecipeId.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbxNewSellRecipeId.Location = new System.Drawing.Point(136, 28);
            this.tbxNewSellRecipeId.MaxLength = 10;
            this.tbxNewSellRecipeId.Multiline = true;
            this.tbxNewSellRecipeId.Name = "tbxNewSellRecipeId";
            this.tbxNewSellRecipeId.Size = new System.Drawing.Size(150, 30);
            this.tbxNewSellRecipeId.TabIndex = 20;
            this.tbxNewSellRecipeId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblNewSellRecipeDate
            // 
            this.lblNewSellRecipeDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellRecipeDate.Location = new System.Drawing.Point(12, 118);
            this.lblNewSellRecipeDate.Name = "lblNewSellRecipeDate";
            this.lblNewSellRecipeDate.Size = new System.Drawing.Size(105, 30);
            this.lblNewSellRecipeDate.TabIndex = 19;
            this.lblNewSellRecipeDate.Text = "Data wyst.";
            this.lblNewSellRecipeDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellRecipeDoc
            // 
            this.lblNewSellRecipeDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellRecipeDoc.Location = new System.Drawing.Point(12, 88);
            this.lblNewSellRecipeDoc.Name = "lblNewSellRecipeDoc";
            this.lblNewSellRecipeDoc.Size = new System.Drawing.Size(105, 30);
            this.lblNewSellRecipeDoc.TabIndex = 18;
            this.lblNewSellRecipeDoc.Text = "Lekarz";
            this.lblNewSellRecipeDoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellRecipePESEL
            // 
            this.lblNewSellRecipePESEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellRecipePESEL.Location = new System.Drawing.Point(12, 58);
            this.lblNewSellRecipePESEL.Name = "lblNewSellRecipePESEL";
            this.lblNewSellRecipePESEL.Size = new System.Drawing.Size(105, 30);
            this.lblNewSellRecipePESEL.TabIndex = 17;
            this.lblNewSellRecipePESEL.Text = "PESEL";
            this.lblNewSellRecipePESEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellRecipeId
            // 
            this.lblNewSellRecipeId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellRecipeId.Location = new System.Drawing.Point(12, 28);
            this.lblNewSellRecipeId.Name = "lblNewSellRecipeId";
            this.lblNewSellRecipeId.Size = new System.Drawing.Size(105, 30);
            this.lblNewSellRecipeId.TabIndex = 16;
            this.lblNewSellRecipeId.Text = "ID recepty";
            this.lblNewSellRecipeId.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbxNewSellNewClient
            // 
            this.gbxNewSellNewClient.Controls.Add(this.lblSexM);
            this.gbxNewSellNewClient.Controls.Add(this.lblSexK);
            this.gbxNewSellNewClient.Controls.Add(this.tbxNewSellClientPESEL);
            this.gbxNewSellNewClient.Controls.Add(this.tbxNewSellClientCity);
            this.gbxNewSellNewClient.Controls.Add(this.tbxNewSellClientPostal);
            this.gbxNewSellNewClient.Controls.Add(this.tbxNewSellClientAddress);
            this.gbxNewSellNewClient.Controls.Add(this.dtpNewSellClientBornOn);
            this.gbxNewSellNewClient.Controls.Add(this.tbNewSellClientSex);
            this.gbxNewSellNewClient.Controls.Add(this.tbxNewSellClientFirstname);
            this.gbxNewSellNewClient.Controls.Add(this.tbxNewSellClientSurname);
            this.gbxNewSellNewClient.Controls.Add(this.lblNewSellClientPESEL);
            this.gbxNewSellNewClient.Controls.Add(this.lblNewSellClientCity);
            this.gbxNewSellNewClient.Controls.Add(this.lblNewSellClientPostal);
            this.gbxNewSellNewClient.Controls.Add(this.lblNewSellClientAddress);
            this.gbxNewSellNewClient.Controls.Add(this.lblNewSellClientBornOn);
            this.gbxNewSellNewClient.Controls.Add(this.lblNewSellClientSex);
            this.gbxNewSellNewClient.Controls.Add(this.lblNewSellClientFirstname);
            this.gbxNewSellNewClient.Controls.Add(this.lblNewSellClientSurname);
            this.gbxNewSellNewClient.Location = new System.Drawing.Point(308, 331);
            this.gbxNewSellNewClient.Name = "gbxNewSellNewClient";
            this.gbxNewSellNewClient.Size = new System.Drawing.Size(304, 190);
            this.gbxNewSellNewClient.TabIndex = 13;
            this.gbxNewSellNewClient.TabStop = false;
            this.gbxNewSellNewClient.Text = "Nowy klient";
            // 
            // lblSexM
            // 
            this.lblSexM.Enabled = false;
            this.lblSexM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSexM.Location = new System.Drawing.Point(265, 61);
            this.lblSexM.Name = "lblSexM";
            this.lblSexM.Size = new System.Drawing.Size(19, 20);
            this.lblSexM.TabIndex = 17;
            this.lblSexM.Text = "M";
            this.lblSexM.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSexK
            // 
            this.lblSexK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblSexK.Location = new System.Drawing.Point(134, 61);
            this.lblSexK.Name = "lblSexK";
            this.lblSexK.Size = new System.Drawing.Size(19, 20);
            this.lblSexK.TabIndex = 16;
            this.lblSexK.Text = "K";
            this.lblSexK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbxNewSellClientPESEL
            // 
            this.tbxNewSellClientPESEL.Location = new System.Drawing.Point(120, 161);
            this.tbxNewSellClientPESEL.MaxLength = 11;
            this.tbxNewSellClientPESEL.Multiline = true;
            this.tbxNewSellClientPESEL.Name = "tbxNewSellClientPESEL";
            this.tbxNewSellClientPESEL.Size = new System.Drawing.Size(173, 20);
            this.tbxNewSellClientPESEL.TabIndex = 15;
            this.tbxNewSellClientPESEL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxNewSellClientCity
            // 
            this.tbxNewSellClientCity.Location = new System.Drawing.Point(120, 141);
            this.tbxNewSellClientCity.MaxLength = 30;
            this.tbxNewSellClientCity.Multiline = true;
            this.tbxNewSellClientCity.Name = "tbxNewSellClientCity";
            this.tbxNewSellClientCity.Size = new System.Drawing.Size(173, 20);
            this.tbxNewSellClientCity.TabIndex = 14;
            this.tbxNewSellClientCity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxNewSellClientPostal
            // 
            this.tbxNewSellClientPostal.Location = new System.Drawing.Point(120, 121);
            this.tbxNewSellClientPostal.MaxLength = 6;
            this.tbxNewSellClientPostal.Multiline = true;
            this.tbxNewSellClientPostal.Name = "tbxNewSellClientPostal";
            this.tbxNewSellClientPostal.Size = new System.Drawing.Size(173, 20);
            this.tbxNewSellClientPostal.TabIndex = 13;
            this.tbxNewSellClientPostal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxNewSellClientAddress
            // 
            this.tbxNewSellClientAddress.Location = new System.Drawing.Point(120, 101);
            this.tbxNewSellClientAddress.MaxLength = 50;
            this.tbxNewSellClientAddress.Multiline = true;
            this.tbxNewSellClientAddress.Name = "tbxNewSellClientAddress";
            this.tbxNewSellClientAddress.Size = new System.Drawing.Size(173, 20);
            this.tbxNewSellClientAddress.TabIndex = 12;
            this.tbxNewSellClientAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dtpNewSellClientBornOn
            // 
            this.dtpNewSellClientBornOn.Location = new System.Drawing.Point(120, 81);
            this.dtpNewSellClientBornOn.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.dtpNewSellClientBornOn.MinDate = new System.DateTime(1850, 1, 1, 0, 0, 0, 0);
            this.dtpNewSellClientBornOn.Name = "dtpNewSellClientBornOn";
            this.dtpNewSellClientBornOn.Size = new System.Drawing.Size(173, 20);
            this.dtpNewSellClientBornOn.TabIndex = 11;
            this.dtpNewSellClientBornOn.Value = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            // 
            // tbNewSellClientSex
            // 
            this.tbNewSellClientSex.AutoSize = false;
            this.tbNewSellClientSex.Location = new System.Drawing.Point(159, 61);
            this.tbNewSellClientSex.Maximum = 1;
            this.tbNewSellClientSex.Name = "tbNewSellClientSex";
            this.tbNewSellClientSex.Size = new System.Drawing.Size(100, 18);
            this.tbNewSellClientSex.TabIndex = 10;
            this.tbNewSellClientSex.ValueChanged += new System.EventHandler(this.tbNewSellClientSex_ValueChanged);
            // 
            // tbxNewSellClientFirstname
            // 
            this.tbxNewSellClientFirstname.Location = new System.Drawing.Point(120, 41);
            this.tbxNewSellClientFirstname.MaxLength = 25;
            this.tbxNewSellClientFirstname.Multiline = true;
            this.tbxNewSellClientFirstname.Name = "tbxNewSellClientFirstname";
            this.tbxNewSellClientFirstname.Size = new System.Drawing.Size(173, 20);
            this.tbxNewSellClientFirstname.TabIndex = 9;
            this.tbxNewSellClientFirstname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxNewSellClientSurname
            // 
            this.tbxNewSellClientSurname.Location = new System.Drawing.Point(120, 21);
            this.tbxNewSellClientSurname.MaxLength = 30;
            this.tbxNewSellClientSurname.Multiline = true;
            this.tbxNewSellClientSurname.Name = "tbxNewSellClientSurname";
            this.tbxNewSellClientSurname.Size = new System.Drawing.Size(173, 20);
            this.tbxNewSellClientSurname.TabIndex = 8;
            this.tbxNewSellClientSurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblNewSellClientPESEL
            // 
            this.lblNewSellClientPESEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellClientPESEL.Location = new System.Drawing.Point(30, 161);
            this.lblNewSellClientPESEL.Name = "lblNewSellClientPESEL";
            this.lblNewSellClientPESEL.Size = new System.Drawing.Size(65, 20);
            this.lblNewSellClientPESEL.TabIndex = 7;
            this.lblNewSellClientPESEL.Text = "PESEL";
            this.lblNewSellClientPESEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellClientCity
            // 
            this.lblNewSellClientCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellClientCity.Location = new System.Drawing.Point(30, 141);
            this.lblNewSellClientCity.Name = "lblNewSellClientCity";
            this.lblNewSellClientCity.Size = new System.Drawing.Size(65, 20);
            this.lblNewSellClientCity.TabIndex = 6;
            this.lblNewSellClientCity.Text = "Miasto";
            this.lblNewSellClientCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellClientPostal
            // 
            this.lblNewSellClientPostal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellClientPostal.Location = new System.Drawing.Point(30, 121);
            this.lblNewSellClientPostal.Name = "lblNewSellClientPostal";
            this.lblNewSellClientPostal.Size = new System.Drawing.Size(65, 20);
            this.lblNewSellClientPostal.TabIndex = 5;
            this.lblNewSellClientPostal.Text = "Kod poczt.";
            this.lblNewSellClientPostal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellClientAddress
            // 
            this.lblNewSellClientAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellClientAddress.Location = new System.Drawing.Point(30, 101);
            this.lblNewSellClientAddress.Name = "lblNewSellClientAddress";
            this.lblNewSellClientAddress.Size = new System.Drawing.Size(65, 20);
            this.lblNewSellClientAddress.TabIndex = 4;
            this.lblNewSellClientAddress.Text = "Adres";
            this.lblNewSellClientAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellClientBornOn
            // 
            this.lblNewSellClientBornOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellClientBornOn.Location = new System.Drawing.Point(30, 81);
            this.lblNewSellClientBornOn.Name = "lblNewSellClientBornOn";
            this.lblNewSellClientBornOn.Size = new System.Drawing.Size(65, 20);
            this.lblNewSellClientBornOn.TabIndex = 3;
            this.lblNewSellClientBornOn.Text = "Data ur.";
            this.lblNewSellClientBornOn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellClientSex
            // 
            this.lblNewSellClientSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellClientSex.Location = new System.Drawing.Point(30, 61);
            this.lblNewSellClientSex.Name = "lblNewSellClientSex";
            this.lblNewSellClientSex.Size = new System.Drawing.Size(65, 20);
            this.lblNewSellClientSex.TabIndex = 2;
            this.lblNewSellClientSex.Text = "Płeć";
            this.lblNewSellClientSex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellClientFirstname
            // 
            this.lblNewSellClientFirstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellClientFirstname.Location = new System.Drawing.Point(30, 41);
            this.lblNewSellClientFirstname.Name = "lblNewSellClientFirstname";
            this.lblNewSellClientFirstname.Size = new System.Drawing.Size(65, 20);
            this.lblNewSellClientFirstname.TabIndex = 1;
            this.lblNewSellClientFirstname.Text = "Imię";
            this.lblNewSellClientFirstname.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNewSellClientSurname
            // 
            this.lblNewSellClientSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNewSellClientSurname.Location = new System.Drawing.Point(30, 21);
            this.lblNewSellClientSurname.Name = "lblNewSellClientSurname";
            this.lblNewSellClientSurname.Size = new System.Drawing.Size(65, 20);
            this.lblNewSellClientSurname.TabIndex = 0;
            this.lblNewSellClientSurname.Text = "Nazwisko";
            this.lblNewSellClientSurname.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnNewSellSaveRecipe
            // 
            this.btnNewSellSaveRecipe.Location = new System.Drawing.Point(3, 495);
            this.btnNewSellSaveRecipe.Name = "btnNewSellSaveRecipe";
            this.btnNewSellSaveRecipe.Size = new System.Drawing.Size(149, 23);
            this.btnNewSellSaveRecipe.TabIndex = 14;
            this.btnNewSellSaveRecipe.Text = "Zapisz receptę";
            this.btnNewSellSaveRecipe.UseVisualStyleBackColor = true;
            this.btnNewSellSaveRecipe.Click += new System.EventHandler(this.btnNewSellSaveRecipe_Click);
            // 
            // btnNewSellSaveClient
            // 
            this.btnNewSellSaveClient.Location = new System.Drawing.Point(155, 495);
            this.btnNewSellSaveClient.Name = "btnNewSellSaveClient";
            this.btnNewSellSaveClient.Size = new System.Drawing.Size(149, 23);
            this.btnNewSellSaveClient.TabIndex = 15;
            this.btnNewSellSaveClient.Text = "Zapisz klienta";
            this.btnNewSellSaveClient.UseVisualStyleBackColor = true;
            this.btnNewSellSaveClient.Click += new System.EventHandler(this.btnNewSellSaveClient_Click);
            // 
            // btnNewSellSaveSell
            // 
            this.btnNewSellSaveSell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnNewSellSaveSell.Location = new System.Drawing.Point(462, 3);
            this.btnNewSellSaveSell.Name = "btnNewSellSaveSell";
            this.btnNewSellSaveSell.Size = new System.Drawing.Size(149, 23);
            this.btnNewSellSaveSell.TabIndex = 16;
            this.btnNewSellSaveSell.Text = "ZAPISZ SPRZEDAŻ";
            this.btnNewSellSaveSell.UseVisualStyleBackColor = true;
            this.btnNewSellSaveSell.Click += new System.EventHandler(this.btnNewSellSaveSell_Click);
            // 
            // formNewSell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 522);
            this.Controls.Add(this.btnNewSellSaveSell);
            this.Controls.Add(this.btnNewSellSaveClient);
            this.Controls.Add(this.btnNewSellSaveRecipe);
            this.Controls.Add(this.gbxNewSellNewClient);
            this.Controls.Add(this.gbxNewSellNewRecipe);
            this.Controls.Add(this.dgvNewSellClientInfo);
            this.Controls.Add(this.lblNewSellClient);
            this.Controls.Add(this.dgvNewSellRecipeInfo);
            this.Controls.Add(this.lblNewSellRecipe);
            this.Controls.Add(this.bnNewSell);
            this.Controls.Add(this.dgvNewSell);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "formNewSell";
            this.Text = "Nowa Sprzedaż";
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnNewSell)).EndInit();
            this.bnNewSell.ResumeLayout(false);
            this.bnNewSell.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsNewSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewSellClientInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewSellRecipeInfo)).EndInit();
            this.gbxNewSellNewRecipe.ResumeLayout(false);
            this.gbxNewSellNewRecipe.PerformLayout();
            this.gbxNewSellNewClient.ResumeLayout(false);
            this.gbxNewSellNewClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbNewSellClientSex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsNewSellRecipeInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsNewSellClientInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvNewSell;
        private System.Windows.Forms.BindingNavigator bnNewSell;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.BindingSource bsNewSell;
        private System.Windows.Forms.DataGridView dgvNewSellClientInfo;
        private System.Windows.Forms.Label lblNewSellClient;
        private System.Windows.Forms.DataGridView dgvNewSellRecipeInfo;
        private System.Windows.Forms.Label lblNewSellRecipe;
        private System.Windows.Forms.GroupBox gbxNewSellNewRecipe;
        private System.Windows.Forms.GroupBox gbxNewSellNewClient;
        private System.Windows.Forms.BindingSource bsNewSellRecipeInfo;
        private System.Windows.Forms.BindingSource bsNewSellClientInfo;
        private System.Windows.Forms.DataGridViewComboBoxColumn Nazwa;
        private System.Windows.Forms.DataGridViewComboBoxColumn Nr_partii;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ilosc;
        private System.Windows.Forms.DataGridViewComboBoxColumn ID_Recepty;
        private System.Windows.Forms.Label lblNewSellClientPESEL;
        private System.Windows.Forms.Label lblNewSellClientCity;
        private System.Windows.Forms.Label lblNewSellClientPostal;
        private System.Windows.Forms.Label lblNewSellClientAddress;
        private System.Windows.Forms.Label lblNewSellClientBornOn;
        private System.Windows.Forms.Label lblNewSellClientSex;
        private System.Windows.Forms.Label lblNewSellClientFirstname;
        private System.Windows.Forms.Label lblNewSellClientSurname;
        private System.Windows.Forms.TextBox tbxNewSellClientPESEL;
        private System.Windows.Forms.TextBox tbxNewSellClientCity;
        private System.Windows.Forms.TextBox tbxNewSellClientPostal;
        private System.Windows.Forms.TextBox tbxNewSellClientAddress;
        private System.Windows.Forms.DateTimePicker dtpNewSellClientBornOn;
        private System.Windows.Forms.TrackBar tbNewSellClientSex;
        private System.Windows.Forms.TextBox tbxNewSellClientFirstname;
        private System.Windows.Forms.TextBox tbxNewSellClientSurname;
        private System.Windows.Forms.ComboBox cbxNewSellRecipePESEL;
        private System.Windows.Forms.DateTimePicker dtpNewSellRecipeDate;
        private System.Windows.Forms.TextBox tbxNewSellRecipeDoc;
        private System.Windows.Forms.TextBox tbxNewSellRecipeId;
        private System.Windows.Forms.Label lblNewSellRecipeDate;
        private System.Windows.Forms.Label lblNewSellRecipeDoc;
        private System.Windows.Forms.Label lblNewSellRecipePESEL;
        private System.Windows.Forms.Label lblNewSellRecipeId;
        private System.Windows.Forms.Button btnNewSellSaveRecipe;
        private System.Windows.Forms.Button btnNewSellSaveClient;
        private System.Windows.Forms.Button btnNewSellSaveSell;
        private System.Windows.Forms.Label lblSexK;
        private System.Windows.Forms.Label lblSexM;
    }
}