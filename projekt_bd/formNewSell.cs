﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace projekt_bd
{
    public partial class formNewSell : Form
    {
        Database database;
        formApteka parent;

        public formNewSell(formApteka pt, Database db)
        {
            InitializeComponent();
            this.database = db;
            this.parent = pt;
            this.dgvNewSell.DataSource = this.bsNewSell;
            this.dgvNewSellRecipeInfo.DataSource = this.bsNewSellRecipeInfo;
            this.dgvNewSellClientInfo.DataSource = this.bsNewSellClientInfo;

            this.dgvNewSell.AllowUserToAddRows = false;
            this.dgvNewSellRecipeInfo.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            this.dgvNewSellRecipeInfo.AllowUserToResizeRows = false;
            this.dgvNewSellRecipeInfo.AllowUserToAddRows = false;
            this.dgvNewSellClientInfo.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            this.dgvNewSellClientInfo.AllowUserToResizeRows = false;
            this.dgvNewSellClientInfo.AllowUserToAddRows = false;

            DataTable meds = this.database.getDataSet("select distinct Nazwa from leki");
            foreach (DataRow row in meds.Rows)
            {
                ((DataGridViewComboBoxColumn)this.dgvNewSell.Columns["Nazwa"]).Items.Add(row[0].ToString());
            }

            meds = this.database.getDataSet("select ID_Recepty, Data_wystawienia from recepty");
            foreach (DataRow row in meds.Rows)
            {
                ((DataGridViewComboBoxColumn)this.dgvNewSell.Columns["ID_Recepty"]).Items.Add(row[0].ToString());
            }

            meds = this.database.getDataSet("select PESEL from klienci");
            foreach (DataRow row in meds.Rows)
            {
                this.cbxNewSellRecipePESEL.Items.Add(row[0].ToString());
            }
            cbxNewSellRecipePESEL.SelectedIndex = 0;

        }

        private void dgvNewSell_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (this.database != null && 0 <= e.RowIndex && e.RowIndex < this.dgvNewSell.Rows.Count
                && this.dgvNewSell.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                switch (e.ColumnIndex)
                {
                    case 0:
                        DataTable nr = this.database.getDataSet("select Nr_partii from leki where Nazwa LIKE '"
                            + this.dgvNewSell.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() + "'");

                        DataGridViewComboBoxCell cellNrPartii =
                            (DataGridViewComboBoxCell)this.dgvNewSell.Rows[e.RowIndex].Cells[1];

                        cellNrPartii.ReadOnly = false;
                        cellNrPartii.Value = null;
                        cellNrPartii.Items.Clear();
                        foreach (DataRow row in nr.Rows)
                        {
                            cellNrPartii.Items.Add(row[0].ToString());
                        }

                        ((DataGridViewTextBoxCell)this.dgvNewSell.Rows[e.RowIndex].Cells[2]).Value = null;
                        ((DataGridViewTextBoxCell)this.dgvNewSell.Rows[e.RowIndex].Cells[2]).ReadOnly = true;
                        ((DataGridViewComboBoxCell)this.dgvNewSell.Rows[e.RowIndex].Cells[3]).Value = null;
                        ((DataGridViewComboBoxCell)this.dgvNewSell.Rows[e.RowIndex].Cells[3]).ReadOnly = true;
                        break;
                    case 1:
                        ((DataGridViewTextBoxCell)this.dgvNewSell.Rows[e.RowIndex].Cells[2]).ReadOnly = false;
                        this.dgvNewSell.Rows[e.RowIndex].Cells[2].Value = "1";

                        DataRow dtr = this.database.getDataSet("select Na_recepte from leki where Nazwa LIKE '"
                            + this.dgvNewSell.Rows[e.RowIndex].Cells[0].Value.ToString()
                            + "' AND Nr_partii LIKE '" + this.dgvNewSell.Rows[e.RowIndex].Cells[1].Value.ToString() + "'").Rows[0];

                        this.dgvNewSell.Rows[e.RowIndex].Cells[3].ReadOnly =
                            !System.Text.RegularExpressions.Regex.IsMatch(dtr[0].ToString(), "^((true)|(1))$", 
                                System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                        if (!((DataGridViewComboBoxCell)this.dgvNewSell.Rows[e.RowIndex].Cells[3]).ReadOnly)
                            ((DataGridViewComboBoxCell)this.dgvNewSell.Rows[e.RowIndex].Cells[3]).Value =
                                ((DataGridViewComboBoxCell)this.dgvNewSell.Rows[e.RowIndex].Cells[3]).Items[0];

                        break;
                    case 3:
                        this.dgvNewSellRecipeInfo.DataSource = this.database.getDataSet("select * from recepty where ID_Recepty LIKE '"
                            + this.dgvNewSell.Rows[e.RowIndex].Cells[3].Value.ToString() + "'");

                        this.dgvNewSellClientInfo.DataSource = this.database.getDataSet(
                            "select distinct klienci.* from klienci join recepty on klienci.PESEL = recepty.PESEL"
                            + " where recepty.ID_Recepty LIKE '" +
                            this.dgvNewSell.Rows[e.RowIndex].Cells[3].Value.ToString() + "'");
                        break;
                }
            }
        }

        private void dgvNewSell_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            this.dgvNewSell.Rows[e.RowIndex].Cells[1].ReadOnly = true;
            this.dgvNewSell.Rows[e.RowIndex].Cells[2].ReadOnly = true;
            this.dgvNewSell.Rows[e.RowIndex].Cells[3].ReadOnly = true;
        }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (dgv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in dgv.SelectedRows)
                {
                    row.Selected = false;
                }
            }
        }

        private void dgvNewSell_SelectionChanged(object sender, EventArgs e)
        {
            if ((this.dgvNewSell.SelectedRows.Count > 0) &&
                (this.dgvNewSell.Rows[this.dgvNewSell.SelectedRows[0].Index].Cells[3].Value != null))
            {
                this.dgvNewSellRecipeInfo.DataSource = this.database.getDataSet("select * from recepty where ID_Recepty LIKE '"
                    + this.dgvNewSell.Rows[this.dgvNewSell.SelectedRows[0].Index].Cells[3].Value.ToString() + "'");

                this.dgvNewSellClientInfo.DataSource = this.database.getDataSet(
                    "select distinct klienci.* from klienci join recepty on klienci.PESEL = recepty.PESEL"
                    + " where recepty.ID_Recepty LIKE '" +
                    this.dgvNewSell.Rows[this.dgvNewSell.SelectedRows[0].Index].Cells[3].Value.ToString() + "'");
            }
        }

        private void btnNewSellSaveSell_Click(object sender, EventArgs e)
        {
            SqlTransaction transacion = this.database.getTransacion(IsolationLevel.ReadCommitted);
            try
            {
                SqlCommand insertSell = this.database.getCommand("dodaj_sprzedaz", transacion);
                insertSell.CommandType = CommandType.StoredProcedure;
                insertSell.Parameters.Add(new SqlParameter("@login", SqlDbType.NVarChar, 6)).Value = Account.id;
                int id = (int)insertSell.ExecuteScalar();

                if (id >= 0)
                {
                    foreach (DataGridViewRow row in this.dgvNewSell.Rows)
                    {
                        if (!row.Cells["Ilosc"].ReadOnly)
                        {
                            SqlCommand nameMed = this.database.getCommand("SELECT ID_Towaru FROM leki" +
                            " WHERE Nazwa LIKE @nazwa AND Nr_partii = @nr", transacion);

                            nameMed.Parameters.Add(new SqlParameter("@nazwa", SqlDbType.NVarChar, 50)).Value =
                            row.Cells["Nazwa"].Value.ToString();
                            nameMed.Parameters.Add(new SqlParameter("@nr", SqlDbType.Int)).Value =
                            Convert.ToInt32(row.Cells["Nr_partii"].Value);

                            SqlCommand insertSellMed = this.database.getCommand(
                            "INSERT INTO sprzedane_leki VALUES (@id_sprz, @id_tow, @nr, @ilosc, @id_rec)", transacion);

                            insertSellMed.Parameters.Add(new SqlParameter("@id_sprz", SqlDbType.Int)).Value = id;
                            insertSellMed.Parameters.Add(new SqlParameter("@id_tow", SqlDbType.Int)).Value =
                                (int)nameMed.ExecuteScalar();
                            insertSellMed.Parameters.Add(new SqlParameter("@nr", SqlDbType.Int)).Value =
                                Convert.ToInt32(row.Cells["Nr_partii"].Value);
                            insertSellMed.Parameters.Add(new SqlParameter("@ilosc", SqlDbType.Int)).Value =
                                Convert.ToInt32(row.Cells["Ilosc"].Value);

                            if (row.Cells["ID_Recepty"].ReadOnly)
                            {
                                insertSellMed.Parameters.Add(new SqlParameter("@id_rec", SqlDbType.BigInt)).Value =
                                    System.Data.SqlTypes.SqlInt64.Null;
                            }
                            else
                            {
                                insertSellMed.Parameters.Add(new SqlParameter("@id_rec", SqlDbType.BigInt)).Value =
                                    Convert.ToInt64(row.Cells["ID_Recepty"].Value);
                            }
                            insertSellMed.ExecuteNonQuery();
                        }
                    }
                }

                transacion.Commit();
                MessageBox.Show("Dodano nową sprzedaż pomyślnie!");
            }
            catch (SqlException exc)
            {
                MessageBox.Show(exc.Message);
                transacion.Rollback();
            }
            this.Close();
            this.parent.refreshTabSell();
        }

        private void btnNewSellSaveRecipe_Click(object sender, EventArgs e)
        {
            if (Regex.IsMatch(this.tbxNewSellRecipeId.Text, "^([ ]+)?$")
                || Regex.IsMatch(this.tbxNewSellRecipeDoc.Text, "^([ ]+)?$"))
            {
                MessageBox.Show("Błędne dane! Recepta nie została dodana!");
                return;
            }

            SqlCommand procedure = this.database.getProcedure("dodaj_recepte");
            procedure.Parameters.Add(new SqlParameter("@id", SqlDbType.BigInt)).Value =
                Convert.ToUInt64(this.tbxNewSellRecipeId.Text);
            procedure.Parameters.Add(new SqlParameter("@PESEL", SqlDbType.NChar, 11)).Value =
                this.cbxNewSellRecipePESEL.Text;
            procedure.Parameters.Add(new SqlParameter("@lekarz", SqlDbType.NChar, 7)).Value =
                this.tbxNewSellRecipeDoc.Text;
            procedure.Parameters.Add(new SqlParameter("@data_wyst", SqlDbType.Date)).Value =
                this.dtpNewSellRecipeDate.Value.ToShortDateString();

            int status = procedure.ExecuteNonQuery();
            MessageBox.Show((status > 0) ? "Recepta dodana pomyślnie!" : "Błędne dane! Recepta nie została dodana!");

            if (status > 0)
            {
                object[] values = new object[this.dgvNewSell.Rows.Count];

                for (int i = 0; i < this.dgvNewSell.Rows.Count; i++)
                {
                    values[i] = this.dgvNewSell.Rows[i].Cells[3].Value;
                }

                ((DataGridViewComboBoxColumn)this.dgvNewSell.Columns["ID_Recepty"]).Items.Clear();
                DataTable meds = this.database.getDataSet("select ID_Recepty, Data_wystawienia from recepty");
                foreach (DataRow row in meds.Rows)
                {
                    ((DataGridViewComboBoxColumn)this.dgvNewSell.Columns["ID_Recepty"]).Items.Add(row[0].ToString());
                }

                for (int i = 0; i < this.dgvNewSell.Rows.Count; i++)
                {
                    this.dgvNewSell.Rows[i].Cells[3].Value = values[i];
                }
            }
        }

        private void tbNewSellClientSex_ValueChanged(object sender, EventArgs e)
        {
            this.lblSexK.Enabled = (this.tbNewSellClientSex.Value > 0) ?
                !(this.lblSexM.Enabled = true) : !(this.lblSexM.Enabled = false);
        }

        private void btnNewSellSaveClient_Click(object sender, EventArgs e)
        {
            SqlCommand procedure = this.database.getProcedure("dodaj_klienta");

            procedure.Parameters.Add(new SqlParameter("@Nazwisko", SqlDbType.NVarChar, 30)).Value =
                this.tbxNewSellClientSurname.Text;
            procedure.Parameters.Add(new SqlParameter("@Imie", SqlDbType.NVarChar, 25)).Value =
                this.tbxNewSellClientFirstname.Text;
            procedure.Parameters.Add(new SqlParameter("@Plec", SqlDbType.NChar, 1)).Value =
                (this.lblSexK.Enabled) ? this.lblSexK.Text : this.lblSexM.Text;
            procedure.Parameters.Add(new SqlParameter("@Data_ur", SqlDbType.Date)).Value =
                this.dtpNewSellClientBornOn.Value.ToShortDateString();
            procedure.Parameters.Add(new SqlParameter("@Adres", SqlDbType.VarChar, 50)).Value =
                this.tbxNewSellClientAddress.Text;
            procedure.Parameters.Add(new SqlParameter("@Kod_poczt", SqlDbType.NChar, 6)).Value =
                this.tbxNewSellClientPostal.Text;
            procedure.Parameters.Add(new SqlParameter("@Miejscowosc", SqlDbType.VarChar, 30)).Value =
                this.tbxNewSellClientCity.Text;
            procedure.Parameters.Add(new SqlParameter("@PESEL", SqlDbType.NChar, 11)).Value =
                this.tbxNewSellClientPESEL.Text;
            procedure.Parameters.Add(new SqlParameter("@ReturnedValue", SqlDbType.Int)).Direction =
                ParameterDirection.ReturnValue;

            procedure.ExecuteNonQuery();

            int retVal = (int)procedure.Parameters["@ReturnedValue"].Value;
            MessageBox.Show(getClientAddingInfo(retVal));

            if (retVal == 0)
            {
                string value = this.cbxNewSellRecipePESEL.Text;
                this.cbxNewSellRecipePESEL.Items.Clear();

                DataTable meds = this.database.getDataSet("select PESEL from klienci");
                foreach (DataRow row in meds.Rows)
                {
                    this.cbxNewSellRecipePESEL.Items.Add(row[0].ToString());
                }

                this.cbxNewSellRecipePESEL.Text = value;
            }
        }

        private string getClientAddingInfo(int nr)
        {
            switch (nr)
            {
                case 0:
                    return "Klient dodany pomyślnie!";
                case 1:
                    return "Błędny PESEL!";
                case 2:
                    return "Niewłaściwy kod pocztowy!";
                case 3:
                    return "Niepoprawna płeć!";
                case 4:
                    return "Data urodzenia nie może być z przyszłości!";
                case 5:
                    return "Podany PESEL już istnieje w bazie!";
                case 6:
                    return "Nazwisko nie może być puste!";
                case 7:
                    return "Imię nie może być puste!";
            }
            return "#ERROR!";
        }
    }
}