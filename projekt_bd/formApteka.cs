﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Collections;
using System.Text.RegularExpressions;

namespace projekt_bd {
    public partial class formApteka : Form {
        Database database;
        public AptekaDataContext aptekaDC = new AptekaDataContext();
        public dostawy nowa_dostawa;

        public formApteka() {
            InitializeComponent();
            this.database = new Database();
        }

        /**
         * Binds data sources to DataGridViews and comboLists.
         */
        private void Form1_Load(object sender, EventArgs e) {
            this.dgvMeds.DataSource = this.bsMeds;
            this.dgvSell.DataSource = this.bsSell;
            this.dgvSellMeds.DataSource = this.bsSellMeds;
            this.dgvRecipeInfo.DataSource = this.bsRecipeInfo;
            this.dgvClient.DataSource = this.bsClient;
            this.farmaceuciBindingSource.DataSource = this.aptekaDC.farmaceuci;
            this.farmaceuciDataGridView.DataSource = this.farmaceuciBindingSource;
            this.dostawcyBindingSource.DataSource = this.aptekaDC.dostawcy;
            this.lekiBindingSource.DataSource = this.aptekaDC.leki;
            this.comboBoxFarmaceuciKolumny.DataSource = Filter.getListOfFilters(typeof(farmaceuci));

            this.bsHistory.DataSource = this.aptekaDC.sprzedaz;
            dgvHistory.DataSource = this.bsHistory;
        }

        private void btnSaveMedsChanges_Click(object sender, EventArgs e) {
            this.database.update((DataTable) bsMeds.DataSource);
        }

        private void btnSaveFarmaceuci_Click(object sender, EventArgs e) {
            try {
                this.aptekaDC.SubmitChanges();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnFarmaceuciFiltruj_Click(object sender, EventArgs e) {
            IQueryable<farmaceuci> query = this.aptekaDC.farmaceuci;

            foreach (Control control in tableFiltry.Controls) {
                if (control is TextBox) {
                    if (control.Name.Contains("Nazwisko")) {
                        query = query.Where(f => f.Nazwisko.Contains(control.Text));
                    }
                    if (control.Name.Contains("Imie")) {
                        query = query.Where(f => f.Imie.Contains(control.Text));
                    }
                    if (control.Name.Contains("Data_ur")) {
                        query = query.Where(f => f.Data_ur.ToString().Contains(control.Text));
                    }
                    if (control.Name.Contains("Adres")) {
                        query = query.Where(f => f.Adres.Contains(control.Text));
                    }
                    if (control.Name.Contains("Kod_poczt")) {
                        query = query.Where(f => f.Kod_poczt.Contains(control.Text));
                    }
                    if (control.Name.Contains("Miejscowosc")) {
                        query = query.Where(f => f.Miejscowosc.Contains(control.Text));
                    }
                    if (control.Name.Contains("Telefon")) {
                        query = query.Where(f => f.Telefon.Contains(control.Text));
                    }
                    if (control.Name.Contains("ID_Farmaceuty")) {
                        query = query.Where(f => f.ID_Farmaceuty.Contains(control.Text));
                    }
                }
            }
            query = query.OrderBy(c => c.Nazwisko);

            farmaceuciBindingSource.DataSource = query;
            btnFarmaceuciKasujFiltry.Enabled = true;
        }

        private void btnFarmaceuciAddSelectedFIlter_Click(object sender, EventArgs e) {
            string columnName = (string) comboBoxFarmaceuciKolumny.SelectedItem;

            Label columnLabel = new Label();
            columnLabel.Text = columnName;
            columnLabel.Visible = true;

            TextBox columnTextBox = new TextBox();
            columnTextBox.Name = "txtFilter_" + columnName;
            columnTextBox.Visible = true;

            RowStyle newRow = new RowStyle(SizeType.AutoSize);
            tableFiltry.RowStyles.Add(newRow);

            // TODO poprawić dodawnie do tabeli
            tableFiltry.Controls.Add(columnLabel, 0, tableFiltry.RowCount - 1);
            tableFiltry.Controls.Add(columnTextBox, 0, tableFiltry.RowCount - 1);

            // Currently chosen filter in the list has already beed added
            // We block the button, unblocking check happens in comboBoxFarmaceuciKolumny_SelectionChangeCommitted
            btnFarmaceuciAddSelectedFIlter.Enabled = false;
            btnFarmaceuciKasujFiltry.Enabled = true;
        }

        private void btnFarmaceuciKasujFiltry_Click(object sender, EventArgs e) {
            farmaceuciBindingSource.DataSource = this.aptekaDC.farmaceuci;
            tableFiltry.Controls.Clear();
            btnFarmaceuciKasujFiltry.Enabled = false;
            btnFarmaceuciAddSelectedFIlter.Enabled = true;
        }

        /**
         * Adding a filter which is already present in the filterTable is not allowed.
         */
        private void comboBoxFarmaceuciKolumny_SelectionChangeCommitted(object sender, EventArgs e) {
            Boolean foundInTable = false;

            foreach (Control control in tableFiltry.Controls) {
                if (control.Text.Equals(comboBoxFarmaceuciKolumny.SelectedItem)) {
                    foundInTable = true;
                    btnFarmaceuciAddSelectedFIlter.Enabled = false;
                    break;
                }
            }
            if (!foundInTable) {
                btnFarmaceuciAddSelectedFIlter.Enabled = true;
            }
        }

        private void additionalMedInfo_Click(object sender, EventArgs e) {
            int selectedRowIndex = dgvMeds.SelectedCells[0].RowIndex;
            int IDLeku = (int) dgvMeds.Rows[selectedRowIndex].Cells["ID_Towaru"].Value;
            string medName = (string) dgvMeds.Rows[selectedRowIndex].Cells["Nazwa"].Value;

            try {
                formMedDetails medDetails = new formMedDetails(IDLeku, medName);
                medDetails.ShowDialog();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void farmaceuciDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
            string headerText = farmaceuciDataGridView.Columns[e.ColumnIndex].HeaderText;

            switch (headerText) {
                case "Nazwisko":
                    // Confirm that the cell is not empty and shorter than 30 chars. 
                    if (string.IsNullOrEmpty(e.FormattedValue.ToString()) || e.FormattedValue.ToString().Length > 30) {
                        farmaceuciDataGridView.Rows[e.RowIndex].ErrorText = "Nazwisko nie może być puste ani dłuższe niż 30 znaków.";
                        e.Cancel = true;
                    }
                    break;
                case "Imie":
                    // Confirm that the cell is not empty and shorter than 25 chars. 
                    if (string.IsNullOrEmpty(e.FormattedValue.ToString()) || e.FormattedValue.ToString().Length > 25) {
                        farmaceuciDataGridView.Rows[e.RowIndex].ErrorText = "Imie nie może być puste ani dłuższe niż 25 znaków.";
                        e.Cancel = true;
                    }
                    break;
                case "Data_ur":
                    DateTime tempDate = new DateTime();

                    // Confirm that the cell is a valid date. 
                    if (e.FormattedValue.ToString().Length > 1 && !DateTime.TryParse(e.FormattedValue.ToString(), out tempDate)) {
                        farmaceuciDataGridView.Rows[e.RowIndex].ErrorText = "Data_ur musi być poprawną datą.";
                        e.Cancel = true;
                    }
                    break;
                case "Adres":
                    // Confirm that the cell is shorter than 50 chars. 
                    if (e.FormattedValue.ToString().Length > 50) {
                        farmaceuciDataGridView.Rows[e.RowIndex].ErrorText = "Adres nie może być dłuższe niż 50 znaków.";
                        e.Cancel = true;
                    }
                    break;
                case "Kod_poczt":
                    // Confirm that the cell is shorter than 6 chars. 
                    if (e.FormattedValue.ToString().Length > 6) {
                        farmaceuciDataGridView.Rows[e.RowIndex].ErrorText = "Kod_poczt nie może być dłuższe niż 6 znaków.";
                        e.Cancel = true;
                    }
                    break;
                case "Miejscowosc":
                    // Confirm that the cell is shorter than 30 chars. 
                    if (e.FormattedValue.ToString().Length > 30) {
                        farmaceuciDataGridView.Rows[e.RowIndex].ErrorText = "Miejscowość nie może być dłuższe niż 30 znaków.";
                        e.Cancel = true;
                    }
                    break;
                case "Telefon":
                    // Confirm that the cell is shorter than 20 chars. 
                    if (e.FormattedValue.ToString().Length > 20) {
                        farmaceuciDataGridView.Rows[e.RowIndex].ErrorText = "Telefon nie może być dłuższe niż 20 znaków.";
                        e.Cancel = true;
                    }
                    break;
                case "ID_Farmaceuty":
                    // Confirm that the cell is shorter than 16 chars. 
                    if (e.FormattedValue.ToString().Length > 16) {
                        farmaceuciDataGridView.Rows[e.RowIndex].ErrorText = "ID_Farmaceuty nie może być dłuższe niż 16 znaków.";
                        e.Cancel = true;
                    }
                    break;
                default:
                    return;
            }
        }

        private void farmaceuciDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e) {
            // Clear the row error in case the user presses ESC.   
            farmaceuciDataGridView.Rows[e.RowIndex].ErrorText = String.Empty;
        }

        private void cbx_tabMeds_filter_Id_CheckedChanged(object sender, EventArgs e)
        {
            this.tbx_tabMeds_filter_Id.Enabled = this.cb_tabMeds_filter_Id.Enabled = ((CheckBox)sender).Checked;
        }

        private void cbx_tabMeds_filter_Name_CheckedChanged(object sender, EventArgs e)
        {
            this.tbx_tabMeds_filter_Name.Enabled = this.cb_tabMeds_filter_Name.Enabled = ((CheckBox)sender).Checked;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.tbx_tabMeds_filter_Nr.Enabled = this.cb_tabMeds_filter_Nr.Enabled = ((CheckBox)sender).Checked;
        }

        private void cbx_tabMeds_filter_Count_CheckedChanged(object sender, EventArgs e)
        {
            this.tbx_tabMeds_filter_Count.Enabled = this.cb_tabMeds_filter_Count.Enabled = ((CheckBox)sender).Checked;
        }

        private void cbx_tabMeds_filter_Price_CheckedChanged(object sender, EventArgs e)
        {
            this.tbx_tabMeds_filter_Price.Enabled = this.cb_tabMeds_filter_Price.Enabled = ((CheckBox)sender).Checked;
        }

        private void cbx_tabMeds_filter_Recipe_CheckedChanged(object sender, EventArgs e)
        {
            this.cb_tabMeds_filter_Recipe.Enabled = ((CheckBox)sender).Checked;
        }

        private void btnAddFilter_Click(object sender, EventArgs e)
        {
            refreshListViewActiveFilter();
            executeSqlQueryMeds();
            this.btnRemoveFilters.Enabled = (this.listView_ActiveFilters_Meds.Items.Count > 0) ? true : false;        }

        private void btnRemoveFilters_Click(object sender, EventArgs e)
        {
            this.btnAddFilters.Enabled = true;
            this.btnRemoveFilters.Enabled = false;
            this.listView_ActiveFilters_Meds.Items.Clear();
            this.bsMeds.DataSource = this.database.getDataSet("select * from leki");
        }

        private void executeSqlQueryMeds()
        {
            StringBuilder query = new StringBuilder();
            query.Append("select * from leki");

            if (this.cbx_tabMeds_filter_Id.Checked || this.cbx_tabMeds_filter_Name.Checked || this.cbx_tabMeds_filter_Nr.Checked
                || this.cbx_tabMeds_filter_Count.Checked || this.cbx_tabMeds_filter_Price.Checked || this.cbx_tabMeds_filter_Recipe.Checked)
            {
                query.Append(" where");
                query.Append(getTextValue(this.cbx_tabMeds_filter_Id, this.cb_tabMeds_filter_Id, this.tbx_tabMeds_filter_Id));
                query.Append(getTextValue(this.cbx_tabMeds_filter_Name, this.cb_tabMeds_filter_Name, this.tbx_tabMeds_filter_Name));
                query.Append(getTextValue(this.cbx_tabMeds_filter_Nr, this.cb_tabMeds_filter_Nr, this.tbx_tabMeds_filter_Nr));
                query.Append(getNumberValue(this.cbx_tabMeds_filter_Count, this.cb_tabMeds_filter_Count, this.tbx_tabMeds_filter_Count));
                query.Append(getNumberValue(this.cbx_tabMeds_filter_Price, this.cb_tabMeds_filter_Price, this.tbx_tabMeds_filter_Price));
                if (cbx_tabMeds_filter_Recipe.Checked)
                {
                    if (cb_tabMeds_filter_Recipe.SelectedIndex == 0) query.Append(" leki.Na_recepte = 1");
                    else query.Append(" leki.Na_recepte = 0");
                }

            }

            string queryStr = query.ToString();
            if(Regex.IsMatch(queryStr, ".* AND$")) queryStr = queryStr.Substring(0, query.ToString().Length - 4);

            this.bsMeds.DataSource = this.database.getDataSet(queryStr);
        }

        private string getTextValue(CheckBox cbx, ComboBox cb, TextBox tbx)
        {
            if (cbx.Checked)
            {
                switch (cb.SelectedIndex)
                {
                    case 0:
                        return " leki." + cbx.Text + " LIKE '%" + tbx.Text.Trim() + "%' AND";
                    case 1:
                        return " leki." + cbx.Text + " LIKE '" + tbx.Text.Trim() + "%' AND";
                    case 2:
                        return " leki." + cbx.Text + " LIKE '%" + tbx.Text.Trim() + "' AND";
                }
            }

            return "";
        }

        private string getNumberValue(CheckBox cbx, ComboBox cb, TextBox tbx)
        {
            if (cbx.Checked)
            {
                switch (cb.SelectedIndex)
                {
                    case 0:
                        return " leki." + cbx.Text + " >= " + tbx.Text.Trim() + " AND";
                    case 1:
                        return " leki." + cbx.Text + " > " + tbx.Text.Trim() + " AND";
                    case 2:
                        return " leki." + cbx.Text + " = " + tbx.Text.Trim() + " AND";
                    case 3:
                        return " leki." + cbx.Text + " < " + tbx.Text.Trim() + " AND";
                    case 4:
                        return " leki." + cbx.Text + " <= " + tbx.Text.Trim() + " AND";
                }
            }

            return "";
        }

        private void refreshListViewActiveFilter()
        {
            this.listView_ActiveFilters_Meds.Items.Clear();

            if (this.cbx_tabMeds_filter_Id.Checked)
            {
                this.listView_ActiveFilters_Meds.Items.Add(this.cbx_tabMeds_filter_Id.Text + " "
                    + this.cb_tabMeds_filter_Id.SelectedItem.ToString() + ":");
                this.listView_ActiveFilters_Meds.Items.Add("   " + this.tbx_tabMeds_filter_Id.Text);
                this.listView_ActiveFilters_Meds.Items.Add("");
            }
            if (this.cbx_tabMeds_filter_Name.Checked)
            {
                this.listView_ActiveFilters_Meds.Items.Add(this.cbx_tabMeds_filter_Name.Text + " "
                    + this.cb_tabMeds_filter_Name.SelectedItem.ToString() + ":");
                this.listView_ActiveFilters_Meds.Items.Add("   " + this.tbx_tabMeds_filter_Name.Text);
                this.listView_ActiveFilters_Meds.Items.Add("");
            }
            if (this.cbx_tabMeds_filter_Nr.Checked)
            {
                this.listView_ActiveFilters_Meds.Items.Add(this.cbx_tabMeds_filter_Nr.Text + " "
                    + this.cb_tabMeds_filter_Nr.SelectedItem.ToString() + ":");
                this.listView_ActiveFilters_Meds.Items.Add("   " + this.tbx_tabMeds_filter_Nr.Text);
                this.listView_ActiveFilters_Meds.Items.Add("");
            }
            if (this.cbx_tabMeds_filter_Count.Checked)
            {
                this.listView_ActiveFilters_Meds.Items.Add(this.cbx_tabMeds_filter_Count.Text + " "
                    + this.cb_tabMeds_filter_Count.SelectedItem.ToString() + ":");
                this.listView_ActiveFilters_Meds.Items.Add("   " + this.tbx_tabMeds_filter_Count.Text);
                this.listView_ActiveFilters_Meds.Items.Add("");
            }
            if (this.cbx_tabMeds_filter_Price.Checked)
            {
                this.listView_ActiveFilters_Meds.Items.Add(this.cbx_tabMeds_filter_Price.Text + " "
                    + this.cb_tabMeds_filter_Price.SelectedItem.ToString() + ":");
                this.listView_ActiveFilters_Meds.Items.Add("   " + this.tbx_tabMeds_filter_Price.Text);
                this.listView_ActiveFilters_Meds.Items.Add("");
            }
            if (this.cbx_tabMeds_filter_Recipe.Checked)
            {
                this.listView_ActiveFilters_Meds.Items.Add(this.cbx_tabMeds_filter_Recipe.Text + " "
                    + this.cb_tabMeds_filter_Recipe.SelectedItem.ToString());
            }
        }

        private void tbx_tabMeds_filter_Leave(object sender, EventArgs e)
        {
            TextBox tbx = (TextBox)sender;
            if (!Regex.IsMatch(tbx.Text, "^[0-9]+(,[0-9]+)?$")) tbx.Text = "0";
        }

        private void dgvMeds_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string headerText = dgvMeds.Columns[e.ColumnIndex].HeaderText;

            switch (headerText)
            {
                case "ID_Towaru":
                    if (string.IsNullOrEmpty(e.FormattedValue.ToString()) || e.FormattedValue.ToString().Length > 8)
                    {
                        dgvMeds.Rows[e.RowIndex].ErrorText = "ID_Towaru nie może być puste ani dłuższe niż 8 znaków.";
                        e.Cancel = true;
                    }
                    break;
                case "Nazwa":
                    if (string.IsNullOrEmpty(e.FormattedValue.ToString()) || e.FormattedValue.ToString().Length > 50)
                    {
                        dgvMeds.Rows[e.RowIndex].ErrorText = "Nazwa nie może być pusta ani dłuższa niż 50 znaków.";
                        e.Cancel = true;
                    }
                    break;
                case "Ilosc":
                    int tempCount;
                    if (!string.IsNullOrEmpty(e.FormattedValue.ToString()) && (!Int32.TryParse(e.FormattedValue.ToString(), out tempCount) || tempCount < 0))
                    {
                        dgvMeds.Rows[e.RowIndex].ErrorText = "Ilosc musi być liczbą naturalną lub zerem!";
                        e.Cancel = true;
                    }
                    break;
                case "Nr_partii":
                    if (string.IsNullOrEmpty(e.FormattedValue.ToString()) || e.FormattedValue.ToString().Length > 8)
                    {
                        dgvMeds.Rows[e.RowIndex].ErrorText = "Nr_partii nie może być pusty ani dłuższy niż 8 znaków.";
                        e.Cancel = true;
                    }
                    break;
                case "Cena":
                    double tempPrice;
                    if (!string.IsNullOrEmpty(e.FormattedValue.ToString()) && (!Double.TryParse(e.FormattedValue.ToString(), out tempPrice) || tempPrice < 0))
                    {
                        dgvMeds.Rows[e.RowIndex].ErrorText = "Ilosc musi być liczbą naturalną lub zerem!";
                        e.Cancel = true;
                    }
                    break;
                default:
                    return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dostawcy dst = new dostawcy()
            {
                Nazwa = ndostawcaNazwa.Text,
                Adres = ndostawcaAdres.Text,
                Kod_poczt = ndostawcaKod.Text,
                Miejscowosc = ndostawcaMiejscowosc.Text,
                Telefon = ndostawcaTelefon.Text                
            };

            aptekaDC.dostawcy.InsertOnSubmit(dst);
            aptekaDC.SubmitChanges();
            CBox_dostawca.Refresh();

            ndostawcaAdres.Clear();
            ndostawcaKod.Clear();
            ndostawcaMiejscowosc.Clear();
            ndostawcaNazwa.Clear();
            ndostawcaTelefon.Clear();
        }

        private void dgvSell_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgvSell.SelectedRows.Count > 0)
            {
                this.bsSellMeds.DataSource = this.database.getDataSet(
                    "select * from sprzedane_leki where Id_Sprzedazy LIKE '"
                    + this.dgvSell.SelectedRows[0].Cells["Id_sprzedazy"].Value.ToString() + "'");

                this.dgvSellMeds.Columns["ID_Towaru"].Visible = false;

                if (this.dgvSellMeds.Columns["Nazwa"] == null)
                {
                    this.dgvSellMeds.Columns.Add(new DataGridViewColumn(this.dgvSellMeds.Rows[0].Cells[0]));
                    this.dgvSellMeds.Columns[this.dgvSellMeds.ColumnCount - 1].Name = "Nazwa";
                    this.dgvSellMeds.Columns["Nazwa"].DisplayIndex = 0;
                    this.dgvSellMeds.Columns["ID_Towaru"].DisplayIndex = this.dgvSellMeds.Columns.Count - 1;
                }

                foreach (DataGridViewRow row in this.dgvSellMeds.Rows)
                {
                    row.Cells["Nazwa"].Value = 
                        this.database.getDataSet("select Nazwa from leki where ID_Towaru = " +
                            row.Cells["ID_Towaru"].Value.ToString()).Rows[0][0].ToString();
                }
            }
        }

        private void dgvSellMeds_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgvSellMeds.SelectedRows.Count > 0)
            {
                this.bsRecipeInfo.DataSource = this.database.getDataSet(
                    "select * from recepty where ID_Recepty LIKE '"
                    + this.dgvSellMeds.SelectedRows[0].Cells["ID_Recepty"].Value.ToString() + "'");
                if (this.dgvRecipeInfo.Rows.Count > 0 && this.dgvRecipeInfo.Rows[0].Cells["PESEL"].Value != null)
                {
                    this.dgvClient.DataSource = this.database.getDataSet(
                    "select * from klienci where PESEL LIKE '"
                    + this.dgvRecipeInfo.Rows[0].Cells["PESEL"].Value.ToString() + "'");
                }
                else
                {
                    this.dgvClient.DataSource = this.database.getDataSet(
                    "select * from klienci where PESEL LIKE ''");
                }
            }
        }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (dgv.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in dgv.SelectedRows)
                {
                    row.Selected = false;
                }
            }
        }

        private void cbx_tabSell_filter_Date_CheckedChanged(object sender, EventArgs e)
        {
            this.dtp_tabSell_filter_Date.Enabled = this.cb_tabSell_filter_Date.Enabled = ((CheckBox)sender).Checked;
        }

        private void cbx_tabSell_filter_Value_CheckedChanged(object sender, EventArgs e)
        {
            this.tbx_tabSell_filter_Value.Enabled = this.cb_tabSell_filter_Value.Enabled = ((CheckBox)sender).Checked;
        }

        private void btnSellDelFil_Click(object sender, EventArgs e)
        {
            this.btnSellDelFil.Enabled = false;
            this.btnSellAddFil.Enabled = true;
            this.refreshTabSell();
        }
        
        private void btnSellAddFil_Click(object sender, EventArgs e)
        {
            StringBuilder query = new StringBuilder();
            query.Append("select * from sprzedaz");

            if (this.cbx_tabSell_filter_Date.Checked || this.cbx_tabSell_filter_Value.Checked)
            {
                query.Append(" where");
                if (this.cbx_tabSell_filter_Date.Checked)
                {
                    if (cb_tabSell_filter_Date.SelectedIndex == 1)
                    {
                        query.Append(" CONVERT( CHAR(8), sprzedaz.data, 112) LIKE ");
                        query.Append("'" + this.dtp_tabSell_filter_Date.Value.ToString("yyyyMMdd") + "%' AND");
                    }
                    else
                    {
                        query.Append(" " + this.cbx_tabSell_filter_Date.Text);
                        switch (cb_tabSell_filter_Date.SelectedIndex)
                        {
                            case 0:
                                query.Append(" < ");
                                break;
                            case 2:
                                query.Append(" > ");
                                break;
                        }
                        query.Append("'" + this.dtp_tabSell_filter_Date.Value.ToShortDateString() + "' AND");
                    }
                }
                if (this.cbx_tabSell_filter_Date.Checked) { }
                if (!Account.roleManager) query.Append(" ID_Farmaceuty LIKE '" + Account.id + "'");
            }
            else
            {
                if (!Account.roleManager) query.Append(" where ID_Farmaceuty LIKE '" + Account.id + "'");
            }
            string queryStr = query.ToString();
            if(Regex.IsMatch(queryStr, ".* AND$")) queryStr = queryStr.Substring(0, query.ToString().Length - 4);
            this.bsSell.DataSource = this.database.getDataSet(queryStr);

            this.refreshValueSell();

            this.btnSellDelFil.Enabled = true;
        }

        private void btnSellNewSell_Click(object sender, EventArgs e)
        {
            try
            {
                formNewSell newSell = new formNewSell(this, this.database);
                newSell.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void btn_dodaj_lek_Click(object sender, EventArgs e)
        {
            try
            {
                int ilosc = int.Parse(TBox_ilosc.Text);
                DateTime data = DateTime.Parse(MTBox_data_waznosci.Text);

                leki lek = (leki)lekiBindingSource.Current;
                dostarczone_leki dl = new dostarczone_leki()
                {
                    ID_Towaru = lek.ID_Towaru,
                    Nr_partii = lek.Nr_partii,
                    Ilosc = ilosc,
                    Data_waznosci = data

                };

                try
                {
                    nowa_dostawa.dostarczone_leki.Add(dl);
                    aptekaDC.SubmitChanges();
                }
                catch
                {
                    aptekaDC.Transaction.Rollback();
                }
            }
            catch
            { MessageBox.Show("Niepoprawne dane"); }
        }

        private void btn_wybierz_dostawce_Click(object sender, EventArgs e)
        {
            aptekaDC.Connection.Open();
            aptekaDC.Transaction = aptekaDC.Connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
            dostawcy dostawca = (dostawcy)dostawcyBindingSource.Current;
            nowa_dostawa = new dostawy()
            {
                ID_Dostawcy = dostawca.ID_Dostawcy,
                Data = DateTime.Now
            };
            try
            {
                aptekaDC.dostawy.InsertOnSubmit(nowa_dostawa);
                aptekaDC.SubmitChanges();
            }
            catch
            {
                aptekaDC.Transaction.Rollback();
            }

            CBox_dostawca.Enabled = false;
            btn_wybierz_dostawce.Enabled = false;
            btn_dodaj_lek.Enabled = true;
            btn_anuluj_dostawe.Enabled = true;
            btn_zapisz_dostawe.Enabled = true;
        }

        private void btn_zapisz_dostawe_Click(object sender, EventArgs e)
        {
            try
            {
                aptekaDC.Transaction.Commit();
            }
            catch
            {
                aptekaDC.Transaction.Rollback();
            }
            

            CBox_dostawca.Enabled = true;
            btn_wybierz_dostawce.Enabled = true;
            btn_dodaj_lek.Enabled = false;
            btn_anuluj_dostawe.Enabled = false;
            btn_zapisz_dostawe.Enabled = false;
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btn_anuluj_dostawe_Click(object sender, EventArgs e)
        {
            aptekaDC.Transaction.Rollback();

            CBox_dostawca.Enabled = true;
            btn_wybierz_dostawce.Enabled = true;
            btn_dodaj_lek.Enabled = false;
            btn_anuluj_dostawe.Enabled = false;
            btn_zapisz_dostawe.Enabled = false;
        }

        private void btn_odswiez_Click(object sender, EventArgs e)
        {
            aptekaDC = new AptekaDataContext();
            aptekaDC.Refresh(System.Data.Linq.RefreshMode.KeepChanges, aptekaDC.leki);
            lekiBindingSource.DataSource = this.aptekaDC.leki;
            dataGridView1.Refresh();
        }

        private void btnHistoryMySell_Click(object sender, EventArgs e) {
            var query = this.aptekaDC.sprzedaz.Where(s => s.ID_Farmaceuty.Equals(Account.id));
            this.bsHistory.DataSource = query;
        }

        private void btnHistoryMySellProcedure_Click(object sender, EventArgs e) {
            using (AptekaDataContext db = new AptekaDataContext()) {
                ISingleResult<wyszukaj_sprzedazResult> result = db.wyszukaj_sprzedaz('F', Account.id);

                this.bsHistory.DataSource = result;
                this.dgvHistory.DataSource = this.bsHistory;
            }
        }

        private void btnHistoryCompareSell_Click(object sender, EventArgs e) {
            using (AptekaDataContext db = new AptekaDataContext()) {
                ISingleResult<porownaj_sprzedazResult> result = db.porownaj_sprzedaz(txtHistoryCompareId.Text);

                if (result.ReturnValue.ToString() == "-1") {
                    MessageBox.Show(string.Format("Farmaceuta o ID \"{0}\" nie istnieje! Nie mogę wyświetlić statystyk.", txtHistoryCompareId.Text));
                } else {
                    this.bsHistory.DataSource = result;
                    this.dgvHistory.DataSource = this.bsHistory;
                }
            }
        }

        private void btn_zmien_cene_Click(object sender, EventArgs e)
        {
            leki lek = (leki)lekiBindingSource.Current;
            lek.Cena = decimal.Parse(TBox_nowa_cena.Text);
            aptekaDC.SubmitChanges();

            aptekaDC.Refresh(System.Data.Linq.RefreshMode.KeepChanges, aptekaDC.leki);
            lekiBindingSource.DataSource = this.aptekaDC.leki;
            dataGridView1.Refresh();
        }

        private void btnHistoryShowBestPharmacist_Click(object sender, EventArgs e) {
            using (AptekaDataContext db = new AptekaDataContext()) {
                string farmaceuta = "";

                int result = db.farmaceuta_najwiecej_sprzedanych_lekow(ref farmaceuta);

                if (result > 0) {
                    MessageBox.Show("Błąd, nie znaleziono farmaceuty!");
                }
                MessageBox.Show("Farmaceuta o największej liczbie sprzedaży to: " + farmaceuta);
            }
        }
    }
}