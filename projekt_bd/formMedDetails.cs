﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace projekt_bd {
    public partial class formMedDetails : Form {
        private int medID;
        private string datagridMedName;
        private bool isNewDescription;
        private XDocument doc;
        private string docPath;
        private IEnumerable<XElement> medDesc;

        public formMedDetails(int medID, string medName) {
            InitializeComponent();

            this.medID = medID;
            this.datagridMedName = medName;
            this.docPath = @"../../../database/additional-xml/additional-med-info.xml";
            this.doc = XDocument.Load(this.docPath);
        }

        private void formMedDetails_Load(object sender, EventArgs e) {
            var medDesc = from d in this.doc.Descendants("med")
                          where d.Attribute("id").Value == this.medID.ToString()
                          select d;

            txtMedDetailsID.Text = this.medID.ToString();

            if (medDesc.Count() == 0) {
                // Found nothing
                this.isNewDescription = true;
                // If Name is empty, autopopulate it with med name from Meds Datagrid
                txtMedDetailsName.Text = this.datagridMedName;
            } else {
                this.medDesc = medDesc;
                this.isNewDescription = false;
                txtMedDetailsName.Text = (string) medDesc.ElementAt(0).Attribute("name").Value;

                if (String.IsNullOrEmpty(txtMedDetailsName.Text)) {
                    // If Name is empty, autopopulate it with med name from Meds Datagrid
                    txtMedDetailsName.Text = this.datagridMedName;
                }
                rtxtMedDetailsDescription.Text = (string) medDesc.ElementAt(0).Element("desc").Value;
            }
        }

        private void btnMedDetailsClose_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void btnMedDetailsSave_Click(object sender, EventArgs e) {
            if (this.isNewDescription) {
                this.doc.Element("meds").Add(new XElement("med",
                    new XAttribute("id", txtMedDetailsID.Text),
                    new XAttribute("name", txtMedDetailsName.Text),
                    new XElement("desc", rtxtMedDetailsDescription.Text)
                ));
            } else {
                // Updates existing XElement
                this.medDesc.ElementAt(0).Attribute("id").Value = txtMedDetailsID.Text;
                this.medDesc.ElementAt(0).Attribute("name").Value = txtMedDetailsName.Text;
                this.medDesc.ElementAt(0).Element("desc").Value = rtxtMedDetailsDescription.Text;
            }
            // Save at the correct file path
            this.doc.Save(this.docPath);
        }
    }
}
