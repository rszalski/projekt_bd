﻿namespace projekt_bd {
    partial class formMedDetails {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnMedDetailsSave = new System.Windows.Forms.Button();
            this.txtMedDetailsID = new System.Windows.Forms.TextBox();
            this.txtMedDetailsName = new System.Windows.Forms.TextBox();
            this.lblMedsDeailsID = new System.Windows.Forms.Label();
            this.lblMedDetailsNazwaLeku = new System.Windows.Forms.Label();
            this.rtxtMedDetailsDescription = new System.Windows.Forms.RichTextBox();
            this.btnMedDetailsClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMedDetailsSave
            // 
            this.btnMedDetailsSave.Location = new System.Drawing.Point(12, 288);
            this.btnMedDetailsSave.Name = "btnMedDetailsSave";
            this.btnMedDetailsSave.Size = new System.Drawing.Size(75, 46);
            this.btnMedDetailsSave.TabIndex = 1;
            this.btnMedDetailsSave.Text = "Zapisz Zmiany";
            this.btnMedDetailsSave.UseVisualStyleBackColor = true;
            this.btnMedDetailsSave.Click += new System.EventHandler(this.btnMedDetailsSave_Click);
            // 
            // txtMedDetailsID
            // 
            this.txtMedDetailsID.Location = new System.Drawing.Point(110, 10);
            this.txtMedDetailsID.Name = "txtMedDetailsID";
            this.txtMedDetailsID.Size = new System.Drawing.Size(262, 20);
            this.txtMedDetailsID.TabIndex = 2;
            // 
            // txtMedDetailsName
            // 
            this.txtMedDetailsName.Location = new System.Drawing.Point(110, 37);
            this.txtMedDetailsName.Name = "txtMedDetailsName";
            this.txtMedDetailsName.Size = new System.Drawing.Size(262, 20);
            this.txtMedDetailsName.TabIndex = 3;
            // 
            // lblMedsDeailsID
            // 
            this.lblMedsDeailsID.AutoSize = true;
            this.lblMedsDeailsID.Location = new System.Drawing.Point(12, 13);
            this.lblMedsDeailsID.Name = "lblMedsDeailsID";
            this.lblMedsDeailsID.Size = new System.Drawing.Size(48, 13);
            this.lblMedsDeailsID.TabIndex = 4;
            this.lblMedsDeailsID.Text = "ID Leku:";
            // 
            // lblMedDetailsNazwaLeku
            // 
            this.lblMedDetailsNazwaLeku.AutoSize = true;
            this.lblMedDetailsNazwaLeku.Location = new System.Drawing.Point(12, 40);
            this.lblMedDetailsNazwaLeku.Name = "lblMedDetailsNazwaLeku";
            this.lblMedDetailsNazwaLeku.Size = new System.Drawing.Size(70, 13);
            this.lblMedDetailsNazwaLeku.TabIndex = 5;
            this.lblMedDetailsNazwaLeku.Text = "Nazwa Leku:";
            // 
            // rtxtMedDetailsDescription
            // 
            this.rtxtMedDetailsDescription.Location = new System.Drawing.Point(12, 63);
            this.rtxtMedDetailsDescription.Name = "rtxtMedDetailsDescription";
            this.rtxtMedDetailsDescription.Size = new System.Drawing.Size(360, 219);
            this.rtxtMedDetailsDescription.TabIndex = 6;
            this.rtxtMedDetailsDescription.Text = "";
            // 
            // btnMedDetailsClose
            // 
            this.btnMedDetailsClose.Location = new System.Drawing.Point(297, 288);
            this.btnMedDetailsClose.Name = "btnMedDetailsClose";
            this.btnMedDetailsClose.Size = new System.Drawing.Size(75, 46);
            this.btnMedDetailsClose.TabIndex = 7;
            this.btnMedDetailsClose.Text = "Zamknij";
            this.btnMedDetailsClose.UseVisualStyleBackColor = true;
            this.btnMedDetailsClose.Click += new System.EventHandler(this.btnMedDetailsClose_Click);
            // 
            // formMedDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 348);
            this.Controls.Add(this.btnMedDetailsClose);
            this.Controls.Add(this.rtxtMedDetailsDescription);
            this.Controls.Add(this.lblMedDetailsNazwaLeku);
            this.Controls.Add(this.lblMedsDeailsID);
            this.Controls.Add(this.txtMedDetailsName);
            this.Controls.Add(this.txtMedDetailsID);
            this.Controls.Add(this.btnMedDetailsSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "formMedDetails";
            this.Text = "formMedDetails";
            this.Load += new System.EventHandler(this.formMedDetails_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMedDetailsSave;
        private System.Windows.Forms.TextBox txtMedDetailsID;
        private System.Windows.Forms.TextBox txtMedDetailsName;
        private System.Windows.Forms.Label lblMedsDeailsID;
        private System.Windows.Forms.Label lblMedDetailsNazwaLeku;
        private System.Windows.Forms.RichTextBox rtxtMedDetailsDescription;
        private System.Windows.Forms.Button btnMedDetailsClose;
    }
}