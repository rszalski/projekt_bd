﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projekt_bd {
    class Filter {
        public static List<string> getListOfFilters(Type tableType) {
            // Populating filter
            formApteka formApteka = new formApteka();
            MetaTable userMeta = formApteka.aptekaDC.Mapping.GetTable(tableType);
            var dataMembers = userMeta.RowType.PersistentDataMembers;
            List<string> kolumny = new List<string>();

            foreach (MetaDataMember dataMember in dataMembers) {
                // We do not want to filter by columns which specify relationships
                if (!dataMember.IsAssociation) {
                    kolumny.Add(dataMember.Name);
                }
            }
            return kolumny;
        }
    }
}
