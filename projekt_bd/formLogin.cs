﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Data.SqlClient;

namespace projekt_bd {
    public partial class formApteka : Form {

        private void button_LogIn_Click(object sender, EventArgs e)
        {
            try
            {
                ASCIIEncoding enc = new ASCIIEncoding();
                SHA1 sha = new SHA1CryptoServiceProvider();
                string hashPass = BitConverter.ToString(sha.ComputeHash(enc.GetBytes(textBox_Password.Text.Trim()))).Replace("-", "").ToLower();
                string login = textBox_Login.Text.Trim();

                database.connect(login, hashPass);

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("LOGIN", textBox_Login.Text.Trim());

                DataRow dataFromDB = this.database.getDataSet(
                    "select * from farmaceuci where farmaceuci.ID_Farmaceuty LIKE @LOGIN", parameters).Rows[0];

                loadDataUser(dataFromDB);
                panelLogin.Visible = false;
                MainMenu.Visible = true;
                loadContentForm();
            }
            catch (SqlException exc)
            {
                MessageBox.Show("Błąd uwierzytelniania!\r\nNiepoprawny login albo hasło lub brak połączenia z bazą!",
                    textBox_Login.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void loadDataUser(DataRow userData)
        {
            this.textBox_OMNIE_value.Clear();

            Account.isOnline = true;

            this.textBox_OMNIE_value.AppendText((Account.id = Convert.ToString(userData["ID_Farmaceuty"])) + "\r\n\r\n");
            this.textBox_OMNIE_value.AppendText((Account.surname = Convert.ToString(userData["Nazwisko"])) + "\r\n\r\n");
            this.textBox_OMNIE_value.AppendText((Account.firstname = Convert.ToString(userData["Imie"])) + "\r\n\r\n");
            this.textBox_OMNIE_value.AppendText((Account.birthDate = Convert.ToString(userData["Data_ur"])) + "\r\n\r\n");
            this.textBox_OMNIE_value.AppendText((Account.address = Convert.ToString(userData["Adres"])) + "\r\n\r\n");
            this.textBox_OMNIE_value.AppendText((Account.postalCode = Convert.ToString(userData["Kod_poczt"])) + "\r\n\r\n");
            this.textBox_OMNIE_value.AppendText((Account.city = Convert.ToString(userData["Miejscowosc"])) + "\r\n\r\n");
            this.textBox_OMNIE_value.AppendText(Account.phone = Convert.ToString(userData["Telefon"]));

            Account.roleManager = Convert.ToInt32(this.database.getDataSet(
                "SELECT IS_ROLEMEMBER('kierownik_apteki')").Rows[0][0]) > 0 ? true : false;
        }

        public void refreshTabSell()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("account_id", Account.id);

            this.bsSell.DataSource = (Account.roleManager) ?
                this.database.getDataSet("select * from sprzedaz")
                : this.database.getDataSet("select Id_sprzedazy, Data from sprzedaz where ID_Farmaceuty LIKE @account_id", parameters);

            this.refreshValueSell();
        }

        private void refreshValueSell()
        {
            /*** ADO.NET procedura z parametrem zwrotnym ***/
            this.dgvSell.Columns["Id_sprzedazy"].Visible = false;

            if (this.dgvSell.Columns["Wartość"] == null)
            {
                this.dgvSell.Columns.Add(new DataGridViewColumn(this.dgvSell.Rows[0].Cells[0]));
                this.dgvSell.Columns[this.dgvSell.ColumnCount - 1].Name = "Wartość";
            }

            SqlCommand procedure = this.database.getProcedure("wartoscSprzedazy");
            procedure.UpdatedRowSource = UpdateRowSource.OutputParameters;
            SqlParameter id = new SqlParameter("@id", SqlDbType.Int);
            SqlParameter value = new SqlParameter("@wartosc", SqlDbType.Money);
            value.Direction = ParameterDirection.Output;
            procedure.Parameters.Add(value);

            foreach (DataGridViewRow row in this.dgvSell.Rows)
            {
                if (procedure.Parameters.Contains(id)) procedure.Parameters.Remove(id);
                id.Value = row.Cells["ID_Sprzedazy"].Value;
                procedure.Parameters.Add(id);
                procedure.ExecuteNonQuery();
                row.Cells["Wartość"].Value = procedure.Parameters["@wartosc"].Value.ToString();
            }
        }

        private void loadContentForm()
        {
            if (!Account.roleManager)
            {
                /*** UKRYWANIE LEKI I PRACOWNICY ***/
                this.MainMenu.TabPages.RemoveByKey("tabDelivery");
                this.MainMenu.TabPages.RemoveByKey("tabEmployees");
                /*** DOSTOSOWYWANIE SZEROKOŚCI KART W MENU ***/
                this.MainMenu.ItemSize = new Size(214, this.MainMenu.ItemSize.Height);
                /*** ANULOWANIE OPCJI EDYCJI ZAKŁADKI LEKI ***/
                this.bnMeds.BindingSource = null;
                this.dgvMeds.CellValidating -= new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvMeds_CellValidating);
                this.dgvMeds.ReadOnly = true;
                this.dgvMeds.RowHeadersVisible = false;
                this.btnSaveMedsChanges.Enabled = false;
                /*** ANULOWANIE OPCJI EDYCJI ZAKŁADKI SPRZEDAŻ ***/
                this.lbl_Sell_Sell.Text = "Moje sprzedaże";
            }

            this.refreshTabSell();
            this.dgvSellMeds.Columns["Id_sprzedazy"].Visible = false;
            this.bsMeds.DataSource = this.database.getDataSet("select * from leki");
            this.cb_tabMeds_filter_Nr.SelectedItem = this.cb_tabMeds_filter_Nr.Items[0];
            this.cb_tabMeds_filter_Name.SelectedItem = this.cb_tabMeds_filter_Name.Items[0];
            this.cb_tabMeds_filter_Id.SelectedItem = this.cb_tabMeds_filter_Id.Items[0];
            this.cb_tabMeds_filter_Count.SelectedItem = this.cb_tabMeds_filter_Count.Items[2];
            this.cb_tabMeds_filter_Price.SelectedItem = this.cb_tabMeds_filter_Price.Items[2];
            this.cb_tabMeds_filter_Recipe.SelectedItem = this.cb_tabMeds_filter_Recipe.Items[0];
            this.cb_tabSell_filter_Date.SelectedItem = this.cb_tabSell_filter_Date.Items[0];
            this.cb_tabSell_filter_Value.SelectedItem = this.cb_tabSell_filter_Value.Items[0];
            
            this.listView_ActiveFilters_Meds.Items.Clear();
            this.listView_ActiveFilters_Meds.View = View.Details;
            this.listView_ActiveFilters_Meds.Columns.Add("Filters");
            this.listView_ActiveFilters_Meds.Columns[0].Width = this.listView_ActiveFilters_Meds.Width - 4;
            this.listView_ActiveFilters_Meds.HeaderStyle = ColumnHeaderStyle.None;
        }
    }
}