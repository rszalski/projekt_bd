﻿-- Baza danych dla Apteki - Dane

-- Dla programu C# z przedmiotu Bazy Danych

-- Mariusz Galler
-- Stanisław Karlik
-- Radosław Szalski

-----> FARMACEUCI <----- 
use apteka
INSERT INTO farmaceuci VALUES ('Przemecki', 'Krystian', '1978-12-03', 'os. Sobieskiego 135/12', '62-105', 'Poznań', '665-696-001', 'przkry') 
INSERT INTO farmaceuci VALUES ('Mazurek', 'Jadwiga', '1964-11-27', 'ul. Półwiejska 24/3', '61-559', 'Poznań', '726-113-221', 'mazjad') 
INSERT INTO farmaceuci VALUES ('Kowalczyk', 'Irena', '1970-05-29', 'ul. Świętego Czesława 27/6', '61-549', 'Poznań', '510-413-181', 'kowire') 
INSERT INTO farmaceuci VALUES ('Kaszke', 'Małgorzata', '1977-03-12', 'ul. Ogrodowa 13/1', '61-559', 'Poznań', '663-205-899', 'kasmal') 
GO

-----> DOSTAWCY <----- 
use apteka
INSERT INTO dostawcy VALUES ('Pharmalen S.A', 'ul. Kosmonautów 77', '56-321', 'Komorowo', '0-69 443-15-15') 
INSERT INTO dostawcy VALUES ('Faran Laboratories S.A.', 'ul. Przemysłowa 13', '61-381', 'Poznań', '0-61 764-25-25') 
INSERT INTO dostawcy VALUES ('FarmaBIOS', 'ul. Słoneczna 22', '65-823', 'Poznań', '0-61 788-69-96') 
INSERT INTO dostawcy VALUES ('MIBA S.A.', 'ul. Przemysłowa 36', '59-435', 'Piła', '0-64 664-00-97') 
INSERT INTO dostawcy VALUES ('Vianex S.A.', 'ul. Generała Kislaka 12', '63-384', 'Dopiewo', '0-67 912-29-22') 
GO 

-----> leki <----- 
use apteka
INSERT INTO leki VALUES ('Ibuprom Max tabletki 12 szt.', 30, '1', '5.59', 0) 
INSERT INTO leki VALUES ('Ibuprom Max tabletki 24 szt.', 20, '2', '9.89', 0)
INSERT INTO leki VALUES ('APAP tabletki 12 szt.', 0, '3', '4.90', 0) 
INSERT INTO leki VALUES ('APAP tabletki 50 szt.', 15, '4', '22.90', 0) 
INSERT INTO leki VALUES ('APAP Extra tabletki 10 szt.', 20, '5', '6.88', 0) 
INSERT INTO leki VALUES ('Arthrostop żel 100ml', 11, '6', '23.99', 0) 
INSERT INTO leki VALUES ('Arcalen maść 30g', 7, '7', '10.89', 0) 
INSERT INTO leki VALUES ('Ibum żel 50g', 29, '8', '15.55', 0) 
INSERT INTO leki VALUES ('Ibum żel 100g', 20, '9', '30.03', 0) 
INSERT INTO leki VALUES ('Naproxen Plus żel 50g', 14, '10', '15.55', 0)
INSERT INTO leki VALUES ('Naproxen Plus żel 100g', 5, '11', '30.03', 0) 
INSERT INTO leki VALUES ('Otrivin aerozol 15ml', 28, '12', '18.77', 0) 
INSERT INTO leki VALUES ('Augmentin SR 1g', 18, '13', '21.12', 1) 
INSERT INTO leki VALUES ('Zinnat tabletki 250mg', 29, '14', '18.53', 1) 
INSERT INTO leki VALUES ('Duomox tabletki 1000mg', 36, '15', '16.66', 1) 
INSERT INTO leki VALUES ('Diohespan max tabletki 60szt.', 0, '16', '44.40', 1) 
INSERT INTO leki VALUES ('Bandaż 1m', 96, '17', '2.23', 0) 
INSERT INTO leki VALUES ('Plaster opatrunkowy', 197, '18', '0.68', 0) 
INSERT INTO leki VALUES ('NiQuitin Mini tabletki 4mg 20szt.', 11, '19', '17.51', 0) 
INSERT INTO leki VALUES ('Bioparox 500mg tabletki 6szt.', 0, '20', '19.99', 1) 
INSERT INTO leki VALUES ('Magnez + wit.B6 tabletki 60szt.', 0, '21', '12.52', 0) 
INSERT INTO leki VALUES ('Witamina C tabletki 30szt.', 38, '22',  '6.66', 0) 
INSERT INTO leki VALUES ('Septolete tabletki 12szt.', 23, '23', '18.12', 0) 
INSERT INTO leki VALUES ('Lacidofil 10szt.', 43, '24', '7.17', 0) 
INSERT INTO leki VALUES ('Cholinex tabletki 15szt.', 36, '25', '11.14', 0) 
INSERT INTO leki VALUES ('Bioparox 500mg tabletki 6szt.', 0, '26', '19.99', 1) 
GO

-----> KLIENCI <----- 
use apteka
INSERT INTO klienci VALUES ('Stawisz', 'Halina', 'K', '1956-07-08', 'ul. Przemysłowa 15/2', '61-304', 'Poznań', '56070891825') 
INSERT INTO klienci VALUES ('Heleniak', 'Jan', 'M', '1945-09-21', 'ul. Floriańska 76', '61-304', 'Poznań', '45092176943') 
INSERT INTO klienci VALUES ('Moniak', 'Maria', 'K', '1987-12-12', 'ul. Pleszewska 45', '61-004', 'Czerwonak', '87121298786') 
INSERT INTO klienci VALUES ('Zawodny', 'Patryk', 'M', '1929-08-16', 'ul. Piastowska 12/4', '61-203', 'Luboń', '29081647823') 
INSERT INTO klienci VALUES ('Kucz', 'Emilia', 'K', '1965-10-27', 'ul. Jarocińska 904', '61-304', 'Poznań', '65102715943') 
INSERT INTO klienci VALUES ('Bochna', 'Józef', 'M', '1934-01-02', 'ul. Borecka 12', '61-304', 'Poznań', '34010287342') 
INSERT INTO klienci VALUES ('Biernat', 'Arleta', 'K','1979-05-15', 'ul. Grunwaldzka 65a', '61-304', 'Poznań', '79051591569') 
GO

-----> RECEPTY <----- 
use apteka
INSERT INTO recepty VALUES ('5607089182', '56070891825', '3248937', '2012-04-15') 
INSERT INTO recepty VALUES ('4509217694', '45092176943', '8342732', '2012-03-25')
INSERT INTO recepty VALUES ('8712129878', '87121298786', '3298472', '2012-04-11') 
INSERT INTO recepty VALUES ('2908164782', '29081647823', '3248937', '2012-03-29') 
INSERT INTO recepty VALUES ('6510271594', '65102715943', '2309812', '2012-02-28') 
INSERT INTO recepty VALUES ('3401028734', '34010287342', '2309812', '2012-03-25') 
INSERT INTO recepty VALUES ('7905159156', '79051591569', '3248937', '2012-04-11') 
GO

-----> SPRZEDAZ <----- 
use apteka
INSERT INTO sprzedaz VALUES ('2012-04-28 13:12', 'kowire') 
INSERT INTO sprzedaz VALUES ('2012-04-25 15:32', 'mazjad')
INSERT INTO sprzedaz VALUES ('2012-03-18 09:12', 'przkry') 
INSERT INTO sprzedaz VALUES ('2012-03-29 20:02', 'kowire') 
INSERT INTO sprzedaz VALUES ('2012-04-15 18:52', 'mazjad') 
INSERT INTO sprzedaz VALUES ('2012-04-12 10:19', 'kasmal') 
INSERT INTO sprzedaz VALUES ('2012-03-22 19:17', 'kasmal')
INSERT INTO sprzedaz VALUES ('2012-04-18 22:14', 'przkry') 
INSERT INTO sprzedaz VALUES ('2012-04-09 07:16', 'kasmal') 
INSERT INTO sprzedaz VALUES ('2012-04-16 17:10', 'przkry') 
INSERT INTO sprzedaz VALUES ('2012-03-26 13:15', 'kowire') 
INSERT INTO sprzedaz VALUES ('2012-04-17 14:42', 'mazjad') 
INSERT INTO sprzedaz VALUES ('2012-04-24 20:22', 'kasmal') 
INSERT INTO sprzedaz VALUES ('2012-04-25 16:12', 'przkry') 
INSERT INTO sprzedaz VALUES ('2012-04-20 21:12', 'mazjad') 
GO

-----> SPRZEDANE LEKI <----- 
use apteka
INSERT INTO sprzedane_leki VALUES (1, '1', '1', 2, NULL) 
INSERT INTO sprzedane_leki VALUES (14, '1', '1', 2, NULL) 
INSERT INTO sprzedane_leki VALUES (2, '2', '2', 1, '5607089182') 
INSERT INTO sprzedane_leki VALUES (3, '2', '2', 1, '4509217694') 
INSERT INTO sprzedane_leki VALUES (4, '3', '3', 1, '8712129878') 
INSERT INTO sprzedane_leki VALUES (5, '4', '4', 1, '2908164782') 
INSERT INTO sprzedane_leki VALUES (6, '4', '4', 1, '6510271594') 
INSERT INTO sprzedane_leki VALUES (7, '4', '4', 1, '3401028734') 
INSERT INTO sprzedane_leki VALUES (8, '4', '4', 1, '7905159156')
INSERT INTO sprzedane_leki VALUES (9, '5', '5', 1, NULL) 
INSERT INTO sprzedane_leki VALUES (9, '6', '6', 3, NULL) 
INSERT INTO sprzedane_leki VALUES (10, '7', '7', 4, NULL) 
INSERT INTO sprzedane_leki VALUES (11, '8', '8', 3, NULL) 
INSERT INTO sprzedane_leki VALUES (11, '9', '9', 3, NULL) 
INSERT INTO sprzedane_leki VALUES (11, '10', '10', 2, NULL) 
INSERT INTO sprzedane_leki VALUES (12, '11', '11', 1, NULL) 
INSERT INTO sprzedane_leki VALUES (12, '12', '12', 1, NULL) 
INSERT INTO sprzedane_leki VALUES (13, '13', '13', 2, NULL) 
INSERT INTO sprzedane_leki VALUES (13, '8', '8', 1, NULL) 
INSERT INTO sprzedane_leki VALUES (13, '14', '14', 1, NULL) 
INSERT INTO sprzedane_leki VALUES (13, '15', '15', 1, NULL) 
GO

-----> DOSTAWY <----- 
use apteka
INSERT INTO dostawy VALUES ('1', '2012-02-03') 
INSERT INTO dostawy VALUES ('2', '2012-02-14') 
INSERT INTO dostawy VALUES ('3', '2012-03-01') 
INSERT INTO dostawy VALUES ('4', '2012-03-12') 
INSERT INTO dostawy VALUES ('5', '2012-03-29') 
INSERT INTO dostawy VALUES ('5', '2012-04-05') 
INSERT INTO dostawy VALUES ('4', '2012-04-15') 
GO

-----> DOSTARCZONE LEKI <----- 
use apteka
INSERT INTO dostarczone_leki VALUES ('1', '1', '1', 100, '2016-01-01')
INSERT INTO dostarczone_leki VALUES ('1', '2', '2', 200, '2016-01-01') 
INSERT INTO dostarczone_leki VALUES ('2', '3', '3', 20, '2013-05-01') 
INSERT INTO dostarczone_leki VALUES ('2', '4', '4', 30, '2013-05-01') 
INSERT INTO dostarczone_leki VALUES ('2', '5', '5', 40, '2013-05-01') 
INSERT INTO dostarczone_leki VALUES ('3', '6', '6', 30, '2015-02-14') 
INSERT INTO dostarczone_leki VALUES ('3', '7', '7', 20, '2015-02-14') 
INSERT INTO dostarczone_leki VALUES ('3', '8', '8', 5, '2014-12-01') 
INSERT INTO dostarczone_leki VALUES ('3', '9', '9', 15, '2014-12-01') 
INSERT INTO dostarczone_leki VALUES ('3', '10', '10', 20, '2014-12-01') 
INSERT INTO dostarczone_leki VALUES ('4', '11', '11', 15, '2014-03-12') 
INSERT INTO dostarczone_leki VALUES ('4', '12', '12', 10, '2014-06-12') 
INSERT INTO dostarczone_leki VALUES ('4', '13', '13', 15, '2015-01-10') 
INSERT INTO dostarczone_leki VALUES ('4', '14', '14', 5, '2012-07-31') 
INSERT INTO dostarczone_leki VALUES ('5', '13', '13', 15, '2015-02-10') 
INSERT INTO dostarczone_leki VALUES ('5', '15', '15', 12, '2015-04-05') 
INSERT INTO dostarczone_leki VALUES ('5', '16', '16', 30, '2015-04-05') 
INSERT INTO dostarczone_leki VALUES ('5', '17', '17', 30, '2015-08-05') 
INSERT INTO dostarczone_leki VALUES ('5', '18', '18', 25, '2014-07-05') 
INSERT INTO dostarczone_leki VALUES ('6', '18', '18', 20, '2014-07-05')
INSERT INTO dostarczone_leki VALUES ('6', '19', '19', 25, '2015-04-15')
GO