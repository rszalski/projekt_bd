﻿-- Baza danych dla Apteki

-- Dla programu C# z przedmiotu Bazy Danych

-- Mariusz Galler
-- Stanisław Karlik
-- Radosław Szalski

-- ### TODO ###
-- - Przed każdą operacją dodać operację usuwania aby można było całą bazę odświeżać 1 Execute,
-- - Naprawić trigger obsługa sprzedazy, jeżeli go w ogóle będziemy używać.

-- Create Database
if db_id('apteka') is null
begin
	create database apteka;
end
go


-- Klienci Table
use apteka
if object_id('klienci', 'U') is null
begin
	CREATE TABLE klienci (
		 Nazwisko varchar(30) NOT NULL,
		 Imie varchar(25) NOT NULL,
		 Plec char(1) NOT NULL default 'M',
		 Data_ur date NOT NULL, 
		 Adres varchar(50), 
		 Kod_poczt char(6), 
		 Miejscowosc varchar(30), 
		 PESEL char(11) PRIMARY KEY 
	)
end
go

-- Farmaceuci Table
use apteka
if object_id('farmaceuci', 'U') is null
begin
CREATE TABLE farmaceuci ( 
	Nazwisko varchar(30) NOT NULL, 
	Imie varchar(25) NOT NULL, 
	Data_ur date NOT NULL, 
	Adres varchar(50), 
	Kod_poczt char(6), 
	Miejscowosc varchar(30), 
	Telefon varchar(20), 
	ID_Farmaceuty varchar(6) PRIMARY KEY 
)
end
go

-- Dostawcy Table
use apteka
if object_id('dostawcy', 'U') is null
begin
CREATE TABLE dostawcy( 
	ID_Dostawcy int PRIMARY KEY IDENTITY,
	Nazwa varchar(50) NOT NULL, 
	Adres varchar(50), 
	Kod_poczt char(6), 
	Miejscowosc varchar(30) NOT NULL, 
	Telefon varchar(20) NOT NULL
)
end
go

-- Leki Table
use apteka
if object_id('leki', 'U') is null
begin
CREATE TABLE leki ( 
	ID_Towaru int IDENTITY,
	Nazwa varchar(50) NOT NULL, 
	Ilosc int default 0, 
	Nr_partii int,
	Cena money NOT NULL default '0,00',
	Na_recepte bit NOT NULL default 0,
	CONSTRAINT ID_leku PRIMARY KEY (Nr_partii, ID_Towaru)
)
end
go

-- recepty Table
use apteka
if object_id('recepty', 'U') is null
begin
CREATE TABLE recepty ( 
	ID_Recepty bigint PRIMARY KEY,
	PESEL char(11) NOT NULL FOREIGN KEY REFERENCES klienci(PESEL), 
	Lekarz char(7) NOT NULL, 
	Data_wystawienia date NOT NULL
)
end
go

-- Dostawy Table
use apteka
if object_id('dostawy', 'U') is null
begin
CREATE TABLE dostawy ( 
	ID_Dostawy int PRIMARY KEY IDENTITY, 
	ID_Dostawcy int FOREIGN KEY REFERENCES dostawcy(ID_Dostawcy), 
	Data date NOT NULL
)
end
go

-- Dostarczone_leki Table
use apteka
if object_id('dostarczone_leki', 'U') is null
begin
CREATE TABLE dostarczone_leki ( 
	ID_Dostawy int, 
	ID_Towaru int, 
	Nr_partii int NOT NULL,
	Ilosc int NOT NULL, 
	Data_waznosci date NOT NULL,
	FOREIGN KEY (Nr_partii, ID_Towaru) REFERENCES leki(Nr_partii, ID_Towaru),
	FOREIGN KEY (ID_Dostawy) REFERENCES dostawy(ID_Dostawy),
	CONSTRAINT ID_buy PRIMARY KEY (ID_Towaru, Nr_partii, ID_Dostawy)
)
end
go

-- Sprzedaz Table
use apteka
if object_id('sprzedaz', 'U') is null
begin
CREATE TABLE sprzedaz ( 
	ID_Sprzedazy int PRIMARY KEY IDENTITY,
	Data datetime NOT NULL, 
	ID_Farmaceuty varchar(6) FOREIGN KEY REFERENCES farmaceuci(ID_Farmaceuty)
)
end
go

-- Sprzedane_leki Table
use apteka
if object_id('sprzedane_leki', 'U') is null
begin
CREATE TABLE sprzedane_leki ( 
	ID_Sprzedazy int, 
	ID_Towaru int, 
	Nr_partii int NOT NULL,
	Ilosc int NOT NULL, 
	ID_Recepty bigint FOREIGN KEY REFERENCES recepty(ID_Recepty) ON DELETE CASCADE default NULL, 
	FOREIGN KEY (Nr_partii, ID_Towaru) REFERENCES leki(Nr_partii, ID_Towaru),
	FOREIGN KEY (ID_Sprzedazy) REFERENCES sprzedaz(ID_Sprzedazy),
	CONSTRAINT ID_sell PRIMARY KEY (ID_Sprzedazy, ID_Towaru, Nr_partii),
)
end
go


-- Brakujace_leki View
use apteka
go
CREATE VIEW brakujace_leki AS ( 
	SELECT * FROM leki WHERE (leki.Ilosc = 0 OR leki.Ilosc is null)
)
go

-- konczace_leki View
use apteka
go
CREATE VIEW konczace_leki AS ( 
		SELECT * FROM leki WHERE (leki.Ilosc < 15) AND (leki.Ilosc > 0)
)
go

-- statystyki_aptekarzy View
use apteka
go
CREATE VIEW statystyki_aptekarzy AS ( 
	SELECT F.Nazwisko, F.Imie, F.ID_Farmaceuty, 
	T1.S AS Ilosc_sprzedazy, 
	T2.IL AS Ilosc_Sprz_Lekow, 
	T2.WL AS Wartosc_lekow 
	FROM farmaceuci F JOIN (
		SELECT ID_Farmaceuty, COUNT(ID_Sprzedazy) AS S FROM sprzedaz GROUP BY ID_Farmaceuty
	) AS T1 
	ON F.ID_Farmaceuty = T1.ID_Farmaceuty, 
	(
		SELECT ID_Farmaceuty, SUM(T21.I) AS IL, 
		SUM(T31.WL) AS WL 
		FROM sprzedaz JOIN (
			SELECT ID_Sprzedazy, SUM(Ilosc) AS I 
			FROM sprzedane_leki GROUP BY ID_Sprzedazy
		) AS T21 ON sprzedaz.ID_Sprzedazy = T21.ID_Sprzedazy JOIN (
			SELECT ID_Sprzedazy, SUM(SW.Ilosc*MW.Cena) AS WL
			FROM sprzedane_leki SW JOIN leki MW ON SW.ID_Towaru = MW.ID_Towaru 
			GROUP BY ID_Sprzedazy
		) AS T31 
		ON sprzedaz.ID_Sprzedazy = T31.ID_Sprzedazy GROUP BY ID_Farmaceuty
	) AS T2 
	WHERE F.ID_Farmaceuty = T2.ID_Farmaceuty
)
go

-- statystyki_lekow View
use apteka
go
CREATE VIEW statystyki_lekow AS ( 
	SELECT leki.*, leki.Ilosc*leki.Cena AS WartTowMag, 
	ISNULL(T1.IDo,0) AS Ilosc_Dost,
	ISNULL(T1.IDo*leki.Cena,0) AS WartDostLek, 
	ISNULL(T2.ISp,0) AS Ilosc_Sprz, 
	ISNULL(T2.ISp*leki.Cena,0) AS WartSprzLek 
	FROM leki LEFT JOIN (
		SELECT ID_Towaru, 
		SUM(Ilosc) AS IDo 
		FROM dostarczone_leki GROUP BY ID_Towaru
	) AS T1 
	ON leki.ID_Towaru = T1.ID_Towaru LEFT JOIN (
		SELECT ID_Towaru, 
		SUM(Ilosc) AS ISp 
		FROM sprzedane_leki GROUP BY ID_Towaru
	) AS T2 
	ON leki.ID_Towaru = T2.ID_Towaru
)
go

-- nie_ujemne Trigger
use apteka
go
CREATE TRIGGER nie_ujemne ON leki 
INSTEAD OF INSERT AS
	DECLARE @cena int 
	SELECT @cena = (SELECT Cena FROM inserted) 
	IF @cena <0 
	BEGIN
		PRINT 'Cena produktu nie moze byc mniejsza od 0' 
	END 
	ELSE 
	BEGIN 
		INSERT INTO leki 
		SELECT Nazwa, Ilosc, Nr_partii, Cena, Na_recepte
		FROM inserted 
	END
go

-- obsluga_dostawy Trigger
use apteka
go
CREATE TRIGGER obsluga_dostawy ON dostarczone_leki 
AFTER INSERT AS 
BEGIN 
	DECLARE @towar int 
	DECLARE @ilosc int 
	DECLARE @iloscD int 
	SELECT @towar = (SELECT ID_Towaru FROM inserted)
	SELECT @ilosc = (SELECT Ilosc FROM inserted) 
	SELECT @iloscD = (SELECT Ilosc FROM leki 
	WHERE leki.ID_Towaru = @towar) 
	UPDATE leki 
		SET Ilosc=(Ilosc + @ilosc) WHERE ID_Towaru = @towar 
	PRINT 'Dodano dostawe!' 
	PRINT 'Aktualna ilosc tego towaru w magazynie: ' + CONVERT(nvarchar(4),(@iloscD + @ilosc)) 
END
go

-- obsluga_sprzedazy Trigger

/*
use apteka
go
CREATE TRIGGER obsluga_sprzedazy ON sprzedane_leki 
INSTEAD OF INSERT AS 
BEGIN 
	DECLARE @towar char(8) 
	DECLARE @recepta char(10) 
	DECLARE @ilosc int 
	DECLARE @iloscD int 
	SELECT @towar = (SELECT ID_Towaru FROM inserted) 
	SELECT @recepta = (SELECT ID_Recepty FROM inserted) 
	SELECT @ilosc = (SELECT Ilosc FROM inserted) 
	SELECT @iloscD = (SELECT Ilosc FROM leki WHERE leki.ID_Towaru = @towar) 
	IF ((@towar LIKE '____1___') AND (@recepta IS NULL)) 
	BEGIN 
		PRINT 'Wprowadzony lek nie moze zostac sprzedany bez recepty!' 
	END 
	ELSE 
	BEGIN 
		IF (@ilosc > @iloscD) 
		BEGIN 
			PRINT 'Zbyt mala ilosc leku w aptece!' 
		END 
		ELSE 
		BEGIN 
			INSERT INTO sprzedane_leki 
			SELECT ID_Sprzedazy, ID_Towaru, Nr_partii, Ilosc, ID_Recepty 
			FROM inserted UPDATE leki SET Ilosc = (Ilosc - @ilosc) 
			WHERE ID_Towaru = @towar
			PRINT 'Dodano sprzedaz!' 
			PRINT 'Pozostala ilosc towaru w magazynie: ' + CONVERT(nvarchar(4),(@iloscD - @ilosc)) 
		END 
	END 
END
go */ 

-- usuwanie_dostawy Trigger
use apteka
go
CREATE TRIGGER usuwanie_dostawy ON dostawy 
INSTEAD OF DELETE AS 
BEGIN 
	DECLARE @dostawa char(8) 
	SELECT @dostawa = (SELECT ID_Dostawy FROM deleted) 
	DELETE FROM dostarczone_leki 
	WHERE dostarczone_leki.ID_Dostawy = @dostawa 
	PRINT 'Usunieto sprzedaz!' 
END
go

-- usuwanie_sprzedazy Trigger
use apteka
go
CREATE TRIGGER usuwanie_sprzedazy ON sprzedaz 
INSTEAD OF DELETE AS 
BEGIN 
	DECLARE @sprzedaz char(10) 
	SELECT @sprzedaz = (SELECT ID_Sprzedazy FROM deleted) 
	DELETE FROM recepty 
	WHERE ID_Recepty NOT IN (SELECT ID_Recepty FROM (SELECT ID_Recepty, COUNT(sprzedane_leki.ID_Sprzedazy) AS K 
								FROM sprzedane_leki 
								WHERE ID_Recepty IN (SELECT ID_Recepty FROM sprzedane_leki WHERE ID_Sprzedazy = @sprzedaz AND ID_Recepty IS NOT NULL) 
									AND ID_Sprzedazy != @sprzedaz GROUP BY ID_Recepty) AS tmp WHERE tmp.K > 0) 
									AND ID_Recepty IN (SELECT ID_Recepty FROM sprzedane_leki WHERE ID_Sprzedazy = @sprzedaz AND ID_Recepty IS NOT NULL) 
	DELETE FROM sprzedane_leki 
	WHERE sprzedane_leki.ID_Sprzedazy = @sprzedaz 
	PRINT 'Usunieto sprzedaz!' 
	DELETE FROM sprzedaz 
	WHERE sprzedaz.ID_Sprzedazy = @sprzedaz 
END
go

-- zwraca wartość sprzedaży
CREATE PROCEDURE wartoscSprzedazy
@id int,
@wartosc money OUTPUT
AS
	SELECT @wartosc = w.Wartosc FROM sprzedaz JOIN
		(SELECT sl.ID_Sprzedazy, SUM(sl.Ilosc*l.Cena) AS Wartosc
		FROM sprzedane_leki sl JOIN leki l
			ON sl.ID_Towaru = l.ID_Towaru
		WHERE ID_Sprzedazy = @id
		GROUP BY ID_Sprzedazy) AS w
	ON w.ID_Sprzedazy = sprzedaz.ID_Sprzedazy
RETURN
GO

-- wyszukaj_dostawy Procedure
use apteka
go
CREATE PROCEDURE wyszukaj_dostawy 
@tryb char(1), @arg varchar(10) 
AS 
BEGIN 
	IF @tryb LIKE 'O' --dostawcy 
	BEGIN 
		SELECT dostarczone_leki.*, dostawy.Data, dostawy.ID_Dostawcy 
		FROM dostarczone_leki 
		JOIN dostawy ON dostarczone_leki.ID_Dostawy = dostawy.ID_Dostawy 
		WHERE dostawy.ID_Dostawcy LIKE ('%' + @arg + '%') 
	END 
	IF @tryb LIKE 'S' --dostawy 
	BEGIN
		SELECT dostarczone_leki.*, dostawy.Data, dostawy.ID_Dostawcy 
		FROM dostarczone_leki 
		JOIN dostawy ON dostarczone_leki.ID_Dostawy = dostawy.ID_Dostawy 
		WHERE dostawy.ID_Dostawy LIKE ('%' + @arg + '%') 
	END 
	IF @tryb LIKE 'T' --towar 
	BEGIN 
		SELECT dostarczone_leki.*, dostawy.Data, dostawy.ID_Dostawcy 
		FROM dostarczone_leki 
		JOIN dostawy ON dostarczone_leki.ID_Dostawy = dostawy.ID_Dostawy
		WHERE dostarczone_leki.ID_Towaru LIKE ('%' + @arg + '%') 
	END 
	IF @tryb LIKE 'P' --partia 
	BEGIN 
		SELECT dostarczone_leki.*, dostawy.Data, dostawy.ID_Dostawcy 
		FROM dostarczone_leki 
		JOIN dostawy ON dostarczone_leki.ID_Dostawy = dostawy.ID_Dostawy 
		WHERE dostarczone_leki.Nr_partii LIKE ('%' + @arg + '%') 
	END 
	IF @tryb LIKE 'D' --data
	BEGIN 
		SELECT dostarczone_leki.*, dostawy.Data, dostawy.ID_Dostawcy 
		FROM dostarczone_leki 
		JOIN dostawy ON dostarczone_leki.ID_Dostawy = dostawy.ID_Dostawy 
		WHERE dostawy.Data LIKE ('%' + @arg + '%') 
	END 
END
go

-- wyszukaj_sprzedaz Procedure
use apteka
go
CREATE PROCEDURE wyszukaj_sprzedaz 
@tryb char(1), @arg varchar(10) 
AS 
BEGIN 
	IF @tryb LIKE 'F' --farmaceuta 
	BEGIN 
		SELECT sprzedaz.Data, leki.Nazwa, sprzedane_leki.Ilosc, sprzedane_leki.Nr_partii, sprzedane_leki.ID_Recepty 
		FROM sprzedane_leki 
		JOIN sprzedaz ON sprzedane_leki.ID_Sprzedazy = sprzedaz.ID_Sprzedazy 
		JOIN leki ON leki.ID_Towaru = sprzedane_leki.ID_Towaru
		WHERE sprzedaz.ID_Farmaceuty LIKE ('%' + @arg + '%') 
	END 
	IF @tryb LIKE 'S' --sprzedaz 
	BEGIN 
		SELECT sprzedane_leki.*, sprzedaz.Data 
		FROM sprzedane_leki 
		JOIN sprzedaz ON sprzedane_leki.ID_Sprzedazy = sprzedaz.ID_Sprzedazy 
		WHERE sprzedane_leki.ID_Sprzedazy LIKE ('%' + @arg + '%') 
	END 
	IF @tryb LIKE 'T' --towar 
	BEGIN 
		SELECT sprzedane_leki.*, sprzedaz.Data 
		FROM sprzedane_leki 
		JOIN sprzedaz ON sprzedane_leki.ID_Sprzedazy = sprzedaz.ID_Sprzedazy 
		WHERE sprzedane_leki.ID_Towaru LIKE ('%' + @arg + '%') 
	END 
	IF @tryb LIKE 'R' --recepta 
	BEGIN 
		SELECT sprzedane_leki.*, sprzedaz.Data 
		FROM sprzedane_leki 
		JOIN sprzedaz ON sprzedane_leki.ID_Sprzedazy = sprzedaz.ID_Sprzedazy 
		WHERE sprzedane_leki.ID_Recepty LIKE ('%' + @arg + '%') 
	END 
	IF @tryb LIKE 'D' --data 
	BEGIN 
		SELECT sprzedaz.Data, leki.Nazwa, sprzedane_leki.Ilosc, sprzedane_leki.Nr_partii, sprzedane_leki.ID_Recepty 
		FROM sprzedane_leki 
		JOIN sprzedaz ON sprzedane_leki.ID_Sprzedazy = sprzedaz.ID_Sprzedazy 
		JOIN leki ON leki.ID_Towaru = sprzedane_leki.ID_Towaru
		WHERE sprzedaz.ID_Sprzedazy LIKE ('%' + @arg + '%') 
	END 
END
go

-- dodaj_klienta Procedure
use apteka
go
CREATE PROCEDURE dodaj_klienta 
@Nazwisko varchar(30), @Imie varchar(25), @Plec char(1), @Data_ur date, @Adres varchar(50), @Kod_poczt char(6), @Miejscowosc varchar(30), @PESEL char(11) 
AS 
BEGIN 
	DECLARE @p char(11)
	SELECT @p = (SELECT klienci.PESEL FROM klienci WHERE klienci.PESEL = @PESEL) 
	IF @PESEL NOT LIKE ('[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') 
		RETURN 1
	IF @Kod_poczt IS NOT NULL AND @Kod_poczt NOT LIKE ('[0-9][0-9]-[0-9][0-9][0-9]') 
		RETURN 2
	IF @Plec NOT LIKE '[kKmM]' 
		RETURN 3
	IF @Data_ur > GETDATE()
		RETURN 4
	IF @PESEL = @p 
		RETURN 5
	IF @Nazwisko LIKE ''
		RETURN 6
	IF @Imie LIKE ''
		RETURN 7
	BEGIN
		INSERT INTO klienci 
		VALUES (@Nazwisko, @Imie, @Plec, @Data_ur, @Adres, @Kod_poczt, @Miejscowosc, @PESEL)
	END
END
GO

-- dodaj_klienta Procedure
use apteka
go
CREATE PROCEDURE dodaj_recepte
@id bigint, @lekarz char(7), @data_wyst date, @PESEL char(11) 
AS
BEGIN 
	DECLARE @r bigint
	SELECT @r = (SELECT recepty.ID_Recepty FROM recepty WHERE recepty.ID_Recepty = @id) 
	IF @PESEL NOT LIKE ('[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]') 
		RETURN -1
	IF @data_wyst > GETDATE()
		RETURN -1
	IF @id = @r 
		RETURN -1
	BEGIN
		INSERT INTO recepty 
		VALUES (@id, @PESEL, @lekarz, @data_wyst)
	END
END
GO

-- dodaj_sprzedaz Procedure
use apteka
go
CREATE PROCEDURE dodaj_sprzedaz
@login char(6)
AS
BEGIN 
	BEGIN
		INSERT INTO sprzedaz OUTPUT inserted.ID_Sprzedazy VALUES (GETDATE(), @login)
	END
END
GO

-- porownaj_sprzedaz Procedure
use apteka
go
CREATE PROCEDURE porownaj_sprzedaz
@id varchar(6)
AS
BEGIN 
	IF EXISTS (select f.ID_Farmaceuty from farmaceuci as f where f.ID_Farmaceuty = @id)
		SELECT COUNT(s.ID_Sprzedazy) as ilosc_sprzedazy,
		  (select COUNT(s2.ID_Sprzedazy) from sprzedaz as s2
					join sprzedane_leki as sl2 on s2.ID_Sprzedazy = sl2.ID_Sprzedazy
					join leki as l2 on sl2.ID_Towaru = l2.ID_Towaru
			) as cala_ilosc_sprzedazy,
			 SUM(l.Cena) as suma_sprzedazy, 
			 (select SUM(l3.Cena) from sprzedaz as s3
					join sprzedane_leki as sl3 on s3.ID_Sprzedazy = sl3.ID_Sprzedazy
					join leki as l3 on sl3.ID_Towaru = l3.ID_Towaru
			) as cala_suma_sprzedazy FROM sprzedaz as s
		JOIN farmaceuci as f on f.ID_Farmaceuty = s.ID_Farmaceuty
		JOIN sprzedane_leki as sl on s.ID_Sprzedazy = sl.ID_Sprzedazy
		JOIN leki as l on l.ID_Towaru = sl.ID_Towaru
		WHERE f.ID_Farmaceuty = @id
	ELSE
		RETURN -1
END
GO

-- zwraca pracownika, który sprzedał najwięcej leków
CREATE PROCEDURE farmaceuta_najwiecej_sprzedanych_lekow
@farmaceuta varchar(128) OUTPUT
AS
	select @farmaceuta = f.Imie from farmaceuci as f
	join sprzedaz as s on s.ID_Farmaceuty = f.ID_Farmaceuty
	join sprzedane_leki as sl on sl.ID_Sprzedazy = s.ID_Sprzedazy
	group by f.Imie
	having count(s.ID_Sprzedazy) = (select top 1 count(s1.ID_Sprzedazy) as countsp from farmaceuci as f1
		join sprzedaz as s1 on s1.ID_Farmaceuty = f1.ID_Farmaceuty
		join sprzedane_leki as sl1 on sl1.ID_Sprzedazy = s1.ID_Sprzedazy
		group by f1.Imie order by countsp desc)

RETURN
GO

CREATE ROLE kierownik_apteki 
CREATE ROLE farmaceuta 

GRANT SELECT, INSERT, UPDATE ON sprzedaz TO farmaceuta 
GRANT SELECT, INSERT, UPDATE ON sprzedane_leki TO farmaceuta 
GRANT SELECT, INSERT, UPDATE ON recepty TO farmaceuta 
GRANT SELECT, INSERT, UPDATE ON klienci TO farmaceuta 
GRANT SELECT, UPDATE ON leki TO farmaceuta
GRANT SELECT ON farmaceuci TO farmaceuta
GRANT EXECUTE TO farmaceuta

--GRANT EXECUTE ON dodaj_klienta TO farmaceuta 
GRANT SELECT, UPDATE, INSERT, DELETE, ALTER, EXECUTE TO kierownik_apteki 

CREATE LOGIN przkry WITH PASSWORD = '91dfedf80a28d7877411e987f64cb9b501bc5f83' 
CREATE LOGIN mazjad WITH PASSWORD = '4bcafd6ee803c1b6919d01cbbabe53e0c675c450'
CREATE LOGIN kowire WITH PASSWORD = '19abc088e35fd3a8f59baa9336bba6315818f2aa'
CREATE LOGIN kasmal WITH PASSWORD = '70eec2ac38f4defa99e4da3380833fccdf39a092'

CREATE USER przkry FOR LOGIN przkry 
CREATE USER mazjad FOR LOGIN mazjad 
CREATE USER kowire FOR LOGIN kowire
CREATE USER kasmal FOR LOGIN kasmal
 
EXEC sp_addrolemember 'kierownik_apteki', 'przkry' 
EXEC sp_addrolemember 'farmaceuta', 'mazjad' 
EXEC sp_addrolemember 'farmaceuta', 'kowire' 
EXEC sp_addrolemember 'farmaceuta', 'kasmal'